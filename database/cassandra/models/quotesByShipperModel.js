module.exports = {
  fields: {
    shipperId : "int",
    shipperUserId : "int",

    vendorId : "int",
    vendorName : "text",

    queryShipmentLoad : "text",
    queryForm : "text",

    quoteId : "text",
    orderFreight : "text",
    orderShippingLine : 'text',

    orderDirect : "boolean",
    orderOriginCountryCode : "text",
    orderOriginCountry : "text",
    orderOriginCityCode : "text",
    orderOriginCity : "text",
    orderPol : "text",
    orderDestinationCountryCode : "text",
    orderDestinationCountry : "text",
    orderDestinationCityCode : "text",
    orderDestinationCity : "text",
    orderPod : "text",
    orderTransittimeMax : "int",
    orderTransittimeMin : "int",
    orderValidUntil : "timestamp",
    orderPriceTotal : "float",
    orderPriceCurrencyCode : "text",
    orderPriceCurrencyRate : "float",

    orderNote : "text",
    orderNoteDetailFreight : "text",
    orderPriceFreight : "text",
    orderPriceDocument : "text",
    orderPriceCustom : "text",
    orderPriceTrucking : "text",
    orderPriceInsurance : "text",

    createdBy : "text",
    createdAt : "timestamp"
    },
  key: [["shipperId"], "shipperUserId", "quoteId", "createdAt"],
  clustering_order: {
    "shipperUserId" : "ASC",
    "quoteId" : "DESC",
    "createdAt" : "DESC"
  },
  table_name: "quotesByShipper"
}
