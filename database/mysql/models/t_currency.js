module.exports = function(sequelize, Sequelize) {
  var t_currency = sequelize.define('t_currency', {
    id: {
      autoIncrement: true,
      primaryKey: true,
      type: Sequelize.BIGINT(11)
    },
    base : {
      type : Sequelize.STRING(8),
      allowNull : false
    },
    quote : {
      type : Sequelize.STRING(8),
      allowNull : false
    },
    rate : {
      type : Sequelize.FLOAT,
      allowNull : false
    },
    created_at : {
      type : Sequelize.DATE,
      allowNull : false
    }
  }, {
    timestamps : false,
    freezeTableName : true, //do not pluralizing tables name
    tableName : 't_currency'
  });
  return t_currency;
}
