const { HTTP_STATUS, HTTP_MESSAGE } = require('../config/express');
const { USER_STATUS, COMPANY_STATUS } = require('../config/const');

var mySQL = require('../database/mysql/mysql-client');
var jwt = require('jsonwebtoken');
var bCrypt = require('bcryptjs');
var moment = require('moment');
var randomstring = require('randomstring');
var path = require('path');
var mailer = require('../utils/mailer');
var crypto = require('../utils/crypto');

var env = process.env.NODE_ENV_REST_API || 'development';
var config = require(path.join(__dirname, '..', 'config', 'config.json'))[env];

const {
  secretOrKey,
  session,
  algorithm,
  issuer,
  audience,
  expiresIn
} = config.jwt;

const hashPassword = (password, nullable=false) => {
    return new Promise((resolve, reject) => {
      if(!nullable){
        var salt = bCrypt.genSaltSync(10);
        bCrypt.hash(password, salt, (err, hash) => {
          (err) && reject(err);
          (hash) && resolve(hash.replace('$2a$', '$2y$'))
        });
      }else{
        resolve(undefined)
      }
    })
}

exports.userById = (req, res) => {
  const { companyUserId, companyUserUsername, codeusertype, companyId } = req.user;
  mySQL.c_user.findOne({
    attributes: [
      'username',
      'email',
      'fullname',
      'codeusertype',
      'gender',
      'photo',
      'phone',
      'job'
    ],
    where: {
      id : companyUserId,
      codeusertype,
      id_company: companyId,
      username : companyUserUsername
    }
  }).then((user) => {
    const {
      username,
      fullname,
      codeusertype,
      email,
      gender,
      photo,
      phone,
      job
    } = user;
    const response = {
        username,
        fullname,
        codeusertype,
        email,
        gender,
        photo,
        phone,
        job
    }
    res.status(HTTP_STATUS.OK).json(response)
  }).catch((err) => {
    console.log(err);
    res.sendStatus(HTTP_STATUS.UNAUTHORIZED);
  });
}
exports.update = (req,res) => {
  const { companyId, companyUserId } = req.user;
  const { userFullname, userSex, userEmail, userPhone, userJob, userPassword, userPasswordConfirm } = req.body;

  hashPassword(userPassword, !userPassword && userPassword === userPasswordConfirm ? true : false).then((hashedPassword) => {
    const userValue = {
      fullname : userFullname,
      gender : userSex,
      password : hashedPassword,
      phone : userPhone,
      job : userJob
    };
    typeof hashedPassword === 'undefined' && delete(userValue.password);
    mySQL.c_user.update(userValue,{
      where : {
        id : companyUserId,
        id_company : companyId
      }
    }).then( (user) => {
      const response = {
        fullname : userFullname,
        gender : userSex,
        email : userEmail,
        phone : userPhone,
        job : userJob
      }
      res.status(HTTP_STATUS.OK).json(response)
    }).catch( (err) => {
      console.log(err);
      res.status(HTTP_STATUS.SERVER_ERROR).json({
        message : HTTP_MESSAGE.SERVER_ERROR
      });
    })
  }).catch( err => {
    console.log(err);
    res.status(HTTP_STATUS.SERVER_ERROR).json({
      message : HTTP_MESSAGE.SERVER_ERROR
    });
  })
}
exports.emailValidity = (req, res) => {
  const {email} = req.body;
  mySQL.c_user.findOne({attributes: ['email'], where: {
      email
    }}).then((user) => {
    if (user) {
      res.status(HTTP_STATUS.OK).json({validity: false});
    } else {
      res.status(HTTP_STATUS.OK).json({validity: true});
    }
  });
}

exports.userRegister = (req, res, next) => {
  const {
    //userinfo
    userEmail,
    userPassword,
    userFullname,
    userSex,
    userPhone,
    userJob,
    userKnoweximku,
    //companyInfo
    companyReference,
    companyName,
    companyAddress,
    companyCountry,
    companyPhone,
    companyIndustry,
    //logistic info
    companyRouteDestinationCountry,
    companyRouteOriginCountry,
    airFreight,
    oceanFreight,
    landFreight,
    companyAnnualFCLShipment,
    companyAnnualLCLShipment,
    companyProduct
  } = req.body;

  hashPassword(userPassword).then((hashedPassword) => {
    const verifcode = randomstring.generate(64);
    mySQL.sequelize.transaction((t) => {
      const transportationmode = `${airFreight
        ? 'air'
        : ''} ${oceanFreight
          ? 'ocean'
          : ''} ${landFreight
            ? 'land'
            : ''}`;
      const created_at = moment().format('YYYY-MM-DD hh:mm:ss');
      const companyValue = {
        compref : companyReference,
        compname: companyName,
        compphone: companyPhone,
        compaddress: companyAddress,
        comptp: 'USER',
        companyheadquarter: companyCountry,
        companyindustry: companyIndustry,
        typeproduct: companyProduct,
        averageshipment: companyAnnualFCLShipment,
        shipmentweight: companyAnnualLCLShipment,
        transportationmode: transportationmode,
        origincountry: companyRouteOriginCountry,
        destinationcountry: companyRouteDestinationCountry,
        created_at,
        updated_at: created_at
      }
      return mySQL.c_comp.create(companyValue, {transaction: t}).then((company) => {
        const userValue = {
          username: userEmail,
          password: hashedPassword,
          email: userEmail,
          codeusertype: USER_STATUS.unconfirmed_user,
          id_company: company.id,
          verifcode : verifcode,
          statverif: 0,
          fullname: userFullname,
          gender: userSex,
          phone: userPhone,
          job: userJob,
          knoweximku: userKnoweximku,
          codereferal: null,
          userrating: 0,
          status: 1,
          created_at,
          updated_at: created_at
        }
        return mySQL.c_user.create(userValue, {transaction: t}).then((user) => {
          const compdetValue = {
            id_user: user.id,
            id_company: company.id,
            status: 1,
            created_at,
            updated_at: created_at
          }
          return mySQL.c_compdet.create(compdetValue, {transaction: t});
        });
      })
    }).then((t_res) => {
      const {id_user, id_company} = t_res;
      const response = {
        userId: id_user,
        userEmail: userEmail,
        userFullname: userFullname,
        companyId: id_company,
        isSuccessful: true
      }

      const templateEmail = {
        recipient: {
          email: userEmail,
          fullName: userFullname,
          companyName: companyName
        },
        body: {
          encryptedVerifcode : crypto.encrypt({
            companyId : id_company,
            userId : id_user,
            userEmail : userEmail,
            verifcode : verifcode
          })
        }
      }
      mailer.sendVerificationEmail(templateEmail);
      res.status(HTTP_STATUS.OK).json(response);
    }).catch((err) => {
      console.log(err);
      res.status(HTTP_STATUS.SERVER_ERROR).json({message: HTTP_MESSAGE.SERVER_ERROR});
    });
  }).catch((err) => {
    console.log(err);
    res.status(HTTP_STATUS.SERVER_ERROR).json({message: HTTP_MESSAGE.SERVER_ERROR});
  });
}

exports.userLogin = (req, res) => {
  const {username, password} = req.body;
  if (username && password) {
    mySQL.c_user.findOne({
      attributes: [
        'id',
        'id_company',
        'username',
        'email',
        'password',
        'fullname',
        'codeusertype',
        'gender',
        'photo',
        'phone',
        'statverif'
      ],
      where: {
        username
      }
    }).then((user) => {
      const phpPassword = user !== null
        ? user.password.replace('$2y$', '$2a$')
        : '';
      bCrypt.compare(password, phpPassword, (err, isMatch) => {
        (err) && console.log(err);
        if (isMatch) {
          if (user.statverif === 1 && user.codeusertype !== USER_STATUS.unconfirmed_user) {
            const {
              id,
              id_company,
              username,
              fullname,
              codeusertype,
              phone,
              email,
              gender,
              photo
            } = user;
            const payload = {
              companyId: id_company,
              companyUserId: id,
              companyUserUsername: username,
              companyUserFullName: fullname,
              codeusertype
            };
            const token = jwt.sign(payload, secretOrKey, {algorithm, audience, issuer,/* expiresIn */});
            const response = {
              profile: {
                username,
                fullname,
                gender,
                photo,
                codeusertype,
                email,
                phone
              },
              session: {
                isValid: true,
                loggedAt: moment(),
                token
              }
            }
            res.status(HTTP_STATUS.OK).json(response)
          } else if( user.statverif === 1 && (user.codeusertype === USER_STATUS.confirmed_user || user.codeusertype === USER_STATUS.confirmed_user_officer) ) {
            res.status(HTTP_STATUS.UNAUTHORIZED).json({message: HTTP_MESSAGE.SUSPENDED})
          }else{
            res.status(HTTP_STATUS.UNAUTHORIZED).json({message: HTTP_MESSAGE.NOT_VERIFIED})
          }
        } else {
          res.status(HTTP_STATUS.UNAUTHORIZED).json({message: HTTP_MESSAGE.INVALID_LOGIN});
        }
      });
    }).catch((err) => {
      console.log(err);
      res.sendStatus(HTTP_STATUS.UNAUTHORIZED);
    });
  } else {
    res.sendStatus(HTTP_STATUS.UNAUTHORIZED);
  }
}

exports.registerMailer = (req, res) => {
  const {userEmail, companyName, userId, companyId, userFullname} = req.body;
  mySQL.c_user.findOne({
    attributes: ['verifcode'],
    where: {
      fullname: userFullname,
      email: userEmail,
      id_company: companyId,
      id: userId,
      codeusertype: USER_STATUS.unconfirmed_user
    }
  }).then((user) => {
    const templateEmail = {
      recipient: {
        email: userEmail,
        fullName: userFullname,
        companyName: companyName
      },
      body: {
        encryptedVerifcode: crypto.encrypt({
          companyId : companyId,
          userId : userId,
          userEmail : userEmail,
          verifcode : user.verifcode
        })
      }
    }
    const response = {
      emailSent: true
    }
    res.status(HTTP_STATUS.OK).json(response);
    mailer.sendVerificationEmail(templateEmail);
  }).catch((err) => {
    console.log(err);
    res.status(HTTP_STATUS.SERVER_ERROR).json({message: HTTP_MESSAGE.SERVER_ERROR});
  });
}

exports.verify = (req,res) => {
  const { verifcode } = req.body;
  const decryptedVerifcode = crypto.decrypt(verifcode);
  if(decryptedVerifcode){
    const { companyId, userId, userEmail, verifcode } = decryptedVerifcode;
    mySQL.c_comp.findOne({
      attributes : ['comptp'],
      where : {
        id : companyId
      }
    }).then((company) => {
      const { comptp } = company;
      let codeusertype = undefined;
      if(comptp === COMPANY_STATUS.shipper){
        codeusertype = USER_STATUS.confirmed_user
      }else if(comptp === COMPANY_STATUS.vendor){
        codeusertype = USER_STATUS.confirmed_vendor;
      }else {
        codeusertype = USER_STATUS.sysadmin
      }
      const userValue = {
        statverif : 1,
        codeusertype : USER_STATUS.confirmed_user,
      }
      mySQL.c_user.update(userValue, {
        where : {
          id : userId,
          id_company : companyId,
          username : userEmail,
          email : userEmail,
          verifcode : verifcode
        }
      }).then((affectedRow) => {
        affectedRow[0] > 0 ? res.status(HTTP_STATUS.OK).send() : res.status(HTTP_STATUS.BAD_REQUEST).json({ message : HTTP_MESSAGE.VERIFIED_TWICE})
      }).catch((err) => {
        console.log(err);
        res.status(HTTP_STATUS.BAD_REQUEST).json({message : HTTP_MESSAGE.BAD_REQUEST});
      });
    })
  }else{
    res.status(HTTP_STATUS.BAD_REQUEST).json({message : HTTP_MESSAGE.BAD_REQUEST});
  }
}

exports.resetPasswordMail = (req,res) => {
  const { userEmail } = req.body;
  mySQL.c_user.findOne({
    attributes : ['id', 'email', 'fullname'],
    where : {
      email : userEmail
    }
  }).then( (user) => {
    const templateEmail = {
      recipient: {
        email: userEmail,
        fullName: user.fullname
      },
      body: {
        encryptedResetCode : crypto.encrypt({
          userId : user.id,
          userEmail : userEmail,
          fullName : user.fullname
        })
      }
    }
    mailer.sendResetPasswordEmail(templateEmail);
    res.status(HTTP_STATUS.OK).send();
  }).catch( (err) => {
    console.log(err);
    res.status(HTTP_STATUS.BAD_REQUEST).json({message : HTTP_STATUS.BAD_REQUEST});
  })
}

exports.resetPassword = (req,res) => {
  const { userPassword, resetcode } = req.body;
  const decryptedResetCode = crypto.decrypt(resetcode);
  mySQL.c_user.findOne({
    attributes : ['id', 'email', 'fullname' ],
    where : {
      email : decryptedResetCode.userEmail,
      id : decryptedResetCode.userId,
      fullname : decryptedResetCode.fullName
    }
  }).then( (user) => {
    hashPassword(userPassword).then( (hashedPassword) => {
      user.updateAttributes({
        id : user.id,
        password : hashedPassword
      });
      res.status(HTTP_STATUS.OK).send();
    }).catch( (err) => {
      console.log(err);
      res.status(HTTP_STATUS.SERVER_ERROR).json({message : HTTP_MESSAGE.SERVER_ERROR});
    })
  }).catch( (err) => {
    console.log(err);
    res.status(HTTP_STATUS.BAD_REQUEST).json({message : HTTP_STATUS.BAD_REQUEST});
  })
}
