module.exports = {
  fields: {
    "companyId" : "int",
    "userId" : "int",
    "orderNumber" : "text",
    "shipmentId" : "int",
    "isSubscribed" : "boolean",
    "subscribedAt" : "timestamp"
  },
  key: [ "companyId", "userId", "shipmentId"],
  clustering_order: {
    "userId" : "ASC",
    "shipmentId" : "ASC"
  },
  table_name: "channelSubscribersByUser"
}
