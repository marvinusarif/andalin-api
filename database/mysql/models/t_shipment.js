module.exports = function(sequelize, Sequelize) {
  var t_shipment = sequelize.define('t_shipment', {
    id: {
      autoIncrement: true,
      primaryKey: true,
      type: Sequelize.BIGINT(11)
    },
    id_invoice : {
      type : Sequelize.BIGINT(11),
      allowNull : true,
      unique : true
    },
    invoice_number : {
      type : Sequelize.STRING(64),
      allowNull : true,
      unique : true
    },
    invoice_duedate : {
      type : Sequelize.DATE,
      allowNull : true
    },
    is_invoice_issued : {
      type : Sequelize.BOOLEAN,
      allowNull : false
    },
    is_invoice_paid : {
      type : Sequelize.BOOLEAN,
      allowNull : false
    },
    order_number : {
      type : Sequelize.STRING(64),
      allowNull : false,
      unique : true
    },
    order_stat : {
      type : Sequelize.STRING(8),
      allowNull : false
    },
    shipper_id_comp : {
      type : Sequelize.BIGINT(11),
      allowNull : false
    },
    shipper_id_user : {
      type : Sequelize.BIGINT(11),
      allowNull : false
    },
    shipper_comp : {
      type : Sequelize.STRING(256),
      allowNull : false
    },
    shipper_name : {
      type : Sequelize.STRING(256),
      allowNull : false
    },
    shipper_address : {
      type : Sequelize.STRING(256),
      allowNull : false
    },
    shipper_phone : {
      type : Sequelize.STRING(64),
      allowNull : false
    },
    shipper_email : {
      type : Sequelize.STRING(64),
      allowNull : false
    },
    shipper_fax : {
      type : Sequelize.STRING(64),
      allowNull : true
    },
    shipper_notes : {
      type : Sequelize.TEXT,
      allowNull : true
    },
    commodities_class : {
      type : Sequelize.STRING(8),
      allowNull : false
    },
    commodities_type : {
      type : Sequelize.STRING(64),
      allowNull : false
    },
    commodities_hscode : {
      type : Sequelize.STRING(64),
      allowNull : false
    },
    commodities_product : {
      type : Sequelize.STRING(2048),
      allowNull : false
    },
    consignee_comp : {
      type : Sequelize.STRING(256),
      allowNull : false
    },
    consignee_name : {
      type : Sequelize.STRING(256),
      allowNull : false
    },
    consignee_address : {
      type : Sequelize.STRING(256),
      allowNull : false
    },
    consignee_phone : {
      type : Sequelize.STRING(64),
      allowNull : false
    },
    consignee_fax : {
      type : Sequelize.STRING(64),
      allowNull : true
    },
    consignee_email : {
      type : Sequelize.STRING(64),
      allowNull : false
    },
    transittime_min : {
      type : Sequelize.STRING(64),
      allowNull : false
    },
    additional_request_stat : {
      type : Sequelize.BOOLEAN,
      allowNull : false
    },
    freight_mode : {
      type : Sequelize.STRING(16),
      allowNull : false
    },
    freight_direct : {
      type : Sequelize.BOOLEAN,
      allowNull : false
    },
    freight_type : {
      type : Sequelize.STRING(8),
      allowNull : false
    },
    freight_sum : {
      type : Sequelize.STRING(512),
      allowNull : false
    },
    trucking_stat : {
      type : Sequelize.BOOLEAN,
      allowNull : false
    },
    document_stat : {
      type : Sequelize.BOOLEAN,
      allowNull : false
    },
    custom_stat : {
      type : Sequelize.BOOLEAN,
      allowNull : false
    },
    insurance_stat : {
      type : Sequelize.BOOLEAN,
      allowNull : false
    },
    price_total : {
      type : Sequelize.DOUBLE,
      allowNull : false
    },
    price_currency_code : {
      type : Sequelize.STRING(8),
      allowNull : false
    },
    book_price_total : {
      type : Sequelize.DOUBLE,
      allowNull : false
    },
    book_price_currency_code : {
      type : Sequelize.STRING(8),
      allowNull : false
    },
    book_price_currency_rate : {
      type : Sequelize.FLOAT,
      allowNull : false
    },
    vendor_id : {
      type : Sequelize.FLOAT,
      allowNull : false
    },
    vendor_name : {
      type : Sequelize.STRING(256),
      allowNull : false
    },
    shipment_date : {
      type : Sequelize.DATE,
      allowNull : false
    },
    carrier_name : {
      type : Sequelize.STRING(64),
      allowNull : true
    },
    carrier_bl_awb : {
      type : Sequelize.STRING(256),
      allowNull : true
    },
    carrier_contract : {
      type : Sequelize.STRING(64),
      allowNull : true
    },
    origin_country_code : {
      type : Sequelize.STRING(8),
      allowNull : false
    },
    origin_country : {
      type : Sequelize.STRING(64),
      allowNull : false
    },
    origin_city_code : {
      type : Sequelize.STRING(8),
      allowNull : false
    },
    origin_city : {
      type : Sequelize.STRING(64),
      allowNull : false
    },
    pol : {
      type : Sequelize.STRING(8),
      allowNull : true
    },
    destination_country_code : {
      type : Sequelize.STRING(8),
      allowNull : false
    },
    destination_country : {
      type : Sequelize.STRING(64),
      allowNull : false
    },
    destination_city_code : {
      type : Sequelize.STRING(8),
      allowNull : false
    },
    destination_city : {
      type : Sequelize.STRING(64),
      allowNull : false
    },
    pod : {
      type : Sequelize.STRING(8),
      allowNull : true
    },
    valid_until : {
      type : Sequelize.DATE,
      allowNull : false
    },
    transittime_min : {
      type : Sequelize.STRING(4),
      allowNull : false
    },
    transittime_max : {
      type : Sequelize.STRING(4),
      allowNull : false
    },
    created_at : {
      type : Sequelize.DATE,
      allowNull : false
    },
    updated_at : {
      type : Sequelize.DATE,
      allowNull : true
    }
  }, {
    timestamps : false,
    freezeTableName : true, //do not pluralizing tables name
    tableName : 't_shipment'
  });
  return t_shipment;
}
