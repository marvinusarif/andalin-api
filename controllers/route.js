const { HTTP_STATUS, HTTP_MESSAGE } = require('../config/express');
var mySQL = require('../database/mysql/mysql-client');

exports.getAllCountry = (req,res) => {
  mySQL.m_country.findAll({
    attributes : [ 'code', 'name'],
    order : [ ['name', 'ASC'] ]
  }).then( (countries) => {
    res.status(HTTP_STATUS.OK).json(countries)
  }).catch( (err) => {
    console.log(err);
    res.status(HTTP_STATUS.SERVER_ERROR).json({
      message : HTTP_MESSAGE.SERVER_ERROR
    });
  });
}
exports.getCountryOrigin = (req,res) => {
  mySQL.m_country.findAll({
    attributes : ['code', 'name'],
    order : [ ['name', 'ASC'] ]
  }).then( (countries) => {
    res.status(HTTP_STATUS.OK).json(countries);
  }).catch( (err) => {
    console.log(err);
    res.status(HTTP_STATUS.SERVER_ERROR).json({
      message : HTTP_MESSAGE.SERVER_ERROR
    });
  });
}

exports.getCityOrigin = (req,res) => {
  const { countryCode } = req.params;
  mySQL.m_city.findAll({
    where : {
      codecountry : countryCode,
      transtp : 'F'
    },
    attributes : ['code', 'name', 'codecountry'],
    order : [ ['name','ASC'] ]
  }).then( (cities) => {
    res.status(HTTP_STATUS.OK).json(cities);
  }).catch( (err) => {
    console.log(err);
    res.status(HTTP_STATUS.SERVER_ERROR).json({
      message : HTTP_MESSAGE.SERVER_ERROR
    });
  });
}

exports.getCountryDestination = (req,res) => {
  mySQL.m_country.findAll({
    attributes : ['code', 'name'],
    order : [ ['name', 'ASC'] ]
  }).then( (countries) => {
    res.status(HTTP_STATUS.OK).json(countries);
  }).catch( (err) => {
    console.log(err);
    res.status(HTTP_STATUS.SERVER_ERROR).json({
      message : HTTP_MESSAGE.SERVER_ERROR
    });
  });
}

exports.getCityDestination = (req,res) => {
  const { countryCode } = req.params;
  mySQL.m_city.findAll({
    where : {
      codecountry : countryCode,
      transtp : 'T'
    },
    attributes : ['code', 'name', 'codecountry'],
    order : [ ['name','ASC'] ]
  }).then( (cities) => {
    res.status(HTTP_STATUS.OK).json(cities);
  }).catch( (err) => {
    console.log(err);
    res.status(HTTP_STATUS.SERVER_ERROR).json({
      message : HTTP_MESSAGE.SERVER_ERROR
    });
  });
}
