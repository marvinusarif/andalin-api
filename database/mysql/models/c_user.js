module.exports = function(sequelize, Sequelize) {
  var c_user = sequelize.define('c_user', {
    id: {
      autoIncrement: true,
      primaryKey: true,
      type: Sequelize.BIGINT(11)
    },
    username : {
      type: Sequelize.STRING(64),
      allowNull : false
    },
    password : {
      type : Sequelize.STRING(64),
      allowNull : false
    },
    email : {
      type : Sequelize.STRING(64),
      allowNull : false
    },
    codeusertype : {
      type : Sequelize.STRING(8),
      allowNull : true
    },
    id_company : {
      type : Sequelize.BIGINT(11),
      allowNull : false
    },
    verifcode : {
      type : Sequelize.STRING(255),
      allowNull : true
    },
    statverif : {
      type : Sequelize.BIGINT(11),
      allowNull : true
    },
    fullname : {
      type : Sequelize.STRING(64),
      allowNull : false
    },
    gender : {
      type : Sequelize.STRING(1),
      allowNull : true
    },
    phone : {
      type : Sequelize.STRING(64),
      allowNull : false
    },
    job : {
      type : Sequelize.STRING(25),
      allowNull : false
    },
    photo : {
      type : Sequelize.STRING(256),
      allowNull : true
    },
    knoweximku : {
      type : Sequelize.STRING(256),
      allowNull : true
    },
    codereferal : {
      type : Sequelize.STRING(128),
      allowNull : true
    },
    userrating : {
      type : Sequelize.BIGINT(11),
      allowNull : true
    },
    note : {
      type : Sequelize.STRING(256),
      allowNull : true
    },
    status : {
      type : Sequelize.INTEGER,
      allowNull : false
    },
    updated_at : {
      type : Sequelize.DATE,
      allowNull : true
    },
    created_at : {
      type : Sequelize.DATE,
      allowNull : true
    },
    created_user : {
      type : Sequelize.BIGINT(11),
      allowNull : false,
      defaultValue : 1
    },
    modified_user : {
      type : Sequelize.BIGINT(11),
      allowNull : false,
      defaultValue : 1
    },
    downpayment : {
      type : Sequelize.DECIMAL(19,6),
      allowNull : true
    },
    bankacc : {
      type : Sequelize.STRING(64),
      allowNull : true
    },
    accname : {
      type : Sequelize.STRING(64),
      allowNull : true
    },
    bankname : {
      type : Sequelize.STRING(64),
      allowNull : true
    },
    bankbranch : {
      type : Sequelize.STRING(64),
      allowNull : true
    },
    productdesc : {
      type : Sequelize.STRING(256),
      allowNull : true
    },
    remember_token : {
      type : Sequelize.STRING(64),
      allowNull : true
    },
    verifiedbyadmin : {
      type : Sequelize.STRING(3),
      allowNull : true
    },

  }, {
    timestamps : false,
    freezeTableName : true, //do not pluralizing tables name
    tableName : 'c_user'
  });
  return c_user;
}
