module.exports = {
  fields: {
    "orderDateBucket" : "text",
    "originCityCode" : "text",
    "destinationCityCode" : "text",
    "userId" : "int",
    "companyId" : "int",

    "loadType" : "text",
    "shipmentLoad" : "text",

    "createdAt" : "timestamp"
  },
  key: [["orderDateBucket"], "originCityCode", "destinationCityCode", "createdAt"],
  clustering_order: {
    "originCityCode" : "ASC",
    "destinationCityCode" : "ASC",
    "createdAt" : "DESC"
  },
  table_name: "routeTracker"
}
