var express = require('express');
var bodyParser = require('body-parser');

var route = require('../controllers/route')

var router = express.Router();
router.use(bodyParser.json());
router.use(bodyParser.urlencoded({extended: true}));

router.route('/origin')
.get( route.getCountryOrigin);

router.route('/destination')
.get( route.getCountryDestination);

router.route('/origin/:countryCode')
.get( route.getCityOrigin);

router.route('/destination/:countryCode')
.get( route.getCityDestination);

router.route('/countries')
.get ( route.getAllCountry);

module.exports = router
