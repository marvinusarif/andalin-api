module.exports = function(sequelize, Sequelize) {
  var c_comp = sequelize.define('c_comp', {
    id: {
      autoIncrement: true,
      primaryKey: true,
      type: Sequelize.BIGINT(11)
    },
    compref : {
      type : Sequelize.STRING(8),
      allowNull : true
    },
    compname : {
      type : Sequelize.STRING(64),
      allowNull : true
    },
    compphone : {
      type : Sequelize.STRING(64),
      allowNull : true
    },
    compfax : {
      type : Sequelize.STRING(64),
      allowNull : true
    },
    compemail : {
      type : Sequelize.STRING(64),
      allowNull : true
    },
    compaddress : {
      type : Sequelize.STRING(256),
      allowNull : true
    },
    compaddressbilling : {
      type : Sequelize.STRING(256),
      allowNull : true
    },
    compdescription : {
      type : Sequelize.STRING(2048),
      allowNull : true
    },
    comprate : {
      type : Sequelize.BIGINT(11),
      allowNull : true
    },
    comptp : {
      type : Sequelize.STRING(8),
      allowNull : true
    },
    attachnpwp : {
      type : Sequelize.STRING(256),
      allowNull : true
    },
    statnpwp : {
      type : Sequelize.BIGINT(64),
      allowNull : true
    },
    attachsiup : {
      type : Sequelize.STRING(256),
      allowNull : true
    },
    statsiup : {
      type : Sequelize.BIGINT(11),
      allowNull : true
    },
    attachktp : {
      type : Sequelize.STRING(256),
      allowNull : true
    },
    statktp : {
      type : Sequelize.BIGINT(11),
      allowNull : true
    },
    attachtdp:{
      type : Sequelize.STRING(256),
      allowNull : true
    },
    stattdp : {
      type : Sequelize.BIGINT(11),
      allowNull : true
    },
    attachpassport : {
      type : Sequelize.STRING(256),
      allowNull : true
    },
    statpassport : {
      type : Sequelize.BIGINT(11),
      allowNull : true
    },
    note : {
      type : Sequelize.STRING(256),
      allowNull : true
    },
    status : {
      type : Sequelize.BIGINT(11),
      allowNull : true
    },
    updated_at : {
      type : Sequelize.DATE,
      allowNull : true
    },
    created_at : {
      type : Sequelize.DATE,
      allowNull : true
    },
    created_user : {
      type : Sequelize.BIGINT(11),
      allowNull : true,
      defaultValue : 1
    },
    modified_user : {
      type : Sequelize.BIGINT(11),
      allowNull : true,
      defaultValue : 1
    },
    topayment : {
      type : Sequelize.BIGINT(11),
      allowNull : true
    },
    attachlicense : {
      type : Sequelize.STRING(256),
      allowNull : true
    },
    attachnikexport : {
      type : Sequelize.STRING(256),
      allowNull : true
    },
    attachnikimport : {
      type : Sequelize.STRING(256),
      allowNull : true
    },
    origincountry : {
      type : Sequelize.TEXT,
      allowNull : true
    },
    destinationcountry : {
      type : Sequelize.TEXT,
      allowNull : true
    },
    transportationmode : {
      type : Sequelize.STRING(25),
      allowNull : true
    },
    averageshipment : {
      type : Sequelize.STRING(10)
    },
    companyheadquarter : {
      type : Sequelize.STRING(25),
      allowNull : true
    },
    companyindustry : {
      type : Sequelize.STRING(25),
      allowNull : true
    },
    typeproduct : {
      type : Sequelize.STRING(25),
      allowNull : true
    },
    shipmentweight : {
      type : Sequelize.STRING(10),
      allowNull : true
    },
    documentupload : {
      type : Sequelize.STRING(3),
      allowNull : true
    }
  }, {
    timestamps : false,
    freezeTableName : true, //do not pluralizing tables name
    tableName : 'c_comp'
  });
  return c_comp;
}
