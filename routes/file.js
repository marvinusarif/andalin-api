var express = require('express');
var bodyParser = require('body-parser');

var auth = require('../controllers/auth');
var file = require('../controllers/file');

var router = express.Router();
router.use(bodyParser.json());
router.use(bodyParser.urlencoded({extended: true}));

router.route('/company/upload/doc')
.post( auth.authenticate(), file.multerSingleDoc, file.uploadCompanyDoc, file.saveCompanyDoc);

router.route('/company/download/doc/:method/:documentType')
.get( auth.authenticate(), file.downloadCompanyDoc);

router.route('/user/upload/photo')
.post( auth.authenticate(), file.multerSingleImage, file.uploadUserPhoto, file.saveUserPhoto);

router.route('/shipment/upload')
.post( auth.authenticate(), file.multerSingleDoc, file.uploadShipmentDoc, file.saveShipmentDoc);

router.route('/shipment/download/:method/:shipmentId/:orderNumber/:fileId/:fileCategory')
.get( auth.authenticate(), file.downloadShipmentDoc);

module.exports = router
