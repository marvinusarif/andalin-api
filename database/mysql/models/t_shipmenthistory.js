module.exports = function(sequelize, Sequelize) {
  var t_shipmenthistory = sequelize.define('t_shipmenthistory', {
    id: {
      autoIncrement: true,
      primaryKey: true,
      type: Sequelize.BIGINT(11)
    },
    id_shipment : {
      type: Sequelize.BIGINT(11),
      allowNull : false
    },
    order_number : {
      type : Sequelize.STRING(64),
      allowNull : false
    },
    action : {
      type : Sequelize.STRING(32),
      allowNull : false
    },
    description : {
      type : Sequelize.STRING(256),
      allowNull : false
    },
    created_by_comptp : {
      type : Sequelize.STRING(8),
      allowNull : false
    },
    created_by_id_comp : {
      type : Sequelize.BIGINT(11),
      allowNull : false
    },
    created_by_id_user : {
      type : Sequelize.BIGINT(11),
      allowNull : false
    },
    created_by_username : {
      type : Sequelize.STRING(8),
      allowNull : false
    },
    created_at : {
      type : Sequelize.DATE,
      allowNull : false
    },
  }, {
    timestamps : false,
    freezeTableName : true, //do not pluralizing tables name
    tableName : 't_shipmenthistory'
  });
  return t_shipmenthistory;
}
