var express = require('express');
var bodyParser = require('body-parser');

var auth = require('../controllers/auth');
var shipment = require('../controllers/shipment')

var router = express.Router();
router.use(bodyParser.json());
router.use(bodyParser.urlencoded({extended: true}));

router.route('/')
.get( auth.authenticate(), shipment.getShipments);

router.route('/detail/:shipmentId/:orderNumber')
.get( auth.authenticate(), shipment.getShipmentDetail);

router.route('/detail/shipperconsignee/:shipmentId/:orderNumber')
.put( auth.authenticate(), shipment.editShipmentDetailShipperConsignee);

router.route('/detail/status/:shipmentId/:orderNumber')
.put( auth.authenticate(), shipment.editShipmentDetailOrderStat);

router.route('/detail/commodities/:shipmentId/:orderNumber')
.put( auth.authenticate(), shipment.editShipmentDetailCommodities);

router.route('/detail/carrier/:shipmentId/:orderNumber')
.put( auth.authenticate(), shipment.editShipmentDetailCarrier);

router.route('/detail/freight/:shipmentId/:orderNumber')
.put( auth.authenticate(), shipment.editShipmentDetailFreight);

router.route('/detail/request/:shipmentId/:orderNumber')
.put( auth.authenticate(), shipment.editShipmentDetailRequest);

router.route('/history/:shipmentId/:orderNumber')
.get( auth.authenticate(), shipment.getShipmentHistory );

router.route('/document/:shipmentId/:orderNumber')
.get( auth.authenticate(), shipment.getShipmentDocuments );

router.route('/price/:shipmentId/:orderNumber')
.get( auth.authenticate(), shipment.getShipmentPrice);

router.route('/price/:shipmentId/:orderNumber')
.put( auth.authenticate(), shipment.editShipmentPrice);

router.route('/invoice/stream/:shipmentId/:orderNumber')
.get( auth.authenticate(), shipment.getInvoice);

router.route('/detail/insurance/:shipmentId/:orderNumber')
.put( auth.authenticate() );

router.route('/carrier/:shipmentId/:orderNumber')
.get( auth.authenticate() );

router.route('/bank')
.get( auth.authenticate(), shipment.getAndalinBankAcc);

module.exports = router
