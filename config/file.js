exports.ALLOWED_CONTENT_TYPES = {
  images  : {
    size : 1024 * 1024 * 3,
    ext : ['.jpg','.jpeg', '.png', '.gif']
  },
  documents : {
    size : 1024 * 1024 * 3,
    ext : ['.doc', '.pdf', '.zip', '.rar', '.xls', '.ppt']
  },
  all : {
    size : 1024 * 1024 * 3,
    ext : ['.jpg','.jpeg', '.png', '.gif', '.doc', '.pdf', '.xls', '.ppt', '.zip', '.rar' ]
  }
}
