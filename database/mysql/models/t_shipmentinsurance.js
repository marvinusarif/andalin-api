module.exports = function(sequelize, Sequelize) {
  var t_shipmentinsurance = sequelize.define('t_shipmentinsurance', {
    id: {
      autoIncrement: true,
      primaryKey: true,
      type: Sequelize.BIGINT(11)
    },
    id_shipment : {
      type : Sequelize.BIGINT(11),
      allowNull : false
    },
    id_price : {
      type : Sequelize.BIGINT(11),
      allowNull : false
    },
    order_number : {
      type : Sequelize.STRING(64),
      allowNull : false
    },
    goods_value : {
      type : Sequelize.DOUBLE,
      allowNull : false
    },
    goods_value_currency : {
      type : Sequelize.STRING(8),
      allowNull : false
    },
    pcg : {
      type : Sequelize.FLOAT,
      allowNull : false
    },
    price_sum : {
      type : Sequelize.DOUBLE,
      allowNull : false
    },
    price_currency_code : {
      type : Sequelize.STRING(8),
      allowNull : true
    },
    price_currency_rate : {
      type : Sequelize.FLOAT,
      allowNull : false
    },
    created_at : {
      type : Sequelize.DATE,
      allowNull : false
    },
    updated_at : {
      type : Sequelize.DATE,
      allowNull : true
    },
    created_by_comptp : {
      type : Sequelize.STRING(8),
      allowNull : false
    },
    created_by_id_comp : {
      type : Sequelize.BIGINT(11),
      allowNull : false
    },
    created_by_id_user : {
      type : Sequelize.BIGINT(11),
      allowNull : false
    }
  }, {
    timestamps : false,
    freezeTableName : true, //do not pluralizing tables name
    tableName : 't_shipmentinsurance'
  });
  return t_shipmentinsurance;
}
