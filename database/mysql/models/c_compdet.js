module.exports = function(sequelize, Sequelize) {
  var c_compdet = sequelize.define('c_compdet', {
    id: {
      autoIncrement: true,
      primaryKey: true,
      type: Sequelize.BIGINT(11)
    },
    id_user : {
      type : Sequelize.BIGINT(11),
      allowNull : false
    },
    id_company : {
      type : Sequelize.BIGINT(11),
      allowNull : false
    },
    note : {
      type : Sequelize.STRING(256),
      allowNull : true,
    },
    status : {
      type : Sequelize.BIGINT(11),
      allowNull : false
    },
    updated_at : {
      type : Sequelize.DATE,
      allowNull : true
    },
    created_at : {
      type : Sequelize.DATE,
      allowNull : true
    },
    created_user : {
      type : Sequelize.BIGINT(11),
      allowNull : false,
      defaultValue : 1
    },
    modified_user : {
      type : Sequelize.BIGINT(11),
      allowNull : false,
      defaultValue : 1
    }
  }, {
    timestamps : false,
    freezeTableName : true, //do not pluralizing tables name
    tableName : 'c_compdet'
  });
  return c_compdet;
}
