module.exports = {
  fields: {
    "companyId" : "int",
    "userId" : "int",
    "shipmentId" : "int",
    "orderNumber" : "text",
    "topic" : "text",
    "isRead" : "boolean",
    "createdAt" : "timestamp"
  },
  key: [ "companyId", "userId", "shipmentId", "createdAt"],
  clustering_order: {
    "shipmentId" : "ASC",
    "createdAt" : "DESC"
  },
  table_name: "channelNotificationsByUser"
}
