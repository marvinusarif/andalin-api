module.exports = function(sequelize, Sequelize) {
  var t_andalinbankacc = sequelize.define('t_andalinbankacc', {
    id: {
      autoIncrement: true,
      primaryKey: true,
      type: Sequelize.BIGINT(11)
    },
    bank : {
      type : Sequelize.STRING(64),
      allowNull : false
    },
    name : {
      type : Sequelize.STRING(64),
      allowNull : false
    },
    branch : {
      type : Sequelize.STRING(64),
      allowNull : false
    },
    number : {
      type : Sequelize.STRING(64),
      allowNull : false
    },
    is_active : {
      type : Sequelize.BOOLEAN,
      allowNull : false
    },
    created_at : {
      type : Sequelize.DATE,
      allowNull : false
    }
  }, {
    timestamps : false,
    freezeTableName : true, //do not pluralizing tables name
    tableName : 't_andalinbankacc'
  });
  return t_andalinbankacc;
}
