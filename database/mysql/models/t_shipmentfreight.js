module.exports = function(sequelize, Sequelize) {
  var t_shipmentfreight = sequelize.define('t_shipmentfreight', {
    id: {
      autoIncrement: true,
      primaryKey: true,
      type: Sequelize.BIGINT(11)
    },
    id_shipment : {
      type : Sequelize.BIGINT(11),
      allowNull : false
    },
    order_number : {
      type : Sequelize.STRING(64),
      allowNull : false
    },
    service_type : {
      type : Sequelize.STRING(16),
      allowNull : false
    },
    tracking_number : {
      type : Sequelize.STRING(64),
      allowNull : true
    },
    qty : {
      type : Sequelize.DECIMAL(10,2),
      allowNull : false
    },
    volume : {
      type : Sequelize.DECIMAL(10,2),
      allowNull : false
    },
    weight : {
      type : Sequelize.DECIMAL(10,2),
      allowNull : false
    },
    additional_info : {
      type : Sequelize.STRING(16),
      allowNull : false
    },
    created_at : {
      type : Sequelize.DATE,
      allowNull : false
    },
    updated_at : {
      type : Sequelize.DATE,
      allowNull : true
    },
    created_by_comptp : {
      type : Sequelize.STRING(8),
      allowNull : false
    },
    created_by_id_comp : {
      type : Sequelize.BIGINT(11),
      allowNull : false
    },
    created_by_id_user : {
      type : Sequelize.BIGINT(11),
      allowNull : false
    }
  }, {
    timestamps : false,
    freezeTableName : true, //do not pluralizing tables name
    tableName : 't_shipmentfreight'
  });
  return t_shipmentfreight;
}
