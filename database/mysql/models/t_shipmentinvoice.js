module.exports = function(sequelize, Sequelize) {
  var t_shipmentinvoice = sequelize.define('t_shipmentinvoice', {
    id: {
      autoIncrement: true,
      primaryKey: true,
      type: Sequelize.BIGINT(11)
    },
    id_shipment : {
      type : Sequelize.BIGINT(11),
      allowNull : false
    },
    order_number : {
      type : Sequelize.STRING(64),
      allowNull : false
    },
    invoice_number : {
      type : Sequelize.STRING(64),
      allowNull : true,
      unique : true
    },
    date : {
      type : Sequelize.DATE,
      allowNull : false
    },
    due_date : {
      type : Sequelize.DATE,
      allowNull : false
    },
    currency_rate : {
      type : Sequelize.FLOAT,
      allowNull : false
    },
    currency_from_code : {
      type : Sequelize.STRING(8),
      allowNull : false
    },
    ppn : {
      type : Sequelize.FLOAT,
      allowNull : false
    },
    total_gross : {
      type : Sequelize.DOUBLE,
      allowNull : false
    },
    total_ppn : {
      type : Sequelize.DOUBLE,
      allowNull : false
    },
    total_grand : {
      type : Sequelize.DOUBLE,
      allowNull : false
    },
    is_paid : {
      type : Sequelize.BOOLEAN,
      allowNull : false
    },
    is_valid : {
      type : Sequelize.BOOLEAN,
      allowNull : false
    },
    note : {
      type : Sequelize.STRING(256),
      allowNull : true
    },
    created_at : {
      type : Sequelize.DATE,
      allowNull : false
    },
    updated_at : {
      type : Sequelize.DATE,
      allowNull : true
    },
    created_by_comptp : {
      type : Sequelize.STRING(8),
      allowNull : false
    },
    created_by_id_comp : {
      type : Sequelize.BIGINT(11),
      allowNull : false
    },
    created_by_id_user : {
      type : Sequelize.BIGINT(11),
      allowNull : false
    }
  }, {
    timestamps : false,
    freezeTableName : true, //do not pluralizing tables name
    tableName : 't_shipmentinvoice'
  });
  return t_shipmentinvoice;
}
