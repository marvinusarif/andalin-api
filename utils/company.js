const { USER_STATUS, COMPANY_STATUS } = require('../config/const');

exports.getCompTp =(codeusertype) => {
  if(codeusertype === USER_STATUS.confirmed_user || codeusertype === USER_STATUS.confirmed_user_officer){
    return COMPANY_STATUS.shipper;
  }else if(codeusertype === USER_STATUS.confirmed_vendor || codeusertype === USER_STATUS.confirmed_vendor_officer){
    return COMPANY_STATUS.vendor;
  }else{
    return COMPANY_STATUS.sysadmin
  }
}
