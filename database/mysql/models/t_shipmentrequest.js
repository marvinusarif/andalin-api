module.exports = function(sequelize, Sequelize) {
  var t_shipmentrequest = sequelize.define('t_shipmentrequest', {
    id: {
      autoIncrement: true,
      primaryKey: true,
      type: Sequelize.BIGINT(11)
    },
    id_shipment : {
      type : Sequelize.BIGINT(11),
      allowNull : false
    },
    shipper_id_comp : {
      type : Sequelize.BIGINT(11),
      allowNull : false
    },
    shipper_id_user : {
      type : Sequelize.BIGINT(11),
      allowNull : false
    },
    order_number : {
      type : Sequelize.STRING(64),
      allowNull : false
    },
    type : {
      type : Sequelize.STRING(16),
      allowNull : false
    },
    value : {
      type : Sequelize.STRING(16),
      allowNull : false
    },
    unit : {
      type : Sequelize.STRING(8),
      allowNull : false
    },
    stat : {
      type : Sequelize.STRING(1),
      allowNull : false
    },
    note : {
      type : Sequelize.STRING(256),
      allowNull : true
    },
    created_at : {
      type : Sequelize.DATE,
      allowNull : false
    },
    updated_at : {
      type : Sequelize.DATE,
      allowNull : true
    },
    created_by_comptp : {
      type : Sequelize.STRING(8),
      allowNull : false
    },
    created_by_id_comp : {
      type : Sequelize.BIGINT(11),
      allowNull : false
    },
    created_by_id_user : {
      type : Sequelize.BIGINT(11),
      allowNull : false
    }
  }, {
    timestamps : false,
    freezeTableName : true, //do not pluralizing tables name
    tableName : 't_shipmentrequest'
  });
  return t_shipmentrequest;
}
