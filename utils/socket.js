
var path = require('path');
var env = process.env.NODE_ENV_REST_API || 'development';
var config = require(path.join(__dirname, '..', 'config', 'config.json'))[env];

const { redis } = config;
var io = require('socket.io-emitter')({host : redis.host, port : redis.port});

exports.emitToRooms = (rooms, type, payload) => {
  return rooms.map( (room) => {
    return io.to(room).emit('action',{
      type,
      payload
    })
  })
}
