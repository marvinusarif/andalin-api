const { HTTP_STATUS, HTTP_MESSAGE } = require('../config/express');
const { USER_STATUS, COMPANY_STATUS } = require('../config/const');

var mySQL = require('../database/mysql/mysql-client');

exports.contacts = (req,res) => {
  const { fullname, email, phone, subject, subjectDescription, message, sender } = req.body;
  const contactValue = {
    fullname,
    email,
    phone,
    subject,
    subject_description : subjectDescription,
    message,
    read : 0,
    sender
  }
  mySQL.m_contacts.create(contactValue).then( (contact) => {
    res.status(HTTP_STATUS.OK).json(contact);
  }).catch( (err) => {
    console.log(err);
    res.status(HTTP_STATUS.SERVER_ERROR).json({
      message : HTTP_MESSAGE.SERVER_ERROR
    })
  })
}
