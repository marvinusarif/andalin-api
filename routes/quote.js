var express = require('express');
var bodyParser = require('body-parser');

var auth = require('../controllers/auth');
var quote = require('../controllers/quote');

var router = express.Router();
router.use(bodyParser.json());
router.use(bodyParser.urlencoded({extended: true}));

router.route('/:pageState?')
.get( auth.authenticate(), quote.getQuote);

router.route('/')
.post( auth.authenticate(), quote.postQuote);

router.route('/:quoteId/:createdAt')
.delete( auth.authenticate(), quote.deleteQuote)

module.exports = router
