const {HTTP_STATUS, HTTP_MESSAGE } = require('../config/express');
const { USER_STATUS, COMPANY_STATUS } = require('../config/const');

var mySQL = require('../database/mysql/mysql-client');
var jwt = require('jsonwebtoken');
var bCrypt = require('bcryptjs');
var moment = require('moment');
var randomstring = require('randomstring');
var path = require('path');
var mailer = require('../utils/mailer');
var crypto = require('../utils/crypto');
var { getCompTp } = require('../utils/company');

var env = process.env.NODE_ENV_REST_API || 'development';
var config = require(path.join(__dirname, '..', 'config', 'config.json'))[env];

const hashPassword = (password, nullable=false) => {
    return new Promise((resolve, reject) => {
      if(!nullable){
        var salt = bCrypt.genSaltSync(10);
        bCrypt.hash(password, salt, (err, hash) => {
          (err) && reject(err);
          (hash) && resolve(hash.replace('$2a$', '$2y$'))
        });
      }else{
        resolve(undefined)
      }
    })
}

exports.company = (req,res) => {
  const { companyId, codeusertype } = req.user;
  companyType = getCompTp(codeusertype);
  mySQL.c_comp.findOne({
    attributes : [
                  ['compname', 'name'],
                  ['compaddress', 'address'],
                  ['compphone', 'phone'],
                  ['companyheadquarter', 'country'],
                  ['companyindustry', 'industry'],
                  ['compfax', 'fax'],
                  ['compemail', 'email'],
                  ['compdescription', 'description'],
                  ['comptp', 'type']
                ],
    where : {
      id : companyId,
      comptp : companyType
    }
  }).then( (company) => {
    res.status(HTTP_STATUS.OK).json(company);
  }).catch( (err) => {
    console.log(err)
    res.status(HTTP_STATUS.BAD_REQUEST).json({
      message : HTTP_MESSAGE.BAD_REQUEST
    });
  });
}
exports.update = (req,res) => {
  const { companyId, companyUserId, codeusertype } = req.user;
  const { companyName, companyAddress, companyCountry,
          companyIndustry, companyPhone, companyEmail,
          companyFax, companyDescription } = req.body;
  if(codeusertype === USER_STATUS.confirmed_user || codeusertype === USER_STATUS.confirmed_vendor || codeusertype === USER_STATUS.sysadmin){
    mySQL.c_comp.update({
      compname : companyName,
      compaddress : companyAddress,
      compemail : companyEmail,
      compphone : companyPhone,
      compfax : companyFax,
      compdescription : companyDescription,
      companycountry : companyCountry,
      companyindustry : companyIndustry,
    },{
      fields : ['compname', 'compaddress', 'compphone', 'compemail', 'companyheadquarter', 'companyindustry',
                'compdescription', 'compfax'],
      where : {
        id : companyId
      }
    }).then( (company) => {
        const response = {
          name : companyName,
          address : companyAddress,
          email : companyEmail,
          phone : companyPhone,
          fax : companyFax,
          description : companyDescription,
          country : companyCountry,
          industry : companyIndustry
        }
        res.status(HTTP_STATUS.OK).json(response);
    }).catch( (err) => {
      res.status(HTTP_STATUS.SERVER_ERROR).json({
        message : HTTP_MESSAGE.SERVER_ERROR
      })
    });
  }else{
    res.status(HTTP_STATUS.UNAUTHORIZED).json({
      message : HTTP_MESSAGE.UNAUTHORIZED
    })
  }
}
exports.companyDocuments = (req,res) => {
  const { companyId, codeusertype } = req.user;

  if(codeusertype === USER_STATUS.confirmed_user || codeusertype === USER_STATUS.confirmed_user_officer ){
    companyType = COMPANY_STATUS.shipper;
  }else if(codeusertype === USER_STATUS.confirmed_vendor || codeusertype === USER_STATUS.confirmed_vendor_officer ){
    companyType = COMPANY_STATUS.vendor;
  }else {
    companyType = COMPANY_STATUS.sysadmin
  }

  mySQL.c_comp.findOne({
    attributes : [ 'attachnpwp', 'attachsiup', 'attachtdp', 'attachktp', 'attachpassport',
                   'attachlicense', 'attachnikexport', 'attachnikimport' ],
    where : {
      id : companyId,
      comptp : companyType
    }
  }).then( (company) => {
    const {  attachnpwp, attachsiup, attachtdp,
             attachktp, attachpassport, attachlicense,
            attachnikexport, attachnikimport } = company;
    const response = {
        npwp : attachnpwp ? path.extname(attachnpwp) : false,
        siup : attachsiup ? path.extname(attachsiup) : false,
        tdp : attachtdp ? path.extname(attachtdp) : false,
        ktp : attachktp ? path.extname(attachktp) : false,
        passport : attachpassport ? path.extname(attachpassport) : false,
        license : attachlicense ? path.extname(attachlicense) : false,
        nikexport : attachnikexport ? path.extname(attachnikexport) : false,
        nikimport : attachnikimport ? path.extname(attachnikimport) : false
    }
    res.status(HTTP_STATUS.OK).json(response);
  }).catch( (err) => {
    console.log(err);
    res.status(HTTP_STATUS.BAD_REQUEST).json({
      message : HTTP_MESSAGE.BAD_REQUEST
    });
  });
}
exports.companyUsers = (req,res) => {
  const { companyId } = req.user;

  mySQL.c_user.findAll({
    attributes : ['username', 'codeusertype', 'gender', 'fullname', 'statverif'],
    where : {
      id_company : companyId,
      //statverif : 1
    }
  }).then( (users) => {
    res.status(HTTP_STATUS.OK).json(users);
  }).catch( (err) => {
    console.log(err);
    res.status(HTTP_STATUS.BAD_REQUEST).json({
      message : HTTP_MESSAGE.BAD_REQUEST
    });
  })
}
exports.updateUsers = (req,res) => {
  const { companyId, codeusertype } = req.user;
  const { companyUsers } = req.body;
  const companyUserUsernames = companyUsers.reduce( (companyUsers, companyUser) => {
    return companyUsers.concat(companyUser.username);
  },[]);

  mySQL.c_user.findAll({
    attributes : ['id', 'username', 'codeusertype', 'statverif'],
    where : {
      'id_company' : companyId,
      'username' : { $in : companyUserUsernames }
    }})
    .then( (users) => {
      const updatePromises = users.map( (user,index) => {
        return user.updateAttributes(companyUsers[index])
      });
      return mySQL.Sequelize.Promise.all(updatePromises);
    }).then( (updatedUsers) => {
      res.status(HTTP_STATUS.OK).json(updatedUsers);
    }).catch( (err) => {
      console.log(err);
      res.status(HTTP_STATUS.SERVER_ERROR).json({
        message : HTTP_MESSAGE.SERVER_ERROR
      })
    })
}

exports.inviteUsers = (req,res) => {
  const { companyId, codeusertype } = req.user;
  const { userEmail, companyName } = req.body;
  mySQL.sequelize.transaction((t) => {
    const created_at = moment().format('YYYY-MM-DD hh:mm:ss');
    let inheritCodeusertype = null;
    if( codeusertype === USER_STATUS.confirmed_user){
      inheritCodeusertype = USER_STATUS.confirmed_user_officer;
    }else if(codeusertype === USER_STATUS.confirmed_vendor){
      inheritCodeusertype = USER_STATUS.confirmed_vendor_officer;
    }else{
      inheritCodeusertype = USER_STATUS.sysadmin;
    }
    const userValue = {
      username : userEmail,
      email : userEmail,
      password : 'unset',
      codeusertype : inheritCodeusertype,
      id_company : companyId,
      fullname : userEmail,
      job : 'unset',
      statverif: 0,
      gender: 'N',
      phone: 'unset',
      knoweximku: 'Invitation',
      codereferal: null,
      userrating: 0,
      status: 1,
      created_at,
      updated_at : created_at
    }
    return mySQL.c_user.create(userValue, {transaction : t}).then( (user) => {
      const compdetValue = {
        id_user: user.id,
        id_company: companyId,
        status: 1,
        created_at,
        updated_at: created_at
      }
      return mySQL.c_compdet.create(compdetValue, {transaction: t});
    })
  }).then( (t_res) => {
    const { id_user, id_company } = t_res;
    const response = {
      userEmail: userEmail,
      isSuccessful: true
    }
    const templateEmail = {
      recipient: {
        email: userEmail,
        companyName: companyName
      },
      body: {
        encryptedInvitationCode : crypto.encrypt({
          companyId : id_company,
          userId : id_user,
          userEmail : userEmail
        })
      }
    }
    mailer.sendInvitationEmail(templateEmail);
    res.status(HTTP_STATUS.OK).json(response);
  }).catch( (err) => {
    console.log(err);
    res.status(HTTP_STATUS.INTERNAL_SERVER_ERROR).json({message : HTTP_MESSAGE.INTERNAL_SERVER_ERROR})
  })
}

exports.joinUser = (req,res) => {
  const { userFullname, userSex, userPassword, userPhone, userJob, registrationCode } = req.body;
  const decryptedRegistrationCode = crypto.decrypt(registrationCode);
  mySQL.c_user.findOne({
    attributes : ['id'],
    where : {
      id_company : decryptedRegistrationCode.companyId,
      id : decryptedRegistrationCode.userId,
      email : decryptedRegistrationCode.userEmail
    }
  }).then( (user) => {
    hashPassword(userPassword).then( (hashedPassword) => {
      user.updateAttributes({
        id : decryptedRegistrationCode.userId,
        fullname : userFullname,
        gender : userSex,
        phone : userPhone,
        password : hashedPassword,
        job : userJob,
        statverif : 1
      });
      const response = {
        isSuccessful : true
      }
      res.status(HTTP_STATUS.OK).send(response);
    }).catch( (err) => {
      console.log(err);
      res.status(HTTP_STATUS.INTERNAL_SERVER_ERROR).json({message : HTTP_MESSAGE.INTERNAL_SERVER_ERROR});
    })
  }).catch( (err) => {
    console.log(err);
    res.status(HTTP_STATUS.BAD_REQUEST).json({message : HTTP_MESSAGE.BAD_REQUEST});
  })
}
