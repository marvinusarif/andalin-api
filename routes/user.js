var express = require('express');
var bodyParser = require('body-parser');

var auth = require('../controllers/auth');
var user = require('../controllers/user');

var router = express.Router();
router.use(bodyParser.json());
router.use(bodyParser.urlencoded({extended: true}));

router.route('/')
.get( auth.authenticate(), user.userById);

router.route('/')
.put( auth.authenticate(), user.update);

router.route('/signup')
.post( user.userRegister);

router.route('/login')
.post( user.userLogin);

router.route('/validity/email')
.post( user.emailValidity);

router.route('/mailer/register')
.post( user.registerMailer);

router.route('/verify')
.put( user.verify);

router.route('/resetpassword/mail')
.post( user.resetPasswordMail);

router.route('/resetpassword')
.post( user.resetPassword);

module.exports = router
