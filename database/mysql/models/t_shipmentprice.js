module.exports = function(sequelize, Sequelize) {
  var t_shipmentprice = sequelize.define('t_shipmentprice', {
    id: {
      autoIncrement: true,
      primaryKey: true,
      type: Sequelize.BIGINT(11)
    },
    id_shipment : {
      type : Sequelize.BIGINT(11),
      allowNull : false
    },
    id_price : {
      type : Sequelize.BIGINT(11),
      allowNull : false
    },
    order_number : {
      type : Sequelize.STRING(64),
      allowNull : false
    },
    component_type : {
      type : Sequelize.STRING(16),
      allowNull : false
    },
    component_city_code : {
      type : Sequelize.STRING(8),
      allowNull : true
    },
    service_type : {
      type : Sequelize.STRING(16),
      allowNull : false
    },
    price_group_code : {
      type : Sequelize.STRING(16),
      allowNull : false
    },
    price_group_desc : {
      type : Sequelize.STRING(64),
      allowNull : true
    },
    price_code : {
      type : Sequelize.STRING(16),
      allowNull : false
    },
    price_desc : {
      type : Sequelize.STRING(64),
      allowNull : true
    },
    qty : {
      type : Sequelize.DECIMAL(10,2),
      allowNull : false
    },
    volume : {
      type : Sequelize.DECIMAL(10,2),
      allowNull : false
    },
    weight : {
      type : Sequelize.DECIMAL(10,2),
      allowNull : false
    },
    price_uom : {
      type : Sequelize.STRING(4),
      allowNull : false
    },
    price : {
      type : Sequelize.FLOAT,
      allowNull : false
    },
    price_min : {
      type : Sequelize.FLOAT,
      allowNull : false
    },
    price_sum : {
      type : Sequelize.DOUBLE,
      allowNull : false
    },
    price_currency_code : {
      type : Sequelize.STRING(8),
      allowNull : false
    },
    book_price : {
      type : Sequelize.FLOAT,
      allowNull : false
    },
    book_price_min : {
      type : Sequelize.FLOAT,
      allowNull : false
    },
    book_price_sum : {
      type : Sequelize.DOUBLE,
      allowNull : false
    },
    book_price_currency_code : {
      type : Sequelize.STRING(8),
      allowNull : false
    },
    book_price_currency_rate : {
      type : Sequelize.FLOAT,
      allowNull : false
    },
    created_at : {
      type : Sequelize.DATE,
      allowNull : false
    },
    updated_at : {
      type : Sequelize.DATE,
      allowNull : true
    },
    created_by_comptp : {
      type : Sequelize.STRING(8),
      allowNull : false
    },
    created_by_id_comp : {
      type : Sequelize.BIGINT(11),
      allowNull : false
    },
    created_by_id_user : {
      type : Sequelize.BIGINT(11),
      allowNull : false
    }
  }, {
    timestamps : false,
    freezeTableName : true, //do not pluralizing tables name
    tableName : 't_shipmentprice'
  });
  return t_shipmentprice;
}
