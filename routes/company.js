var express = require('express');
var bodyParser = require('body-parser');

var auth = require('../controllers/auth');
var company = require('../controllers/company');

var router = express.Router();
router.use(bodyParser.json());
router.use(bodyParser.urlencoded({extended: true}));

router.route('/')
.get( auth.authenticate(), company.company);

router.route('/')
.put ( auth.authenticate(), company.update);

router.route('/docs')
.get( auth.authenticate(), company.companyDocuments);

router.route('/users')
.get( auth.authenticate(), company.companyUsers);

router.route('/users')
.post( auth.authenticate(), company.inviteUsers);

router.route('/users/join')
.post( company.joinUser);

router.route('/users')
.put( auth.authenticate(), company.updateUsers);

module.exports = router
