const {HTTP_STATUS, HTTP_MESSAGE } = require('../config/express');
const { WEBSOCKET } = require('../config/const');

var noSQL = require('express-cassandra')
var mySQL = require('../database/mysql/mysql-client');
var moment = require('moment');
var { getCompTp } = require('../utils/company');
var { emitToRooms } = require('../utils/socket');

const getMessagesByMessagesDateBucket = (req,res) => {
  const shipmentId  = parseInt(req.params.shipmentId, 0);
  const splittedOrderNumber = req.params.orderNumber.split('-');
  const shipmentYearMonthBucket = parseInt(splittedOrderNumber[5].substr(2,4),0);
  const orderNumber = req.params.orderNumber.split('-').join('/');
  const { messageDateBucket } = req.params;
  const queryMessagesByShipment = { shipmentYearMonthBucket, messageDateBucket : parseInt(messageDateBucket,0), shipmentId, orderNumber };
  noSQL.instance.messagesByShipment.find(queryMessagesByShipment, (err,messageBodies) => {
    if(err) {
      console.log(err);
      res.status(HTTP_STATUS.SERVER_ERROR).json({message : HTTP_MESSAGE.SERVER_ERROR})
    }else{
      res.status(HTTP_STATUS.OK).json(messageBodies);
    }
  })
}

const getMessagesAll = (req,res) => {
  const shipmentId  = parseInt(req.params.shipmentId, 0);
  const splittedOrderNumber = req.params.orderNumber.split('-');
  const shipmentYearMonthBucket = parseInt(splittedOrderNumber[5].substr(2,4),0);
  const orderNumber = splittedOrderNumber.join('/');
  noSQL.instance.messagesByShipmentHead.find({shipmentYearMonthBucket, shipmentId}, (err,messageHeads) => {
    if(err) {
      console.log(err);
      res.status(HTTP_STATUS.SERVER_ERROR).json({message : HTTP_MESSAGE.SERVER_ERROR})
    } else {
      const messagesBodyPromises = messageHeads.reduce( (messagesBodyPromises, messageHead) => {
        const { messageDateBucket, shipmentId, shipmentYearMonthBucket } = messageHead;
        return messagesBodyPromises.concat(noSQL.instance.messagesByShipment.findAsync({ shipmentYearMonthBucket, messageDateBucket, shipmentId, orderNumber }))
      }, [])
      Promise.all(messagesBodyPromises).then( (messageBodies) => {
        const combinedMessageBodies = messageBodies.reduce( (combinedMessageBodies, messageBody) => {
          return combinedMessageBodies.concat(messageBody);
        })
        res.status(HTTP_STATUS.OK).json(combinedMessageBodies);
      }).catch( (err) => {
        console.log(err);
        res.status(HTTP_STATUS.SERVER_ERROR).json({message : HTTP_MESSAGE.SERVER_ERROR})
      })
    }
  })
}
exports.getChannelSubscribersByShipment = (req,res) => {
  const { companyId, companyUserId, companyUserUsername, codeusertype } = req.user;
  const shipmentId = parseInt(req.params.shipmentId,0);
  const splittedOrderNumber = req.params.orderNumber.split('-');
  const shipmentYearMonthBucket = parseInt(splittedOrderNumber[5].substr(2,4),0);
  noSQL.instance.channelSubscribersByShipment.find({
    shipmentYearMonthBucket,
    shipmentId,
    userId : companyUserId
  },{ select : ['shipmentId', 'orderNumber', 'userId', 'username', 'comptp', 'companyId', "isSubscribed"] }, (err,subscribers) => {
    if(err){
      console.log(err);
      res.status(HTTP_STATUS.SERVER_ERROR).json({message : HTTP_MESSAGE.SERVER_ERROR})
    } else {
      const activeSubscribers = subscribers.reduce( (activeSubscribers, subscriber) => {
        return subscriber.isSubscribed ? activeSubscribers.concat(subscriber) : activeSubscribers
      },[])
      res.status(HTTP_STATUS.OK).json(activeSubscribers)
    }
  })
}
exports.postChannelSubscribersByUser = (req,res) => {
  const { companyId, companyUserId, companyUserUsername, codeusertype } = req.user;
  const splittedOrderNumber = req.body.orderNumber.split('/');
  const shipmentYearMonthBucket = parseInt(splittedOrderNumber[5].substr(2,4),0);
  const shipmentId = parseInt(req.body.shipmentId,0);
  const subscribedAt = moment().format('YYYY-MM-DD HH:mm:ss Z');
  let queries = []
  const channelSubscribersByUserValue = {
    companyId,
    orderNumber : req.body.orderNumber,
    userId : companyUserId,
    shipmentId,
    isSubscribed : true,
    subscribedAt
  }
  const channelSubscribersByShipmentValue = {
    shipmentYearMonthBucket,
    shipmentId,
    companyId,
    orderNumber : req.body.orderNumber,
    userId : companyUserId,
    username : companyUserUsername,
    comptp : getCompTp(codeusertype),
    isSubscribed : true,
    subscribedAt
  }
  queries.push(new noSQL.instance.channelSubscribersByUser(channelSubscribersByUserValue).save({return_query : true}))
  queries.push(new noSQL.instance.channelSubscribersByShipment(channelSubscribersByShipmentValue).save({return_query : true}))
  noSQL.doBatch(queries, function(err){
    if(err) {
      console.log(err)
      res.status(HTTP_STATUS.SERVER_ERROR).json({message : HTTP_MESSAGE.SERVER_ERROR})
    } else{
      mySQL.t_shipment.findOne({
        attributes : [
          ['vendor_id', 'vendorId'],
          ['shipper_id_comp', 'shipperId']
        ],
        where : {
          id : shipmentId,
          order_number : req.body.orderNumber
        }
      }).then( (shipment) => {
        const { shipperId, vendorId } = shipment.dataValues;
        const rooms = [`${shipperId}`,`${vendorId}`,`${1}`]
        emitToRooms(rooms, WEBSOCKET.RECEIVE_WEBSOCKET_NEW_SUBSCRIBER, { response : channelSubscribersByShipmentValue})
      })
      res.status(HTTP_STATUS.OK).json(channelSubscribersByUserValue);
    }
  });
}

exports.getMessages = (req,res) => {
  const { messageDateBucket } = req.params;
  if(messageDateBucket.toUpperCase() === 'ALL'){
    getMessagesAll(req,res);
  }else{
    getMessagesByMessagesDateBucket(req,res);
  }
}

exports.getMessageHeads = (req,res) => {
  const shipmentId  = parseInt(req.params.shipmentId, 0);
  const splittedOrderNumber = req.params.orderNumber.split('-');
  const shipmentYearMonthBucket = parseInt(splittedOrderNumber[5].substr(2,4),0);
  const orderNumber = splittedOrderNumber.join('/');
  noSQL.instance.messagesByShipmentHead.find({shipmentYearMonthBucket, shipmentId}, (err, messageHeads) => {
    if(err) {
      console.log(err);
      res.status(HTTP_STATUS.SERVER_ERROR).json({message : HTTP_MESSAGE.SERVER_ERROR})
    }else{
      res.status(HTTP_STATUS.OK).json(messageHeads);
    }
  })
}
exports.getUnreadNotifications = (req,res) => {
  const { companyId, companyUserId } = req.user;
  noSQL.instance.channelNotificationsByUser.find({
    companyId,
    userId : companyUserId,
  }, { select : ['shipmentId','orderNumber', 'topic', 'isRead', 'createdAt'], limit : 100}, (err,notifications) => {
    if(err){
      console.log(err)
      res.status(HTTP_STATUS.SERVER_ERROR).json({message : HTTP_MESSAGE.SERVER_ERROR})
    } else {
      const unreadNotifications = notifications.reduce( (unreadNotifications, notification) => {
        return !notification.isRead
          ?  unreadNotifications.concat(notification)
          :  unreadNotifications
      },[])
      res.status(HTTP_STATUS.OK).json(unreadNotifications);
    }
  })
}

exports.updateNotificationsByShipment = (req,res) => {
  const { companyId, companyUserId } = req.user;
  const shipmentId = parseInt(req.params.shipmentId, 0);
  const { notifications } = req.body;
  const queries = notifications.reduce( (queries, notification) => {
    const { createdAt } = notification;
    return queries.concat(
      noSQL.instance.channelNotificationsByUser.update({
        companyId,
        userId : companyUserId,
        shipmentId,
        createdAt
      },{
        isRead : true
      }, { return_query : true, ttl : 86400*90}))
  },[])

  noSQL.doBatch( queries, (err) => {
    if(err){
      console.log(err)
    } else {
      const updatedNotifications = notifications.reduce( (updatedNotifications, notification) => {
        return updatedNotifications.concat(Object.notification, {
          isRead : true
        })
      },[])
      res.status(HTTP_STATUS.OK).json({ response : updatedNotifications })
    }
  })
}

exports.saveMessage = (req,res) => {
  const { orderNumber, messageBody, attachment, asReplyTo } = req.body;
  const shipmentId = parseInt(req.body.shipmentId, 0);
  const splittedOrderNumber = orderNumber.split('/');
  const shipmentYearMonthBucket = parseInt(splittedOrderNumber[5].substr(2,4),0);
  const { companyId, companyUserUsername, companyUserId, codeusertype } = req.user;
  const createdAt = moment().format('YYYY-MM-DD HH:mm:ss Z');
  const messagesHeadValue = {
    shipmentId,
    shipmentYearMonthBucket,
    messageDateBucket : parseInt(moment().format('YYYYMMDD'),0)
    }
  new noSQL.instance.messagesByShipmentHead(messagesHeadValue).save((err) => {
    if(err) {
      console.log(err);
      res.status(HTTP_STATUS.SERVER_ERROR).json({message : HTTP_MESSAGE.SERVER_ERROR})
    }else{
      const { shipmentYearMonthBucket, messageDateBucket, shipmentId } = messagesHeadValue;
      const response = {
        shipmentYearMonthBucket,
        messageDateBucket,
        shipmentId,
        orderNumber,
        body : messageBody,
        attachment : attachment ? {
          type : attachment.type,
          fileId : parseInt(attachment.fileId,0),
          fileDesc : attachment.fileDesc
        } : null,
        asReplyTo : asReplyTo ? {
          messageId : asReplyTo.messageId,
          body : asReplyTo.body,
          sender : asReplyTo.sender,
          attachment : asReplyTo.attachment ? asReplyTo.attachment : null,
          createdAt : asReplyTo.createdAt
        } : null,
        postedFrom : 'WEBAPP',
        sender : {
          id : parseInt(companyUserId,0),
          username : companyUserUsername,
          codeusertype : codeusertype,
          compid : companyId,
          comptp : getCompTp(codeusertype)
        },
        createdAt,
        messageId : noSQL.uuid()
      }
      new noSQL.instance.messagesByShipment(response).saveAsync().then(() => {
        noSQL.instance.channelSubscribersByShipment.find({
          shipmentYearMonthBucket,
          shipmentId,
        }, { select : ['companyId', 'userId'] }, (err,subscribers) => {
          //update channel notifications on the sender
          noSQL.instance.channelNotificationsByUser.findAsync({
            companyId,
            userId : companyUserId,
            shipmentId
          },{ select : ['createdAt', 'isRead'] }).then( (notifications) => {
            const updateNoticationQueries = notifications.reduce( (updateNoticationQueries, notification) => {
              const { createdAt, isRead } = notification;
              return !isRead
                ? updateNoticationQueries.concat(noSQL.instance.channelNotificationsByUser.update({
                companyId,
                userId : companyUserId,
                shipmentId,
                createdAt : createdAt
              }, {isRead : true }, { return_query : true, ttl: 86400 * 90}))
              : updateNoticationQueries
            },[])
            const queries = subscribers.reduce( (queries, subscriber) => {
              const { userId, companyId } = subscriber;

              return userId !== companyUserId
              ? queries.concat(new noSQL.instance.channelNotificationsByUser({
                companyId,
                userId,
                shipmentId,
                orderNumber,
                topic  : 'NEW_MESSAGE',
                isRead : false,
                createdAt
              }).save({return_query : true, ttl : 86400 * 90}))
              : queries
            },updateNoticationQueries);

            noSQL.doBatch(queries, (err) => {
              if(err) console.log(err)
            })
            const rooms = [orderNumber]
            emitToRooms(rooms, WEBSOCKET.RECEIVE_WEBSOCKET_UPDATE_SHIPMENT_MESSAGES, { response : {
              message : response,
              notification : {
                shipmentId,
                orderNumber,
                topic : 'NEW_MESSAGE',
                isRead : false,
                createdAt
              }
            }})
            res.status(HTTP_STATUS.OK).json(response);
          })
        })
      }).catch( (err) => {
        console.log(err);
        res.status(HTTP_STATUS.SERVER_ERROR).json({message : HTTP_MESSAGE.SERVER_ERROR})
      })
    }
  })
}
