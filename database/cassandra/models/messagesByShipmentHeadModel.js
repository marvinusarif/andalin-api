module.exports = {
  fields: {
    "shipmentYearMonthBucket" : "int",
    "shipmentId" : "int",
    "messageDateBucket" : "int"
  },
  key: [ "shipmentYearMonthBucket", "shipmentId", "messageDateBucket"],
  clustering_order: {
    "shipmentId" : "ASC",
    "messageDateBucket" : "ASC"
  },
  table_name: "messagesByShipmentHead"
}
