module.exports = {
  fields: {
    "shipmentYearMonthBucket" : "int",
    "shipmentId" : "int",
    "companyId" : "int",
    "orderNumber" : "text",
    "userId" : "int",
    "username" : "text",
    "comptp" : "text",
    "isSubscribed" : "boolean",
    "subscribedAt" : "timestamp"
  },
  key: [ "shipmentYearMonthBucket", "shipmentId", "userId"],
  clustering_order: {
    "shipmentId" : "DESC",
    "userId" : "ASC"
  },
  table_name: "channelSubscribersByShipment"
}
