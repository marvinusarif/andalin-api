const { HTTP_STATUS, HTTP_MESSAGE } = require('../config/express');
const { USER_STATUS, COMPANY_STATUS, HISTORY_ACTION, WEBSOCKET } = require('../config/const');

var mySQL = require('../database/mysql/mysql-client');
var _ = require('lodash');
var moment = require('moment');
var invoice = require('../utils/invoice');
var { getCompTp } = require('../utils/company');
var { emitToRooms } = require('../utils/socket');

exports.getInvoice = (req,res) => {
  const shipmentId  = parseInt(req.params.shipmentId, 0);
  const orderNumber = req.params.orderNumber.split('-').join('/');
  const { codeusertype, companyUserId, companyId } = req.user;
  let queryShipmenDetail;
  let response = {};
  let comptp = getCompTp(codeusertype)
  if( comptp === COMPANY_STATUS.vendor){
    queryShipment = {
      vendor_id : companyId,
      id : shipmentId,
      order_number : orderNumber
    }
  }else if( comptp === COMPANY_STATUS.shipper){
    queryShipment = {
      shipper_id_comp : companyId,
      id : shipmentId,
      order_number : orderNumber
    }
  }else{
    queryShipment = {
      id : shipmentId,
      order_number : orderNumber
    }
  }
  const queryShipmentDetail= {
    id_shipment : shipmentId,
    order_number : orderNumber
  }

  mySQL.c_comp.findOne({
    attributes : [
      ['compname', 'companyName'],
      ['compphone', 'companyPhone'],
      ['compfax', 'companyFax'],
      ['compemail', 'companyEmail'],
      ['compaddress', 'companyAddress'],
      ['compaddressbilling', 'companyAddressBilling'],
      ['comptp', 'comptp']
    ],
    where : {
      id : companyId
    }
  }).then( (shipper) => {
    mySQL.t_shipment.findOne({
      attributes : [
        ['id', 'shipmentId'],
        ['order_stat', 'orderStat'],
        ['order_number', 'orderNumber'],
        ['shipper_name', 'shipperName'],
        ['shipper_comp', 'shipperCompany'],
        ['shipper_address', 'shipperAddress'],
        ['shipper_phone','shipperPhone'],
        ['shipper_email','shipperEmail'],
        ['shipper_fax', 'shipperFax'],
        ['shipper_notes', 'shipperNotes'],
        ['consignee_name', 'consigneeName'],
        ['consignee_comp', 'consigneeCompany'],
        ['consignee_address', 'consigneeAddress'],
        ['consignee_phone', 'consigneePhone'],
        ['consignee_email', 'consigneeEmail'],
        ['consignee_fax', 'consigneeFax'],
        ['freight_type', 'freightType'],
        ['freight_mode', 'freightMode'],
        ['shipment_date', 'shipmentDate'],
        ['carrier_bl_awb', 'carrierBlAwb'],
        ['carrier_name', 'carrierName'],
        ['origin_country', 'originCountry'],
        ['origin_city', 'originCity'],
        ['destination_country', 'destinationCountry'],
        ['destination_city', 'destinationCity'],
        ['book_price_total', 'bookPriceTotal'],
        ['custom_stat', 'customStat'],
        ['insurance_stat', 'insuranceStat'],
        ['trucking_stat', 'truckingStat'],
        ['commodities_class', 'commoditiesClass'],
        ['commodities_type', 'commoditiesType'],
        ['commodities_hscode', 'commoditiesHSCode'],
        ['commodities_product', 'commoditiesProduct'],
        ['book_price_currency_code', 'bookPriceCurrencyCode'],
        ['book_price_currency_rate', 'bookPriceCurrencyRate'],
      ],
      where : queryShipment
    }).then( (shipment) => {
      mySQL.t_shipmentfreight.findAll({
        attributes : [
          ['id', 'freightId'],
          ['tracking_number', 'trackingNumber'],
          ['order_number', 'orderNumber'],
          ['qty', 'qty'],
          ['volume', 'volume'],
          ['service_type', 'serviceType'],
          ['additional_info', 'additionalInfo'],
          ['weight', 'weight'],
          'updated_at'
        ],
        where : queryShipmentDetail
      }).then( (shipmentFreights) => {
        const combinedShipmentFreight = _.reduce( shipmentFreights, (combinedShipmentFreight, freight) => {
          return combinedShipmentFreight.concat(freight.dataValues);
        },[]);

        mySQL.t_shipmentprice.findAll({
          attributes : [
            ['id_shipment', 'shipmentId'],
            ['component_type','componentType'],
            ['component_city_code', 'componentCityCode'],
            ['service_type', 'serviceType'],
            ['price_code', 'priceCode'],
            ['price_desc', 'priceDesc'],
            ['qty','qty'],
            ['volume','volume'],
            ['weight','weight'],
            ['price_uom','priceUOM'],
            ['book_price_sum', 'bookPriceSum'],
            ['book_price_currency_rate', 'bookPriceCurrencyRate'],
            ['book_price_currency_code', 'bookPriceCurrencyCode']
          ],
          order : [['created_at', 'ASC']],
          where : queryShipmentDetail
        }).then((shipmentPrice) => {
          const combinedShipmentPrice = _.reduce( shipmentPrice, (combinedShipmentPrice, price) => {
            return combinedShipmentPrice.concat(Object.assign({},price.dataValues,{
              bookPriceSum : price.dataValues.bookPriceSum.toFixed(2)
            }));
          },[])
          mySQL.t_shipmentinsurance.findOne({
            attributes : [
              ['goods_value', 'goodsValue'],
              ['goods_value_currency', 'goodsValueCurrency'],
              ['pcg', 'pcg'],
              ['price_sum', 'priceSum'],
              ['price_currency_code', 'priceCurrencyCode'],
              ['price_currency_rate', 'priceCurrencyRate'],
            ],
            where : queryShipmentDetail,
            order : [['created_at', 'DESC']]
          }).then( (shipmentInsurance) => {
            mySQL.t_shipmentinvoice.findOne({
              attributes : [
                ['id', 'invoiceId'],
                ['date', 'invoiceDate'],
                ['due_date', 'invoiceDueDate'],
                ['invoice_number', 'invoiceNumber'],
                ['currency_rate', 'currencyRate'],
                ['currency_from_code', 'currencyFromCode'],
                ['ppn', 'ppn'],
                ['total_gross', 'totalGross'],
                ['total_ppn', 'totalPPN'],
                ['total_grand', 'totalGrand'],
                ['is_paid', 'isPaid'],
                ['note', 'note']
              ],
              where : {
                id_shipment : shipmentId,
                order_number : orderNumber,
                updated_at : null
              },
              order : [['created_at','DESC']]
            }).then( (shipmentInvoice) => {
              mySQL.t_shipmentpayment.findAll({
                attributes : [
                  ['id', 'paymentId'],
                  ['method', 'paymentMethod'],
                  ['andalin_bank_name', 'andalinBankName'],
                  ['andalin_bank_account_name', 'andalinBankAccName'],
                  ['andalin_bank_account_number', 'andalinBankAccNumber'],
                  ['shipper_bank_name', 'shipperBankName'],
                  ['shipper_bank_account_name', 'shipperBankAccName'],
                  ['shipper_bank_account_number', 'shipperBankAccNumber'],
                  ['payment_reference_number', 'paymentReferenceNumber'],
                  ['payment_date', 'paymentDate'],
                  ['payment_total', 'paymentTotal'],
                  ['payment_currency_code', 'paymentCurrencyCode']
                ],
                where : queryShipmentDetail
              }).then( ( shipmentPayments) => {
                mySQL.t_andalinbankacc.findAll({
                  where : {
                    is_active : true
                  }
                }).then((andalinBanks) => {
                  mySQL.t_currency.findOne({
                    attributes : [ 'base', 'quote','rate',
                                  ['created_at', 'createdAt']
                                ],
                    where : {
                      base : 'USD',
                      quote : 'IDR'
                    },
                    order : [['id','DESC'], ['created_at','DESC']]
                  }).then((currency) => {
                    response = Object.assign({}, response, {
                      invoiceStatus : shipment.dataValues.orderStat === 'PAYMENT' || shipment.dataValues.orderStat === 'COMPLETE' ? 'FINAL' : 'PROFORMA',
                      shipper : shipper.dataValues,
                      shipment : shipment.dataValues,
                      freights : combinedShipmentFreight,
                      prices : combinedShipmentPrice,
                      invoice : shipmentInvoice ? shipmentInvoice.dataValues : null,
                      payments : _.reduce(shipmentPayments, (payments, payment) => {
                        return payments.concat(Object.assign({}, payment.dataValues));
                      },[]),
                      insurance : shipmentInsurance ? Object.assign({},shipmentInsurance.dataValues,{
                                    pcg : shipmentInsurance.dataValues.pcg * 100,
                                    goodsValue : parseFloat(shipmentInsurance.dataValues.goodsValue,2)
                                  }) : null,
                      banks : _.reduce(andalinBanks, (banks, bank) => {
                        return banks.concat(Object.assign({}, bank.dataValues));
                      },[]),
                      currency : currency.dataValues
                    })
                    invoice.generateInvoice(response).then( (invoiceStream) => {
                      invoiceStream.pipe(res);
                  }).catch( (err) => {
                    console.log(err);
                    res.status(HTTP_STATUS.SERVER_ERROR).json({message : HTTP_MESSAGE.SERVER_ERROR});
                  })
                }).catch( (err) => {
                  console.log(err);
                  res.status(HTTP_STATUS.SERVER_ERROR).json({message : HTTP_MESSAGE.SERVER_ERROR});
                })
              }).catch( (err) => {
                console.log(err);
                res.status(HTTP_STATUS.SERVER_ERROR).json({message : HTTP_MESSAGE.SERVER_ERROR});
              })
            }).catch( (err) => {
              console.log(err);
              res.status(HTTP_STATUS.SERVER_ERROR).json({message : HTTP_MESSAGE.SERVER_ERROR});
            })

            }).catch( (err) => {
              console.log(err);
              res.status(HTTP_STATUS.SERVER_ERROR).json({message : HTTP_MESSAGE.SERVER_ERROR});
            })
          }).catch( (err) => {
            console.log(err);
            res.status(HTTP_STATUS.SERVER_ERROR).json({message : HTTP_MESSAGE.SERVER_ERROR});
          })
        }).catch( (err) => {
          console.log(err);
          res.status(HTTP_STATUS.SERVER_ERROR).json({message : HTTP_MESSAGE.SERVER_ERROR});
        })
      }).catch( (err) => {
        console.log(err);
        res.status(HTTP_STATUS.SERVER_ERROR).json({message : HTTP_MESSAGE.SERVER_ERROR});
      })
    }).catch( (err) => {
      console.log(err);
      res.status(HTTP_STATUS.SERVER_ERROR).json({message : HTTP_MESSAGE.SERVER_ERROR});
    })
  }).catch( (err) => {
    console.log(err);
    res.status(HTTP_STATUS.SERVER_ERROR).json({message : HTTP_MESSAGE.SERVER_ERROR});
  })
}

exports.editShipmentDetailShipperConsignee = (req,res) => {
  const shipmentId  = parseInt(req.params.shipmentId, 0);
  const orderNumber = req.params.orderNumber.split('-').join('/');
  const { companyId, codeusertype, companyUserId, companyUserUsername } = req.user;
  const { shipperPICName, shipperCompany, shipperPhone, shipperFax, shipperAddress, shipperEmail, shipperNotes,
          consigneePICName, consigneeCompany, consigneePhone, consigneeFax, consigneeEmail, consigneeAddress } = req.body;
  let queryShipmentDetail;
  const comptp = getCompTp(codeusertype);
  if( comptp === COMPANY_STATUS.vendor){
    queryShipmentDetail = {
      vendor_id : companyId,
      id : shipmentId,
      order_number : orderNumber
    }
  }else if( comptp === COMPANY_STATUS.shipper){
    queryShipmentDetail = {
      shipper_id_comp : companyId,
      id : shipmentId,
      order_number : orderNumber
    }
  }else{
    queryShipmentDetail = {
      id : shipmentId,
      order_number : orderNumber
    }
  }
  mySQL.sequelize.transaction((t) => {
    const historyValue = {
      id_shipment : shipmentId,
      order_number : orderNumber,
      created_by_comptp : comptp,
      created_by_id_comp : companyId,
      created_by_id_user : companyUserId,
      created_by_username : companyUserUsername,
      action : HISTORY_ACTION.SHIPPER_CONSIGNEE.UPDATE,
      description : `update shipper/consignee detail`,
      created_at : moment().format('YYYY-MM-DD hh:mm:ss')
    }
    return mySQL.t_shipmenthistory.create(historyValue,{transaction : t}).then( (history) => {
      return mySQL.t_shipment.findOne({
        attributes : [
          ['shipper_id_comp', 'shipperId'],
          ['vendor_id', 'vendorId'],
          ['shipper_name', 'shipperName'],
          ['shipper_comp', 'shipperCompany'],
          ['shipper_address', 'shipperAddress'],
          ['shipper_phone','shipperPhone'],
          ['shipper_email','shipperEmail'],
          ['shipper_fax', 'shipperFax'],
          ['shipper_notes', 'shipperNotes'],
          ['consignee_name', 'consigneeName'],
          ['consignee_comp', 'consigneeCompany'],
          ['consignee_address', 'consigneeAddress'],
          ['consignee_phone', 'consigneePhone'],
          ['consignee_email', 'consigneeEmail'],
          ['consignee_fax', 'consigneeFax'],
          'updated_at'
        ],
        where : queryShipmentDetail
      }).then( (shipment) => {
        return shipment.updateAttributes({
          id : shipmentId,
          shipper_id_comp : shipment.dataValues.shipperId,
          vendor_id : shipment.dataValues.vendorId,
          order_number : orderNumber,
          shipper_name : shipperPICName,
          shipper_comp : shipperCompany,
          shipper_phone : shipperPhone,
          shipper_address : shipperAddress,
          shipper_email : shipperEmail,
          shipper_fax : shipperFax,
          shipper_notes : shipperNotes,
          consignee_name : consigneePICName,
          consignee_address : consigneeAddress,
          consignee_comp : consigneeCompany,
          consignee_phone : consigneePhone,
          consignee_email : consigneeEmail,
          consignee_fax : consigneeFax,
          updated_at : moment().format('YYYY-MM-DD hh:mm:ss')
        }, { transaction : t})
      })
    })
  }).then((transactionResult) => {
    const { id, shipper_id_comp, vendor_id, order_number, shipper_name, shipper_comp, shipper_phone, shipper_address, shipper_email, shipper_fax,
            consignee_name, consignee_address, consignee_comp, consignee_phone, consignee_email, consignee_fax, updated_at } = transactionResult;
    const response = Object.assign({}, {
      shipmentId : id,
      orderNumber : order_number,
      shipperName : shipper_name,
      shipperCompany : shipper_comp,
      shipperPhone : shipper_phone,
      shipperEmail : shipper_email,
      shipperFax : shipper_fax,
      consigneeName : consignee_name,
      consigneeCompany : consignee_comp,
      consigneePhone : consignee_phone,
      consigneeEmail : consignee_email,
      consigneeFax : consignee_fax,
      updatedAt : updated_at
    })

    //return action creator to client
    const rooms = [`${shipper_id_comp}`, `${vendor_id}`, `${1}`]
    emitToRooms(rooms, WEBSOCKET.RECEIVE_WEBSOCKET_UPDATE_SHIPMENT_SHIPPER_CONSIGNEE, { response })
    res.status(HTTP_STATUS.OK).json(response);
  }).catch( (err) => {
    console.log(err);
    res.status(HTTP_STATUS.SERVER_ERROR).json({message : HTTP_MESSAGE.SERVER_ERROR});
  })
}

exports.getAndalinBankAcc = (req,res) => {
  mySQL.t_andalinbankacc.findAll({
    attributes : [
      ['id', 'bankId'],
      ['name', 'name'],
      ['bank', 'bank'],
      ['branch', 'branch'],
      ['number', 'number']
    ],
    where : {
      is_active : true
    }
  }).then( (andalinBank) => {
    res.status(HTTP_STATUS.OK).json(andalinBank);
  }).catch( (err) => {
    console.log(err);
      res.status(HTTP_STATUS.SERVER_ERROR).json({message : HTTP_MESSAGE.SERVER_ERROR});
  })
}
exports.editShipmentDetailOrderStat = (req,res) => {
  const shipmentId  = parseInt(req.params.shipmentId, 0);
  const orderNumber = req.params.orderNumber.split('-').join('/');
  const { companyId, codeusertype, companyUserId, companyUserUsername} = req.user;
  const { orderStat } = req.body;
  let queryShipmentDetail;
  let tempShipmentDetail;
  let invoiceNumber;
  const timestamp = moment().format('YYYY-MM-DD hh:mm:ss');
  const comptp = getCompTp(codeusertype);
  if( comptp === COMPANY_STATUS.vendor){
    queryShipmentDetail = {
      vendor_id : companyId,
      id : shipmentId,
      order_number : orderNumber
    }
  }else if( comptp === COMPANY_STATUS.shipper){
    queryShipmentDetail = {
      shipper_id_comp : companyId,
      id : shipmentId,
      order_number : orderNumber
    }
  }else{
    queryShipmentDetail = {
      id : shipmentId,
      order_number : orderNumber
    }
  }
  if(orderStat === 'BOOK' || orderStat === 'CONFIRM' || orderStat === 'DISPATCH' || orderStat === 'DISCHARGE'){
    mySQL.sequelize.transaction( (t) => {
      return mySQL.t_shipment.findOne({
        attributes : [
          ['shipper_id_comp','shipperId'],
          ['vendor_id', 'vendorId'],
          ['order_stat', 'orderStat'],
          'updated_at'
        ],
        where : queryShipmentDetail
      }).then( (shipment) => {
        return shipment.updateAttributes({
          id : shipmentId,
          shipper_id_comp : shipment.dataValues.shipperId,
          vendor_id : shipment.dataValues.vendorId,
          order_number : orderNumber,
          order_stat : orderStat,
          updated_at : timestamp
        }, {transaction : t}).then( (newShipmentDetail) => {
          tempShipmentDetail = newShipmentDetail;
          const historyValue = {
              id_shipment : shipmentId,
              order_number : orderNumber,
              created_by_comptp : comptp,
              created_by_id_comp : companyId,
              created_by_id_user : companyUserId,
              created_by_username : companyUserUsername,
              action : HISTORY_ACTION.ORDER_STATUS.UPDATE,
              description : `update order status from ${ shipment.dataValues.orderStat } to ${ orderStat }`,
              created_at : timestamp
            }
          return mySQL.t_shipmenthistory.create(historyValue,{transaction : t})
        })
      })
    }).then( (transactionResult) => {
      const { id, order_number, order_stat, updated_at, shipper_id_comp, vendor_id } = tempShipmentDetail;
      const response = Object.assign({}, {
        shipmentId : id,
        orderNumber : order_number,
        orderStat : order_stat,
        updatedAt : updated_at
      })

      //return action creator to client
      const rooms = [`${shipper_id_comp}`, `${vendor_id}`, `${1}`]
      emitToRooms(rooms, WEBSOCKET.RECEIVE_WEBSOCKET_UPDATE_SHIPMENT_ORDER_STAT, { response })
      res.status(HTTP_STATUS.OK).json(response);
    }).catch( (err) => {
      console.log(err);
      res.status(HTTP_STATUS.SERVER_ERROR).json({message : HTTP_MESSAGE.SERVER_ERROR});
    })
  } else if ( orderStat === 'PAYMENT') {
    const { invoiceCurrencyRate, invoicePPN, invoiceDueDate, invoiceNote } = req.body;
    mySQL.sequelize.transaction((t) => {
      return mySQL.t_shipment.findOne({
        attributes : [
          ['shipper_id_comp','shipperId'],
          ['vendor_id', 'vendorId'],
          ['order_stat', 'orderStat'],
          ['book_price_currency_code', 'bookPriceCurrencyCode'],
          'updated_at'
        ],
        where : queryShipmentDetail
      }).then( (shipment) => {
        return shipment.updateAttributes({
          id : shipmentId,
          shipper_id_comp : shipment.dataValues.shipperId,
          vendor_id : shipment.dataValues.vendorId,
          order_number : orderNumber,
          order_stat : orderStat,
          is_invoice_issued : true,
          updated_at : timestamp,
        },{transaction : t}).then( (newShipmentDetail) => {
          tempShipmentDetail = newShipmentDetail;
          let totalNewPrices = 0;
          return mySQL.t_shipmentprice.sum('book_price_sum', {
            where :
            { id_shipment : shipmentId,
              order_number : orderNumber
            }
          }).then( (totalOldPrices) => {
            totalNewPrices += totalOldPrices;
            return mySQL.t_shipmentinsurance.sum('price_sum',{
              where : {
                id_shipment : shipmentId,
                order_number : orderNumber
              }
            }).then( (totalInsurancePrice) => {
              if(!isNaN(totalInsurancePrice)){
                totalNewPrices += totalInsurancePrice;
              }
              const convertedInvoiceCurrencyRate = parseFloat(invoiceCurrencyRate.replace(/[^0-9\.-]+/g,""),2);
              let total_gross, total_ppn, total_grand;
              if(shipment.dataValues.bookPriceCurrencyCode !== 'IDR'){
                totalNewPrices = totalNewPrices*convertedInvoiceCurrencyRate;
              }
              total_gross = totalNewPrices;
              total_ppn = totalNewPrices*parseFloat(invoicePPN,2)/100;
              total_grand = total_gross + total_ppn;

              const invoiceValue = {
                id_shipment : shipmentId,
                order_number : orderNumber,
                date : moment().format('YYYY-MM-DD'),
                due_date : moment(invoiceDueDate,'DD/MM/YY').format('YYYY-MM-DD'),
                currency_rate : convertedInvoiceCurrencyRate,
                currency_from_code : shipment.dataValues.bookPriceCurrencyCode ,
                ppn : invoicePPN,
                total_gross,
                total_ppn,
                total_grand,
                is_paid : false,
                is_valid : true,
                note : invoiceNote,
                created_at : timestamp,
                created_by_comptp : comptp,
                created_by_id_user : companyUserId,
                created_by_id_comp : companyId
              }
              return mySQL.t_shipmentinvoice.create(invoiceValue, {transaction : t}).then((shipmentInvoice) => {
                const historyValues = [{
                    id_shipment : shipmentId,
                    order_number : orderNumber,
                    created_by_comptp : comptp,
                    created_by_id_comp : companyId,
                    created_by_id_user : companyUserId,
                    created_by_username : companyUserUsername,
                    action : HISTORY_ACTION.ORDER_STATUS.UPDATE,
                    description : `update order status from ${ shipment.dataValues.orderStat } to ${ orderStat }`,
                    created_at : timestamp
                  },{
                    id_shipment : shipmentId,
                    order_number : orderNumber,
                    created_by_comptp : comptp,
                    created_by_id_comp : companyId,
                    created_by_id_user : companyUserId,
                    created_by_username : companyUserUsername,
                    action : HISTORY_ACTION.INVOICE.ADD,
                    description : `issued invoice with total of IDR ${invoiceValue.total_grand}`,
                    created_at : timestamp
                  }]
                return mySQL.t_shipmenthistory.bulkCreate(historyValues,{transaction : t})
                })
              })
            })
          })
        })
    }).then( (transactionResult) => {
      /* */
      mySQL.sequelize.transaction( (t) => {
        return mySQL.t_shipmentinvoice.findAll({
          attributes : ['id'],
          where : {
            id_shipment : shipmentId,
            order_number : orderNumber,
            is_valid : true
          },
          order : [['created_at', 'DESC']]
        }).then((shipmentInvoices) => {
          //update another invoice which are no longer valid and update the new one as valid invoice
          let newestShipmentInvoiceId;
          const updateInvoicePromises = shipmentInvoices.map( (_shipmentInvoice, index) => {
            let updatedInvoiceValues;
            if(index === 0){
              newestShipmentInvoiceId = _shipmentInvoice.id;
              invoiceNumber = `INV/${moment().format('YYMMDD')}/${_shipmentInvoice.id}`;
              updatedInvoiceValues = {
                id : _shipmentInvoice.id,
                invoice_number : invoiceNumber
              }
            }else{
              updatedInvoiceValues = {
                id : _shipmentInvoice.id,
                is_valid : false,
                updated_at : timestamp
              }
            }
            return _shipmentInvoice.updateAttributes(updatedInvoiceValues, {transaction : t});
          })
          return mySQL.Sequelize.Promise.all(updateInvoicePromises).then( (updatedShipmentInvoices) => {
            return mySQL.t_shipment.findOne({
              attributes : [
                ['id', 'shipmentId'],
                ['order_number', 'orderNumber'],
                ['order_stat', 'orderStat'],
              ],
              where : {
                id : shipmentId,
                order_number : orderNumber
              }
            }).then( (shipment) => {
              return shipment.updateAttributes({
                id : shipmentId,
                id_invoice : newestShipmentInvoiceId,
                invoice_duedate : invoiceDueDate,
                invoice_number : invoiceNumber,
                updated_at : timestamp
              }, {transaction : t})
            })
          })
        })
      }).then( (secondTransactionResult) => {
        const { id, shipper_id_comp, vendor_id, order_number, order_stat, updated_at } = tempShipmentDetail;
        const response = Object.assign({}, {
          shipmentId : id,
          invoiceNumber : invoiceNumber,
          invoiceDueDate : invoiceDueDate,
          orderNumber : order_number,
          orderStat : order_stat,
          updatedAt : updated_at
        })
        const rooms = [`${shipper_id_comp}`, `${vendor_id}`, `${1}`]
        emitToRooms(rooms, WEBSOCKET.RECEIVE_WEBSOCKET_UPDATE_SHIPMENT_ORDER_STAT, { response })
        res.status(HTTP_STATUS.OK).json(response);
      }).catch( (err) => {
        //error of 2nd transaction
        console.log(err);
        res.status(HTTP_STATUS.SERVER_ERROR).json({message : HTTP_MESSAGE.SERVER_ERROR});
      })
    }).catch( (err) => {
      //error of 1st transaction
      console.log(err);
      res.status(HTTP_STATUS.SERVER_ERROR).json({message : HTTP_MESSAGE.SERVER_ERROR});
    })

  } else if ( orderStat === 'COMPLETE') {
    mySQL.sequelize.transaction((t) => {
      return  mySQL.t_shipment.findOne({
        attributes : [
          ['shipper_id_comp', 'shipperId'],
          ['vendor_id', 'vendorId'],
          ['order_stat', 'orderStat'],
          ['book_price_currency_code', 'bookPriceCurrencyCode'],
          'updated_at'
        ],
        where : queryShipmentDetail
      }).then( (shipment) => {
        return shipment.updateAttributes({
          id : shipmentId,
          shipper_id_comp : shipment.dataValues.shipperId,
          vendor_id : shipment.dataValues.vendorId,
          order_number : orderNumber,
          order_stat : orderStat,
          is_invoice_paid : true,
          updated_at : timestamp,
        }, {transaction : t}).then((newShipmentDetail) => {
          tempShipmentDetail = newShipmentDetail;
          return mySQL.t_shipmentinvoice.findOne({
            attributes : [
              ['id', 'invoiceId'],
              ['total_grand', 'totalGrand'],
              ['is_paid', 'isPaid']
            ],
            where : {
              id_shipment : shipmentId,
              order_number : orderNumber,
              is_valid : true
            },
            order : [['created_at', 'DESC']]
          }).then( (shipmentInvoice) => {
            return shipmentInvoice.updateAttributes({
              id : shipmentInvoice.dataValues.invoiceId,
              is_paid : true,
              update_at : timestamp
            },{transaction : t}).then( (newShipmentInvoice) => {
              const { payments } = req.body;
              const totalPayments = _.reduce(payments, (totalPayments, payment) => {
                const { paymentTotalPaid } = payment;
                const convertedPaymentTotalPaid = parseFloat(paymentTotalPaid.replace(/[^0-9\.-]+/g,""),2);
                return totalPayments + convertedPaymentTotalPaid;
              },0)
              if(totalPayments >= parseFloat(shipmentInvoice.dataValues.totalGrand,2)){
                return mySQL.t_andalinbankacc.findAll({
                  attributes : [
                    ['id', 'bankId'],
                    ['name', 'name'],
                    ['bank', 'bank'],
                    ['branch', 'branch'],
                    ['number', 'number']
                  ],
                  where : {
                    is_active : true
                  }
                }).then( (andalinBank) => {
                  const banks = _.reduce(andalinBank, (banks,bank) => {
                    const { bankId } = bank.dataValues;
                    banks[bankId] = Object.assign({}, bank.dataValues)
                    return banks
                  },{});
                  const paymentValues = _.reduce(payments, (paymentValues, payment) => {
                    const { paymentToBank, paymentTotalPaid, paymentCurrencyCode, paymentMethod, paymentDate } = payment;
                    const normalizedTotalPaid = parseFloat(paymentTotalPaid.replace(/[^0-9\.-]+/g,""),2)
                    return paymentValues.concat({
                      id_invoice : shipmentInvoice.dataValues.invoiceId,
                      id_shipment : shipmentId,
                      order_number : orderNumber,
                      method : payment.paymentMethod,
                      andalin_bank_name : banks[paymentToBank].bank,
                      andalin_bank_account_name : banks[paymentToBank].name,
                      andalin_bank_account_number : banks[paymentToBank].number,
                      shipper_bank_name : payment.shipperBankName,
                      shipper_bank_account_name :  payment.shipperBankAccName,
                      shipper_bank_account_number :  payment.shipperBankAccNumber,
                      payment_reference_number :  payment.paymentReferenceNumber,
                      payment_date : moment(payment.paymentDate, 'DD/MM/YY').format('YYYY-MM-DD'),
                      payment_currency_code :  payment.paymentCurrencyCode,
                      payment_total :  normalizedTotalPaid,
                      created_at : timestamp,
                      updated_at : null,
                      created_by_comptp : comptp,
                      created_by_id_user : companyUserId,
                      created_by_id_comp : companyId
                    })
                  },[])
                  return mySQL.t_shipmentpayment.bulkCreate(paymentValues, { transaction : t}).then( (shipmentPayments) => {
                    const historyValues = [{
                        id_shipment : shipmentId,
                        order_number : orderNumber,
                        created_by_comptp : comptp,
                        created_by_id_comp : companyId,
                        created_by_id_user : companyUserId,
                        created_by_username : companyUserUsername,
                        action : HISTORY_ACTION.ORDER_STATUS.UPDATE,
                        description : `update order status from ${ shipment.dataValues.orderStat } to ${ orderStat }`,
                        created_at : timestamp
                      },{
                        id_shipment : shipmentId,
                        order_number : orderNumber,
                        created_by_comptp : comptp,
                        created_by_id_comp : companyId,
                        created_by_id_user : companyUserId,
                        created_by_username : companyUserUsername,
                        action : HISTORY_ACTION.PAYMENT.ADD,
                        description : `confirmed ${paymentValues.length} payment record(s)`,
                        created_at : timestamp
                      }]
                    return mySQL.t_shipmenthistory.bulkCreate(historyValues,{transaction : t})
                  })
                });
              }else{
                t.rollback();
                res.status(HTTP_STATUS.SERVER_ERROR).json({
                  message : HTTP_MESSAGE.PAYMENT_IS_LESSER
                })
              }
            })
          })
        })
      });
    }).then((transactionResult) => {
      const { shipper_id_comp, vendor_id, order_stat, updated_at } = tempShipmentDetail;
      const response = Object.assign({}, {
        shipmentId : shipmentId,
        orderNumber : orderNumber,
        orderStat : order_stat,
        updatedAt : updated_at
      })
      const rooms = [`${shipper_id_comp}`, `${vendor_id}`, `${1}`]
      emitToRooms(rooms, WEBSOCKET.RECEIVE_WEBSOCKET_UPDATE_SHIPMENT_ORDER_STAT, { response })
      res.status(HTTP_STATUS.OK).json(response);
    }).catch( (err) => {
      console.log(err);
      res.status(HTTP_STATUS.SERVER_ERROR).json({message : HTTP_MESSAGE.SERVER_ERROR});
    })
  } else if ( orderStat === 'SCHEDULED') {
    const { schedules } = req.body;
    const combinedSchedules = _.reduce(schedules, (combinedSchedules, schedule, index) => {
      const { portOpenTime, portCutoffTime, inlandCutoffTime, dateArrival, dateDeparture, originCountry, originCity, pol, pod, destinationCountry, destinationCity, vesselName } = schedule;
      return combinedSchedules.concat({
        id_schedule_head : 0,
        id_shipment : shipmentId,
        order_number : orderNumber,
        date_arrival : moment(dateArrival, 'DD/MM/YY').format('YYYY-MM-DD'),
        date_departure : moment(dateDeparture,'DD/MM/YY').format('YYYY-MM-DD'),
        port_open_time : index === 0 ? moment(portOpenTime,'DD/MM/YY HH:mm').format('YYYY-MM-DD hh:mm:ss') : null,
        port_cutoff_time : index === 0 ? moment(portCutoffTime,'DD/MM/YY HH:mm').format('YYYY-MM-DD hh:mm:ss') : null,
        inland_cutoff_time : index === 0 ? moment(inlandCutoffTime, 'DD/MM/YY HH:mm').format('YYYY-MM-DD hh:mm:ss') : null,
        vessel_name : vesselName,
        pol : pol,
        pod : pod,
        origin_city_code : originCity,
        origin_country_code : originCountry.value,
        destination_city_code : destinationCity,
        destination_country_code : destinationCountry.value,
        is_valid : true,
        created_at : timestamp,
        updated_at : null,
        created_by_comptp : comptp,
        created_by_id_comp : companyId,
        created_by_id_user : companyUserId
      });
    }, []);
    mySQL.sequelize.transaction( (t) => {
      return mySQL.t_shipmentschedule.bulkCreate(combinedSchedules,{transaction : t}).then((shipmentSchedules) => {
        return mySQL.t_shipment.findOne({
          attributes : [
            ['id', 'shipmentId'],
            ['shipper_id_comp', 'shipperId'],
            ['vendor_id', 'vendorId'],
            ['order_number', 'orderNumber'],
            ['order_stat', 'orderStat'],
            ['pol','pol'],
            ['pod','pod']
          ],
          where : {
            id : shipmentId,
            order_number : orderNumber
          }
        }).then( (shipment) => {
          return shipment.updateAttributes({
            id : shipmentId,
            shipper_id_comp : shipment.dataValues.shipperId,
            vendor_id : shipment.dataValues.vendorId,
            order_number : orderNumber,
            order_stat : orderStat,
            pol : combinedSchedules[0].pol,
            pod : combinedSchedules[combinedSchedules.length - 1].pod,
            updated_at : timestamp
          },{transaction : t}). then( (newShipmentDetail) => {
            tempShipmentDetail = newShipmentDetail;
            const historyValues = [{
                id_shipment : shipmentId,
                order_number : orderNumber,
                created_by_comptp : comptp,
                created_by_id_comp : companyId,
                created_by_id_user : companyUserId,
                created_by_username : companyUserUsername,
                action : HISTORY_ACTION.ORDER_STATUS.UPDATE,
                description : `update order status from ${ shipment.dataValues.orderStat } to ${ orderStat }`,
                created_at : timestamp
              },{
                id_shipment : shipmentId,
                order_number : orderNumber,
                created_by_comptp : comptp,
                created_by_id_comp : companyId,
                created_by_id_user : companyUserId,
                created_by_username : companyUserUsername,
                action : HISTORY_ACTION.SCHEDULE.ADD,
                description : `post new schedule ${combinedSchedules[0].origin_city_code}/${combinedSchedules[0].origin_country_code}(${combinedSchedules[0].pol}) to ${combinedSchedules[combinedSchedules.length-1].destination_city_code}/${combinedSchedules[combinedSchedules.length-1].destination_country_code}(${combinedSchedules[combinedSchedules.length-1].pod})`,
                created_at : timestamp
              }]
            return mySQL.t_shipmenthistory.bulkCreate(historyValues,{transaction : t})
          })
        })
      })
    }).then((transactionResult) => {
      mySQL.sequelize.transaction( (ts) => {
        return mySQL.t_shipmentschedule.findAll({
          attributes : [
            ['id', 'scheduleId'],
            ['is_valid', 'isValid'],
            ['id_shipment', 'shipmentId'],
            ['id_schedule_head', 'scheduleHeadId'],
            ['created_at','createdAt'],
            ['updated_at', 'updatedAt']
          ],
          where : {
            id_shipment : shipmentId,
            order_number : orderNumber,
            updated_at : null,
            created_at : {
              [mySQL.Sequelize.Op.lt]: timestamp
            }
          },
          order : [ ['created_at', 'DESC']]
        }).then( (oldShipmentSchedules) => {
          if(oldShipmentSchedules.length > 0){
            const updateOldShipmentSchedulesPromises = oldShipmentSchedules.map( (oldShipmentSchedule, index) => {
              return oldShipmentSchedule.updateAttributes({
                id : oldShipmentSchedule.dataValues.scheduleId,
                is_valid : false,
                updated_at : timestamp
              },{transaction : ts})
            })
            return mySQL.Sequelize.Promise.all(updateOldShipmentSchedulesPromises);
          }
        })
      }).then( (transactionResult2) => {
        mySQL.t_shipmentschedule.findAll({
          attributes : [
            ['date_arrival', 'dateArrival'],
            ['date_departure', 'dateDeparture'],
            ['port_open_time', 'portOpenTime'],
            ['port_cutoff_time', 'portCutoffTime'],
            ['inland_cutoff_time', 'inlandCutoffTime'],
            ['vessel_name', 'vesselName'],
            ['pol','pol'],
            ['pod', 'pod'],
            ['origin_city_code','originCityCode'],
            ['origin_country_code', 'originCountryCode'],
            ['destination_city_code', 'destinationCityCode'],
            ['destination_country_code', 'destinationCountryCode'],
            ['is_valid', 'isValid'],
            ['created_at', 'createdAt']
          ],
          where : {
            id_shipment : shipmentId,
            order_number : orderNumber
          }
        }).then((shipmentSchedules) => {
          const { id, shipper_id_comp, vendor_id, order_number, order_stat, updated_at, pol, pod } = tempShipmentDetail;
          const response = Object.assign({}, {
            shipmentId : id,
            orderNumber : order_number,
            orderStat : order_stat,
            updatedAt : updated_at,
            pol : pol,
            pod : pod,
            schedules : _.reduce(shipmentSchedules, (schedules,schedule) => {
              return schedules.concat(schedule.dataValues);
            },[])
          });

          const rooms = [`${shipper_id_comp}`, `${vendor_id}`, `${1}`]
          emitToRooms(rooms, WEBSOCKET.RECEIVE_WEBSOCKET_UPDATE_SHIPMENT_ORDER_STAT, { response })
          res.status(HTTP_STATUS.OK).json(response);
        })
      }).catch( (err) => {
        //error of 2nd transaction
        console.log(err);
        res.status(HTTP_STATUS.SERVER_ERROR).json({message : HTTP_MESSAGE.SERVER_ERROR});
      })
    }).catch( (err) => {
      //error of 1st transaction
      console.log(err);
      res.status(HTTP_STATUS.SERVER_ERROR).json({message : HTTP_MESSAGE.SERVER_ERROR});
    })
  }
}

exports.editShipmentDetailCommodities = (req,res) => {
  const shipmentId  = parseInt(req.params.shipmentId, 0);
  const orderNumber = req.params.orderNumber.split('-').join('/');
  const { companyId, codeusertype, companyUserId, companyUserUsername } = req.user;
  const { commoditiesClass, commoditiesType, commoditiesHSCode, commoditiesProduct } = req.body;
  let queryShipmentDetail;
  let tempShipmentDetail;
  const comptp = getCompTp(codeusertype);
  if( comptp === COMPANY_STATUS.vendor){
    queryShipmentDetail = {
      vendor_id : companyId,
      id : shipmentId,
      order_number : orderNumber
    }
  }else if( comptp === COMPANY_STATUS.shipper){
    queryShipmentDetail = {
      shipper_id_comp : companyId,
      id : shipmentId,
      order_number : orderNumber
    }
  }else{
    queryShipmentDetail = {
      id : shipmentId,
      order_number : orderNumber
    }
  }
  mySQL.sequelize.transaction((t) => {
    return mySQL.t_shipment.findOne({
      attributes : [
        ['shipper_id_comp', 'shipperId'],
        ['vendor_id', 'vendorId'],
        ['commodities_class', 'commoditiesClass'],
        ['commodities_type', 'commoditiesType'],
        ['commodities_hscode', 'commoditiesHSCode'],
        ['commodities_product', 'commoditiesProduct'],
        'updated_at'
      ],
      where : queryShipmentDetail
    }).then( (shipment) => {
      return shipment.updateAttributes({
        id : shipmentId,
        shipper_id_comp : shipment.dataValues.shipperId,
        vendor_id : shipment.dataValues.vendorId,
        order_number : orderNumber,
        commodities_class : commoditiesClass,
        commodities_type : commoditiesType,
        commodities_hscode : commoditiesHSCode,
        commodities_product : commoditiesProduct,
        updated_at : moment().format('YYYY-MM-DD hh:mm:ss')
      }, {transaction : t}).then( (newShipmentDetail) => {
        tempShipmentDetail = newShipmentDetail;
        const historyValue = {
            id_shipment : shipmentId,
            order_number : orderNumber,
            created_by_comptp : comptp,
            created_by_id_comp : companyId,
            created_by_id_user : companyUserId,
            created_by_username : companyUserUsername,
            action : HISTORY_ACTION.COMMODITIES.UPDATE,
            description : `update commodities detail from ${shipment.dataValues.commoditiesClass}/${shipment.dataValues.commoditiesType}/${shipment.dataValues.commoditiesHSCode} to ${newShipmentDetail.commodities_class}/${newShipmentDetail.commodities_type}/${newShipmentDetail.commodities_hscode}`,
            created_at : moment().format('YYYY-MM-DD hh:mm:ss')
          }
        return mySQL.t_shipmenthistory.create(historyValue,{transaction : t})
      })
    })
  }).then((transactionResult) => {
    const { id, shipper_id_comp, vendor_id, order_number, commodities_class, commodities_product, commodities_type, commodities_hscode, updated_at } = tempShipmentDetail;
    const response = Object.assign({}, {
      shipmentId : id,
      orderNumber : order_number,
      commoditiesClass : commodities_class,
      commoditiesType : commodities_type,
      commoditiesHSCode : commodities_hscode,
      commoditiesProduct : commodities_product,
      updatedAt : updated_at
    })

    const rooms = [`${shipper_id_comp}`, `${vendor_id}`, `${1}`]
    emitToRooms(rooms, WEBSOCKET.RECEIVE_WEBSOCKET_UPDATE_SHIPMENT_COMMODITIES, { response })
    res.status(HTTP_STATUS.OK).json(response);
  }).catch( (err) => {
    console.log(err);
    res.status(HTTP_STATUS.SERVER_ERROR).json({message : HTTP_MESSAGE.SERVER_ERROR});
  })

}

exports.editShipmentDetailCarrier = (req,res) => {
  const shipmentId  = parseInt(req.params.shipmentId, 0);
  const orderNumber = req.params.orderNumber.split('-').join('/');
  const { companyId, codeusertype, companyUserId, companyUserUsername } = req.user;
  const { carrierName, carrierBlAwb, carrierContract } = req.body;
  let queryShipmentDetail;
  let tempShipmentDetail;
  const comptp = getCompTp(codeusertype);
  if( comptp === COMPANY_STATUS.vendor){
    queryShipmentDetail = {
      vendor_id : companyId,
      id : shipmentId,
      order_number : orderNumber
    }
  }else if( comptp === COMPANY_STATUS.shipper){
    queryShipmentDetail = {
      shipper_id_comp : companyId,
      id : shipmentId,
      order_number : orderNumber
    }
  }else{
    queryShipmentDetail = {
      id : shipmentId,
      order_number : orderNumber
    }
  }
  mySQL.sequelize.transaction((t) => {
    return mySQL.t_shipment.findOne({
      attributes : [
        ['shipper_id_comp', 'shipperId'],
        ['vendor_id', 'vendorId'],
        ['carrier_bl_awb', 'carrierBlAwb'],
        ['carrier_name', 'carrierName'],
        ['carrier_contract', 'carrierContract'],
        'updated_at'
      ],
      where : queryShipmentDetail
    }).then( (shipment) => {
      return shipment.updateAttributes({
        id : shipmentId,
        shipper_id_comp : shipment.dataValues.shipperId,
        vendor_id : shipment.dataValues.vendorId,
        order_number : orderNumber,
        carrier_bl_awb : carrierBlAwb,
        carrier_name : carrierName.toUpperCase(),
        carrier_contract : carrierContract,
        updated_at : moment().format('YYYY-MM-DD hh:mm:ss')
      }, {transaction : t}).then( (newShipmentDetail) => {
        tempShipmentDetail = newShipmentDetail;
        const historyValue = {
            id_shipment : shipmentId,
            order_number : orderNumber,
            created_by_comptp : comptp,
            created_by_id_comp : companyId,
            created_by_id_user : companyUserId,
            created_by_username : companyUserUsername,
            action : HISTORY_ACTION.CARRIER.UPDATE,
            description : `update carrier detail from ${shipment.dataValues.carrierName}/${shipment.dataValues.carrierBlAwb}/${shipment.dataValues.carrierContract} to ${newShipmentDetail.carrier_name}/${newShipmentDetail.carrier_bl_awb}/${newShipmentDetail.carrier_contract}`,
            created_at : moment().format('YYYY-MM-DD hh:mm:ss')
          }
        return mySQL.t_shipmenthistory.create(historyValue,{transaction : t})
      })
    })
  }).then( (transactionResult) => {
    const { id, shipper_id_comp, vendor_id, order_number, carrier_name, carrier_bl_awb, carrier_contract, updated_at } = tempShipmentDetail;
    const response = Object.assign({}, {
      shipmentId : id,
      orderNumber : order_number,
      carrierBlAwb : carrier_bl_awb,
      carrierContract : carrier_contract,
      carrierName : carrier_name,
      updatedAt : updated_at
    })

    const rooms = [`${shipper_id_comp}`, `${vendor_id}`, `${1}`]
    emitToRooms(rooms, WEBSOCKET.RECEIVE_WEBSOCKET_UPDATE_SHIPMENT_CARRIER, { response })
    res.status(HTTP_STATUS.OK).json(response);
  }).catch( (err) => {
    console.log(err);
    res.status(HTTP_STATUS.SERVER_ERROR).json({message : HTTP_MESSAGE.SERVER_ERROR});
  })
}

exports.editShipmentDetailFreight = (req,res) => {
  const shipmentId  = parseInt(req.params.shipmentId, 0);
  const orderNumber = req.params.orderNumber.split('-').join('/');
  const { companyId, codeusertype, companyUserId, companyUserUsername } = req.user;
  const { vendorId, freights } = req.body;
  let queryShipmentDetail;
  let shipperId;
  const comptp = getCompTp(codeusertype);
  const freightsIds = freights.reduce( (freightsIds, freight) => {
    return freightsIds.concat(freight.freightId);
  },[]);
  if( comptp === COMPANY_STATUS.vendor){
    queryShipmentDetail = {
      id_shipment : shipmentId,
      order_number : orderNumber,
      id : { $in : freightsIds}
    }
  }else if( comptp === COMPANY_STATUS.shipper){
    queryShipmentDetail = {
      id_shipment : shipmentId,
      order_number : orderNumber,
      id : { $in : freightsIds}
    }
  }else{
    queryShipmentDetail = {
      id_shipment : shipmentId,
      order_number : orderNumber,
      id : { $in : freightsIds}
    }
  }
  mySQL.sequelize.transaction( (t) => {
    return mySQL.t_shipmentfreight.findAll({
      attributes : [
        ['id', 'freightId'],
        ['tracking_number', 'trackingNumber'],
        ['order_number', 'orderNumber'],
        ['qty', 'qty'],
        ['volume','volume'],
        ['service_type', 'serviceType'],
        ['additional_info', 'additionalInfo'],
        ['weight', 'weight'],
        ['created_by_id_comp', 'shipperId'],
        'updated_at'
      ],
      where : queryShipmentDetail
    }).then( (shipmentFreights) => {
      shipperId = shipmentFreights[0].dataValues.shipperId
      const updatePromises = shipmentFreights.map( (shipmentFreight,index) => {
        const { trackingNumber, qty, volume, weight, serviceType, freightId, additionalInfo} = freights[index];
        return shipmentFreight.updateAttributes({
          id : freightId,
          qty : qty,
          volume : volume,
          weight : weight,
          service_type : serviceType,
          tracking_number : trackingNumber,
          additional_info : additionalInfo,
          updated_at : moment().format('YYYY-MM-DD hh:mm:ss')
        }, { transaction : t})
      });
      return mySQL.Sequelize.Promise.all(updatePromises).then( (newShipmentFreights) => {
        tempShipmentFreights = newShipmentFreights;
        const historyValue = {
            id_shipment : shipmentId,
            order_number : orderNumber,
            created_by_comptp : comptp,
            created_by_id_comp : companyId,
            created_by_id_user : companyUserId,
            created_by_username : companyUserUsername,
            action : HISTORY_ACTION.FREIGHT.UPDATE,
            description : `update ${newShipmentFreights.length} freight(s)`,
            created_at : moment().format('YYYY-MM-DD hh:mm:ss')
          }
        return mySQL.t_shipmenthistory.create(historyValue,{transaction : t})
      })
    })
  }).then((transactionResult) => {
    const freights = tempShipmentFreights.reduce( (response, newFreight) => {
      return response.concat({
        freightId : newFreight.id,
        trackingNumber : newFreight.tracking_number,
        qty : newFreight.qty,
        volume : newFreight.volume,
        weight : newFreight.weight,
        serviceType : newFreight.service_type,
        additionalInfo : newFreight.additional_info
      })
    },[]);
    const rooms = [`${shipperId}`, `${vendorId}`, `${1}`]
    emitToRooms(rooms, WEBSOCKET.RECEIVE_WEBSOCKET_UPDATE_SHIPMENT_FREIGHTS, { response : {
      shipmentId,
      freights }})
    res.status(HTTP_STATUS.OK).json(freights);
  }).catch( (err) => {
    console.log(err);
    res.status(HTTP_STATUS.SERVER_ERROR).json({message : HTTP_MESSAGE.SERVER_ERROR});
  })
}
const getRequestUnit = (type) => {
  if(type === 'min_temperature' || type === 'max_temperature'){
    return 'Celcius'
  }else if(type === 'free_time') {
    return 'Days'
  }
}

exports.editShipmentDetailRequest = (req,res) => {
  const shipmentId  = parseInt(req.params.shipmentId, 0);
  const orderNumber = req.params.orderNumber.split('-').join('/');
  const { companyId, codeusertype, companyUserId, companyUserUsername } = req.user;
  const { requests, vendorId } = req.body;
  const comptp = getCompTp(codeusertype);
  let queryShipmentDetail;

  if(comptp === COMPANY_STATUS.shipper){

    const newRequests = requests.reduce( (newRequests, request) => {
      const _request = {
        id_shipment : shipmentId,
        order_number : orderNumber,
        shipper_id_comp : companyId,
        shipper_id : companyUserId,
        type : request.type,
        value : request.value,
        unit : getRequestUnit(request.type),
        stat : 'U',
        note : request.note,
        created_by_comptp : 'USER',
        created_by_id_comp : companyId,
        created_by_id_user : companyUserId,
        created_at : moment().format('YYYY-MM-DD hh:mm:ss')
      }
      return request.requestId === 0 ? newRequests.concat(_request) : newRequests;
    }, []);
    mySQL.sequelize.transaction((t) => {
      return mySQL.t_shipmentrequest.bulkCreate(newRequests).then((newRequestsResult) => {
        const historyValue = {
          id_shipment : shipmentId,
          order_number : orderNumber,
          created_by_comptp : comptp,
          created_by_id_comp : companyId,
          created_by_id_user : companyUserId,
          created_by_username : companyUserUsername,
          action : HISTORY_ACTION.REQUEST.ADD,
          description : `add ${newRequests.length} request(s)`,
          created_at : moment().format('YYYY-MM-DD hh:mm:ss')
        }
        return mySQL.t_shipmenthistory.create(historyValue,{transaction : t})
      })
    }).then((transactionResult) => {
      queryShipmentDetail = {
        id_shipment : shipmentId,
        order_number : orderNumber
      }
      mySQL.t_shipmentrequest.findAll({
        attributes : [
                    ['id', 'requestId'],
                    ['id_shipment', 'shipmentId'],
                    ['shipper_id_comp', 'shipmentIdCompany'],
                    ['order_number', 'orderNumber'],
                    ['type', 'type'],
                    ['value', 'value'],
                    ['unit', 'unit'],
                    ['stat', 'stat'],
                    ['note', 'note'],
                    ['created_at', 'createdAt'],
                    ['created_by_comptp', 'createdByComptp'],
                    ['created_by_id_comp', 'createdByIdCompany'],
                    ['created_by_id_user', 'createdByIdUser']
                  ],
        where : queryShipmentDetail,
        order : [['created_at', 'ASC']]
      }).then( (requests) => {
        const rooms = [`${companyId}`, `${vendorId}`, `${1}`]
        emitToRooms(rooms, WEBSOCKET.RECEIVE_WEBSOCKET_UPDATE_SHIPMENT_REQUESTS, { response : {
          shipmentId,
          requests }})
        res.status(HTTP_STATUS.OK).json(requests);
      }).catch( (err) => {
        console.log(err);
        res.status(HTTP_STATUS.SERVER_ERROR).json({message : HTTP_MESSAGE.SERVER_ERROR});
      })
    })

  }else{
    let shipperId;
    const requestsIds = requests.reduce( (requestsIds, request) => {
      return requestsIds.concat(request.requestId);
    },[]);

    if( comptp === COMPANY_STATUS.vendor){

      queryShipmentDetail = {
        id_shipment : shipmentId,
        order_number : orderNumber,
        id : { $in : requestsIds }
      }
    } else {
      queryShipmentDetail = {
        id_shipment : shipmentId,
        order_number : orderNumber,
        id : { $in : requestsIds }
      }
    }
    mySQL.sequelize.transaction((t) => {
      return mySQL.t_shipmentrequest.findAll({
        attributes : [
          ['id', 'requestId'],
          ['shipper_id_comp','shipperId'],
          ['id_shipment', 'shipmentId'],
          ['order_number', 'orderNumber'],
          ['type', 'type'],
          ['value', 'value'],
          ['unit', 'unit'],
          ['stat', 'stat'],
          ['note', 'note'],
          'updated_at'
        ],
        where : queryShipmentDetail
      }).then( (shipmentRequests) => {
        shipperId = shipmentRequests[0].dataValues.shipperId
        const updatePromises = shipmentRequests.map( (shipmentRequest,index) => {
          const { requestId, type, value, note, stat } = requests[index];
          return shipmentRequest.updateAttributes({
            id : requestId,
            id_shipment : shipmentId,
            order_number : orderNumber,
            type : type,
            value : value,
            note : note,
            stat : stat,
            unit : shipmentRequests[index].dataValues.unit,
            updated_at : moment().format('YYYY-MM-DD hh:mm:ss')
          }, { transaction : t})
        });
        return mySQL.Sequelize.Promise.all(updatePromises).then((updatedShipmentRequests) => {
          const historyValue = {
            id_shipment : shipmentId,
            order_number : orderNumber,
            created_by_comptp : comptp,
            created_by_id_comp : companyId,
            created_by_id_user : companyUserId,
            created_by_username : companyUserUsername,
            action : HISTORY_ACTION.REQUEST.UPDATE,
            description : `update ${updatedShipmentRequests.length} request(s)`,
            created_at : moment().format('YYYY-MM-DD hh:mm:ss')
          }
          return mySQL.t_shipmenthistory.create(historyValue,{transaction : t})
        })
      })
    }).then( (transactionResult) => {
      queryShipmentDetail = {
        id_shipment : shipmentId,
        order_number : orderNumber
      }
      mySQL.t_shipmentrequest.findAll({
        attributes : [
                    ['id', 'requestId'],
                    ['id_shipment', 'shipmentId'],
                    ['shipper_id_comp', 'shipmentIdCompany'],
                    ['order_number', 'orderNumber'],
                    ['type', 'type'],
                    ['value', 'value'],
                    ['unit', 'unit'],
                    ['stat', 'stat'],
                    ['note', 'note'],
                    ['created_at', 'createdAt'],
                    ['created_by_comptp', 'createdByComptp'],
                    ['created_by_id_comp', 'createdByIdCompany'],
                    ['created_by_id_user', 'createdByIdUser']
                  ],
        where : queryShipmentDetail,
        order : [['created_at', 'ASC']]
      }).then( (requests) => {
        const rooms = [`${shipperId}`, `${vendorId}`, `${1}`]
        emitToRooms(rooms, WEBSOCKET.RECEIVE_WEBSOCKET_UPDATE_SHIPMENT_REQUESTS, { response : {
          shipmentId,
          requests } })
        res.status(HTTP_STATUS.OK).json(requests);
      }).catch( (err) => {
        console.log(err);
        res.status(HTTP_STATUS.SERVER_ERROR).json({message : HTTP_MESSAGE.SERVER_ERROR});
      })
    })
  }
}
const getQtyBasedOnFieldAndPriceUOM = (field,qty,priceUOM) => {
  if( field === 'qty'){
    if(priceUOM === 'CONT'){
      return qty
    } else if (priceUOM === 'ALL') {
      return qty
    } else {
      return 0
    }
  }else if(field === 'volume'){
    if(priceUOM === 'V'){
      return qty
    } else {
      return 0
    }
  }else if(field === 'weight'){
    if(priceUOM === 'W'){
      return qty
    } else {
      return 0
    }
  }
}
exports.editShipmentPrice = (req,res) => {
  const shipmentId  = parseInt(req.params.shipmentId, 0);
  const orderNumber = req.params.orderNumber.split('-').join('/');
  const { companyId, codeusertype, companyUserId, companyUserUsername } = req.user;
  const comptp = getCompTp(codeusertype);
  const { prices } = req.body;
  let totalNewPrices = 0;
  let tempTotalOldPricesAndInsurance = 0;
  mySQL.t_shipmentprice.sum('book_price_sum', {
    where :
    { id_shipment : shipmentId,
      order_number : orderNumber
    }
  }).then( (totalOldPrices) => {
    totalNewPrices += totalOldPrices;
    tempTotalOldPricesAndInsurance = totalNewPrices;
    mySQL.t_shipmentinsurance.sum('price_sum',{
      where : {
        id_shipment : shipmentId,
        order_number : orderNumber
      }
    }).then( (totalInsurancePrice) => {
      if(!isNaN(totalInsurancePrice)){
        totalNewPrices += totalInsurancePrice;
        tempTotalOldPricesAndInsurance = totalNewPrices;
      }
      mySQL.sequelize.transaction((t) => {
        const priceComponentsValues = prices.reduce( (priceComponentsValues, priceComponent) => {
          const { bookPriceCurrencyCode, componentType, qty, priceDesc, priceUOM, priceSum } = priceComponent;
          const normalizedPriceSum = parseFloat(priceSum.replace(/[^0-9\.-]+/g,""));
          totalNewPrices += normalizedPriceSum;
          return priceComponentsValues.concat({
            id_shipment : shipmentId,
            id_price : 0,
            order_number : orderNumber,
            component_type : componentType,
            service_type : 'ADDITIONAL',
            price_group_code : '',
            price_group_desc : null,
            price_code : 'ADDITIONAL',
            price_desc : priceDesc,
            qty : getQtyBasedOnFieldAndPriceUOM('qty',qty,priceUOM),
            volume : getQtyBasedOnFieldAndPriceUOM('volume',qty,priceUOM),
            weight : getQtyBasedOnFieldAndPriceUOM('weight',qty,priceUOM),
            price_uom : priceUOM,
            price : 0,
            price_min : 0,
            price_sum : 0,
            price_currency_code : bookPriceCurrencyCode,
            book_price : 0,
            book_price_min : 0,
            book_price_sum : normalizedPriceSum,
            book_price_currency_code : bookPriceCurrencyCode,
            book_price_currency_rate : 0,
            created_at : moment().format('YYYY-MM-DD hh:mm:ss'),
            created_by_comptp : comptp,
            created_by_id_comp : companyId,
            created_by_id_user : companyUserId
          })
        },[]);
        return mySQL.t_shipmentprice.bulkCreate(priceComponentsValues, {transaction : t}).then((newPrice) => {
          const shipmentValue = {
            book_price_total : parseFloat(totalNewPrices,2)
          }
          return mySQL.t_shipment.update(shipmentValue,{
            fields : ['book_price_total'],
            where : {
              id : shipmentId,
              order_number : orderNumber
            },
            transaction : t
          }).then((newShipment) => {
            const historyValue = {
                id_shipment : shipmentId,
                order_number : orderNumber,
                created_by_comptp : comptp,
                created_by_id_comp : companyId,
                created_by_id_user : companyUserId,
                created_by_username : companyUserUsername,
                action : HISTORY_ACTION.PRICE.ADD,
                description : `add ${ priceComponentsValues.length } price components and update total price from ${ priceComponentsValues[0].book_price_currency_code } ${ tempTotalOldPricesAndInsurance } to ${ priceComponentsValues[0].book_price_currency_code } ${ totalNewPrices }`,
                created_at : moment().format('YYYY-MM-DD hh:mm:ss')
              }
            return mySQL.t_shipmenthistory.create(historyValue,{transaction : t})
          })
        })
      }).then((transactionResult) => {
        mySQL.t_shipmentprice.findAll({
          where : {
            id_shipment : shipmentId,
            order_number : orderNumber
          },
          attributes : [
            ['id_shipment', 'shipmentId'],
            ['id_price', 'priceId'],
            ['order_number', 'orderNumber'],
            ['component_type', 'componentType'],
            ['component_city_code', 'componentCityCode'],
            ['service_type', 'serviceType'],
            ['price_group_code', 'priceGroupCode'],
            ['price_group_desc','priceGroupDesc'],
            ['price_code', 'priceCode'],
            ['price_desc', 'priceDesc'],
            ['qty', 'qty'],
            ['volume', 'volume'],
            ['weight', 'weight'],
            ['price_uom', 'priceUOM'],
            ['book_price', 'bookPrice'],
            ['book_price_sum', 'bookPriceSum'],
            ['book_price_min', 'bookPriceMin'],
            ['book_price_currency_code', 'bookPriceCurrencyCode'],
            ['book_price_currency_rate', 'bookPriceCurrencyRate'],
            ['created_at', 'createdAt'],
            ['created_by_comptp', 'createdByComptp'],
            ['created_by_id_comp', 'createdByIdCompany'],
            ['created_by_id_user', 'createdByIdUser']
          ]
        }).then( (shipmentPrices) => {
          const response = Object.assign({}, {
            shipmentId,
            bookPriceTotal : parseFloat(totalNewPrices,2),
            shipmentPrice : _.reduce(shipmentPrices, (shipmentPrice, price) => {
                              return shipmentPrice.concat(price);
                            },[])
          })
          mySQL.t_shipment.findOne({
            attributes : [
              ['vendor_id', 'vendorId'],
              ['shipper_id_comp', 'shipperId']
            ],
            where : {
              id : shipmentId,
              order_number : orderNumber
            }
          }).then((shipment) => {
            const { shipperId, vendorId } = shipment.dataValues
            const rooms = [`${shipperId}`, `${vendorId}`, `${1}`]
            emitToRooms(rooms, WEBSOCKET.RECEIVE_WEBSOCKET_UPDATE_SHIPMENT_ORDER_PRICE, { response })
          })
          res.status(HTTP_STATUS.OK).json(response);
        })
    }).catch((err) => {
      console.log(err);
      res.status(HTTP_STATUS.SERVER_ERROR).json({message : HTTP_MESSAGE.SERVER_ERROR});
    })
  }).catch((err) => {
    console.log(err);
    res.status(HTTP_STATUS.SERVER_ERROR).json({message : HTTP_MESSAGE.SERVER_ERROR});
  })
}).catch((err) => {
  console.log(err);
  res.status(HTTP_STATUS.SERVER_ERROR).json({message : HTTP_MESSAGE.SERVER_ERROR});
})
}
exports.getShipments = (req,res) => {
  const { companyId, codeusertype } = req.user;
  let queryShipment;
  const comptp = getCompTp(codeusertype);

  if( comptp === COMPANY_STATUS.vendor){
    queryShipment = {
      vendor_id : companyId
    }
  }else if( comptp === COMPANY_STATUS.shipper){
    queryShipment = {
      shipper_id_comp : companyId,
    }
  }else{
    queryShipment = null;
  }
  mySQL.t_shipment.findOne({
    attributes : [
      [mySQL.sequelize.fn('COUNT', mySQL.sequelize.col('id')), 'recordLength'],
    ],
    where : queryShipment
  }).then( (totalShipment) => {
    mySQL.t_shipment.findAll({
      attributes : [
        ['id', 'shipmentId'],
        ['order_number', 'orderNumber'],
        ['order_stat', 'orderStat'],
        ['shipper_id_user', 'shipperId'],
        ['shipper_comp', 'shipperCompany'],
        ['freight_mode', 'freightMode'],
        ['freight_type', 'freightType'],
        ['freight_sum', 'freightSummary'],
        ['vendor_name', 'vendorName'],
        ['shipment_date', 'shipmentDate'],
        ['carrier_bl_awb', 'carrierBlAwb'],
        ['carrier_name', 'carrierName'],
        ['origin_country_code', 'originCountryCode'],
        ['origin_city_code', 'originCityCode'],
        ['destination_country_code', 'destinationCountryCode'],
        ['destination_city_code', 'destinationCityCode'],
        ['transittime_min', 'transittimeMin'],
        ['transittime_max', 'transittimeMax'],
        ['created_at', 'createdAt']
      ],
      where : queryShipment,
      order : [['created_at', 'DESC'], ['shipment_date', 'DESC']]
    }).then((shipments) => {
      const results = _.reduce( shipments, (modifiedShipments, shipment) => {
        return modifiedShipments.concat(Object.assign({}, shipment.dataValues, {
          freightSummary : JSON.parse(shipment.dataValues.freightSummary)
        }))
      },[]);
      //const page = parseInt(totalShipment.dataValues.recordLength / limit,0);
      const response = Object.assign({}, {
        //pageLength :  page === 0 ? page + 1 : page,
        recordLength : totalShipment.dataValues.recordLength,
        results
      })
      res.status(HTTP_STATUS.OK).json(response)
    })
  }).catch( (err) => {
    console.log(err);
    res.status(HTTP_STATUS.SERVER_ERROR).json({message : HTTP_MESSAGE.SERVER_ERROR});
  })

}
exports.getShipmentDocuments = (req,res) => {
  const shipmentId  = parseInt(req.params.shipmentId, 0);
  const orderNumber = req.params.orderNumber.split('-').join('/');
  const { companyId, codeusertype } = req.user;
  const queryShipment = {
        id_shipment : shipmentId,
        order_number : orderNumber
      }
  mySQL.t_shipmentfile.findAll({
    attributes : [
      ['id', 'fileId'],
      ['id_shipment', 'shipmentId'],
      ['order_number', 'orderNumber'],
      ['file_category', 'fileCategory'],
      ['file_description', 'fileDesc'],
      ['file_ext', 'fileExt'],
      ['file_origin', 'fileOrigin'],
      ['created_at', 'createdAt'],
      ['created_by_comptp', 'createdByComptp'],
      ['created_by_id_comp', 'createdByIdCompany'],
      ['created_by_id_user', 'createdByIdUser'],
      ['created_by_username', 'createdByUsername'],
    ],
    where : queryShipment,
    order : [['created_at', 'DESC']]
  }).then( (shipmentDocuments) => {
    res.status(HTTP_STATUS.OK).json(shipmentDocuments);
  }).catch( (err) => {
    console.log(err);
    res.status(HTTP_STATUS.SERVER_ERROR).json({message : HTTP_MESSAGE.SERVER_ERROR})
  })
}
exports.getShipmentHistory = (req,res) => {
  const shipmentId  = parseInt(req.params.shipmentId, 0);
  const orderNumber = req.params.orderNumber.split('-').join('/');
  const { companyId, codeusertype } = req.user;
  const queryShipment = {
        id_shipment : shipmentId,
        order_number : orderNumber
      }
  mySQL.t_shipmenthistory.findAll({
    attributes : [
      ['id', 'fileId'],
      ['id_shipment', 'shipmentId'],
      ['order_number', 'orderNumber'],
      ['action', 'action'],
      ['description', 'description'],
      ['created_at', 'createdAt'],
      ['created_by_comptp', 'createdByComptp'],
      ['created_by_id_comp', 'createdByIdCompany'],
      ['created_by_id_user', 'createdByIdUser'],
      ['created_by_username', 'createdByUsername'],
    ],
    where : queryShipment,
    order : [['created_at', 'DESC']]
  }).then( (shipmentHistories) => {
    res.status(HTTP_STATUS.OK).json(shipmentHistories);
  }).catch( (err) => {
    console.log(err);
    res.status(HTTP_STATUS.SERVER_ERROR).json({message : HTTP_MESSAGE.SERVER_ERROR})
  })
}
exports.getShipmentDetail = (req,res) => {
  const shipmentId  = parseInt(req.params.shipmentId, 0);
  const orderNumber = req.params.orderNumber.split('-').join('/');
  const { companyId, codeusertype } = req.user;
  let queryShipment;
  const comptp = getCompTp(codeusertype);

  if( comptp === COMPANY_STATUS.vendor){
    queryShipment = {
      vendor_id : companyId,
      id : shipmentId,
      order_number : orderNumber
    }
  }else if( comptp === COMPANY_STATUS.shipper){
    queryShipment = {
      shipper_id_comp : companyId,
      id : shipmentId,
      order_number : orderNumber
    }
  }else{
    queryShipment = {
      id : shipmentId,
      order_number : orderNumber
    };
  }
  mySQL.t_shipment.findOne({
    attributes : [
                  ['id', 'shipmentId'],
                  ['order_number', 'orderNumber'],
                  ['order_stat', 'orderStat'],
                  ['shipper_name', 'shipperName'],
                  ['shipper_comp', 'shipperCompany'],
                  ['shipper_address', 'shipperAddress'],
                  ['shipper_phone','shipperPhone'],
                  ['shipper_email','shipperEmail'],
                  ['shipper_fax', 'shipperFax'],
                  ['shipper_notes', 'shipperNotes'],
                  ['consignee_name', 'consigneeName'],
                  ['consignee_comp', 'consigneeCompany'],
                  ['consignee_address', 'consigneeAddress'],
                  ['consignee_phone', 'consigneePhone'],
                  ['consignee_email', 'consigneeEmail'],
                  ['consignee_fax', 'consigneeFax'],
                  ['commodities_class', 'commoditiesClass'],
                  ['commodities_type', 'commoditiesType'],
                  ['commodities_hscode', 'commoditiesHSCode'],
                  ['commodities_product', 'commoditiesProduct'],
                  ['freight_mode', 'freightMode'],
                  ['freight_type', 'freightType'],
                  ['freight_direct', 'freightDirect'],
                  ['trucking_stat', 'truckingStat'],
                  ['custom_stat', 'customStat'],
                  ['insurance_stat', 'insuranceStat'],
                  ['additional_request_stat', 'additionalRequestStat'],
                  ['vendor_id', 'vendorId'],
                  ['vendor_name', 'vendorName'],
                  ['shipment_date', 'shipmentDate'],
                  ['price_total', 'priceTotal'],
                  ['price_currency_code', 'priceCurrencyCode'],
                  ['book_price_total', 'bookPriceTotal'],
                  ['book_price_currency_code', 'bookPriceCurrencyCode'],
                  ['book_price_currency_rate', 'bookPriceCurrencyRate'],
                  ['carrier_name', 'carrierName'],
                  ['carrier_bl_awb', 'carrierBlAwb'],
                  ['carrier_contract', 'carrierContract'],
                  ['origin_country', 'originCountry'],
                  ['origin_country_code', 'originCountryCode'],
                  ['origin_city', 'originCity'],
                  ['origin_city_code', 'originCityCode'],
                  'pol',
                  ['destination_country', 'destinationCountry'],
                  ['destination_country_code', 'destinationCountryCode'],
                  ['destination_city', 'destinationCity'],
                  ['destination_city_code', 'destinationCityCode'],
                  'pod',
                  ['transittime_min', 'transittimeMin'],
                  ['transittime_max', 'transittimeMax'],
                  ['invoice_number', 'invoiceNumber'],
                  ['invoice_duedate', 'invoiceDueDate'],
                  ['is_invoice_issued', 'isInvoiceIssued'],
                  ['is_invoice_paid', 'isInvoicePaid'],
                  ['created_at', 'createdAt']
                ],
    where : queryShipment,
    order : [['created_at', 'DESC']]
  }).then((shipment) => {
    const queryRequest = {
      id_shipment : shipmentId,
      order_number : orderNumber
    }
    mySQL.t_shipmentrequest.findAll({
      attributes : [
                  ['id', 'requestId'],
                  ['id_shipment', 'shipmentId'],
                  ['shipper_id_comp', 'shipmentIdCompany'],
                  ['order_number', 'orderNumber'],
                  ['type', 'type'],
                  ['value', 'value'],
                  ['unit', 'unit'],
                  ['stat', 'stat'],
                  ['note', 'note'],
                  ['created_at', 'createdAt'],
                  ['created_by_comptp', 'createdByComptp'],
                  ['created_by_id_comp', 'createdByIdCompany'],
                  ['created_by_id_user', 'createdByIdUser']
                ],
      where : queryRequest,
      order : [['created_at', 'ASC']]
    }).then( (requests) => {
      const queryFreight = {
        id_shipment : shipmentId,
        order_number : orderNumber
      }
      mySQL.t_shipmentfreight.findAll({
        attributes : [
                  ['id', 'freightId'],
                  ['id_shipment', 'shipmentId'],
                  ['order_number', 'orderNumber'],
                  ['service_type', 'serviceType'],
                  ['tracking_number', 'trackingNumber'],
                  ['qty', 'qty'],
                  ['weight', 'weight'],
                  ['volume', 'volume'],
                  ['additional_info', 'additionalInfo'],
                  ['created_at', 'createdAt']
                ],
        where : queryFreight,
        order : [['created_at', 'ASC'], ['service_type', 'ASC']]
      }).then( (freights) => {
        const queryInsurance = {
          id_shipment : shipmentId,
          order_number : orderNumber
        }
        mySQL.t_shipmentinsurance.findAll({
          attributes : [
                    ['id', 'insuranceId'],
                    ['id_shipment', 'shipmentId'],
                    ['id_price', 'priceId'],
                    ['order_number', 'orderNumber'],
                    ['goods_value', 'goodsValue'],
                    ['goods_value_currency', 'goodsValueCurrency'],
                    ['pcg', 'pcg'],
                    ['price_sum', 'priceSum'],
                    ['price_currency_rate', 'priceCurrencyRate'],
                    ['price_currency_code', 'priceCurrencyCode'],
                    ['created_at', 'createdAt']
                  ],
          where : queryInsurance,
          order : [['created_at', 'ASC']]
        }).then((insurances) => {
          mySQL.t_shipmentschedule.findAll({
            attributes : [
              ['date_arrival', 'dateArrival'],
              ['date_departure', 'dateDeparture'],
              ['port_open_time', 'portOpenTime'],
              ['port_cutoff_time', 'portCutoffTime'],
              ['inland_cutoff_time', 'inlandCutoffTime'],
              ['vessel_name', 'vesselName'],
              ['pol','pol'],
              ['pod', 'pod'],
              ['origin_city_code','originCityCode'],
              ['origin_country_code', 'originCountryCode'],
              ['destination_city_code', 'destinationCityCode'],
              ['destination_country_code', 'destinationCountryCode'],
              ['is_valid', 'isValid'],
              ['created_at', 'createdAt']
            ],
            where : {
              id_shipment : shipmentId,
              order_number : orderNumber,
            },
            order : [['created_at', 'DESC'], ['port_cutoff_time','DESC'], ['is_valid', 'DESC']]
          }).then( (schedules) => {
            const response = Object.assign({},shipment.dataValues,{
              requests,
              freights,
              insurances,
              schedules,
            })
            res.status(HTTP_STATUS.OK).json(response)
          }).catch( (err) => {
            console.log(err);
            res.status(HTTP_STATUS.SERVER_ERROR).json({ message : HTTP_MESSAGE.SERVER_ERROR})
          })
        }).catch( (err) => {
          console.log(err);
          res.status(HTTP_STATUS.SERVER_ERROR).json({ message : HTTP_MESSAGE.SERVER_ERROR})
        })
      }).catch( (err) => {
        console.log(err);
        res.status(HTTP_STATUS.SERVER_ERROR).json({ message : HTTP_MESSAGE.SERVER_ERROR})
      })
    }).catch( (err) => {
      console.log(err);
      res.status(HTTP_STATUS.SERVER_ERROR).json({ message : HTTP_MESSAGE.SERVER_ERROR})
    })
  }).catch( (err) => {
    console.log(err);
    res.status(HTTP_STATUS.SERVER_ERROR).json({ message : HTTP_MESSAGE.SERVER_ERROR})
  })
}

exports.getShipmentPrice = (req,res) => {
  const shipmentId  = parseInt(req.params.shipmentId, 0);
  const orderNumber = req.params.orderNumber.split('-').join('/');
  mySQL.t_shipmentprice.findAll({
    where : {
      id_shipment : shipmentId,
      order_number : orderNumber
    },
    attributes : [
      ['id_shipment', 'shipmentId'],
      ['id_price', 'priceId'],
      ['order_number', 'orderNumber'],
      ['component_type', 'componentType'],
      ['component_city_code', 'componentCityCode'],
      ['service_type', 'serviceType'],
      ['price_group_code', 'priceGroupCode'],
      ['price_group_desc','priceGroupDesc'],
      ['price_code', 'priceCode'],
      ['price_desc', 'priceDesc'],
      ['qty', 'qty'],
      ['volume', 'volume'],
      ['weight', 'weight'],
      ['price_uom', 'priceUOM'],
      ['book_price', 'bookPrice'],
      ['book_price_sum', 'bookPriceSum'],
      ['book_price_min', 'bookPriceMin'],
      ['book_price_currency_code', 'bookPriceCurrencyCode'],
      ['book_price_currency_rate', 'bookPriceCurrencyRate'],
      ['created_at', 'createdAt'],
      ['created_by_comptp', 'createdByComptp'],
      ['created_by_id_comp', 'createdByIdCompany'],
      ['created_by_id_user', 'createdByIdUser']
    ]
  }).then( (shipmentPrices) => {
    res.status(HTTP_STATUS.OK).json(shipmentPrices);
  })
}
