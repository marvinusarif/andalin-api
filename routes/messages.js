var express = require('express');
var bodyParser = require('body-parser');

var messages = require('../controllers/messages')
var auth = require('../controllers/auth');

var router = express.Router();
router.use(bodyParser.json());
router.use(bodyParser.urlencoded({extended: true}));

//users/shippers/vendors post message
router.route('/')
.post( auth.authenticate(), messages.saveMessage)

//users/shippers/vendors get messages head from shipment
router.route('/head/:shipmentId/:orderNumber')
.get( auth.authenticate(), messages.getMessageHeads)

//users/shippers/vendors get messages head from shipment
router.route('/body/:shipmentId/:orderNumber/:messageDateBucket')
.get( auth.authenticate(), messages.getMessages)

router.route('/channelsubscribers/:shipmentId/:orderNumber')
.get( auth.authenticate(), messages.getChannelSubscribersByShipment)

router.route('/channelsubscribers')
.post( auth.authenticate(), messages.postChannelSubscribersByUser)

router.route('/notifications')
.get( auth.authenticate(), messages.getUnreadNotifications)

router.route('/notifications/:shipmentId')
.put( auth.authenticate(), messages.updateNotificationsByShipment)
module.exports = router
