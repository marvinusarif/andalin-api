module.exports = function(sequelize, Sequelize) {
  var t_shipmentschedule = sequelize.define('t_shipmentschedule', {
    id: {
      autoIncrement: true,
      primaryKey: true,
      type: Sequelize.BIGINT(11)
    },
    id_schedule_head : {
      type : Sequelize.BIGINT(11),
      allowNull : false
    },
    id_shipment : {
      type : Sequelize.BIGINT(11),
      allowNull : false
    },
    order_number : {
      type : Sequelize.STRING(64),
      allowNull : false
    },
    date_arrival : {
      type : Sequelize.DATE,
      allowNull : false
    },
    date_departure : {
      type : Sequelize.DATE,
      allowNull : false
    },
    port_open_time : {
      type : Sequelize.DATE,
      allowNull : true
    },
    port_cutoff_time : {
      type : Sequelize.DATE,
      allowNull : true
    },
    inland_cutoff_time : {
      type : Sequelize.DATE,
      allowNull : true
    },
    vessel_name : {
      type : Sequelize.STRING(32),
      allowNull : false
    },
    pol : {
      type : Sequelize.STRING(16),
      allowNull : false
    },
    pod : {
      type : Sequelize.STRING(16),
      allowNull : false
    },
    origin_city_code : {
      type : Sequelize.STRING(8),
      allowNull : false
    },
    origin_country_code : {
      type : Sequelize.STRING(8),
      allowNull : false
    },
    destination_city_code : {
      type : Sequelize.STRING(8),
      allowNull : false
    },
    destination_country_code : {
      type : Sequelize.STRING(8),
      allowNull : false
    },
    is_valid : {
      type : Sequelize.BOOLEAN,
      allowNull : false
    },
    created_at : {
      type : Sequelize.DATE,
      allowNull : false
    },
    updated_at : {
      type : Sequelize.DATE,
      allowNull : true
    },
    created_by_comptp : {
      type : Sequelize.STRING(8),
      allowNull : false
    },
    created_by_id_comp : {
      type : Sequelize.BIGINT(11),
      allowNull : false
    },
    created_by_id_user : {
      type : Sequelize.BIGINT(11),
      allowNull : false
    }
  }, {
    timestamps : false,
    freezeTableName : true, //do not pluralizing tables name
    tableName : 't_shipmentschedule'
  });
  return t_shipmentschedule;
}
