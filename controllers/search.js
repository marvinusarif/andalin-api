const { HTTP_STATUS, HTTP_MESSAGE } = require('../config/express');
const { SHIPMENTLOAD_TYPES, CONTAINER_TYPES, SERVICE_TYPES, ORDER_TYPES } = require('../config/const');

var mySQL = require('../database/mysql/mysql-client');
var noSQL = require('express-cassandra');
var moment = require('moment');
var _ = require('lodash');

let debug_post = null;
let debug_response = null;

const getOrderType = (form) => {
  const { custom, originArea, destinationArea } = form;
  let orderType = '';
  if(originArea === 'fob'){
      orderType += 'PT';
  }else{
      orderType += 'DT';
  }
  if(destinationArea === 'fob'){
      orderType += 'P';
  }else{
      orderType += 'D';
  }
  return orderType;
}
const getFreightServiceExtension = (shipmentType) => {
  switch(shipmentType) {
    case CONTAINER_TYPES.REEFER :
      return SERVICE_TYPES.REEFER;
    case CONTAINER_TYPES.VENTILATE :
      return SERVICE_TYPES.VENTILATE;
    default :
      return '';
  }
}

const getFreightCostPromise = (form, shipmentLoad, totalShipment) => {
  /*
  * LCL : AIRCOURIER, AIRCARGO, SEALCL
    FCL : SEAFCL20FT, SEAFCL20FTREF, SEAFCL20FTVENT
          SEAFCL40FT, SEAFCL40FTREF, SEAFCL40FTVENT,
          SEAFCL40FTHC
    returning promise of call sdisplaypricelistfreight ('JKT','SIN','PTP','SEAFCL40FT',2,100,100);
*/
  const { originCity, destinationCity, dangerousGoods} = form;
  const promiseFreightCost = [];
  if(shipmentLoad === SHIPMENTLOAD_TYPES.LCL){
    [ SERVICE_TYPES.AIRCARGO, SERVICE_TYPES.AIRCOURIER, SERVICE_TYPES.SEALCL ].map( serviceType => {
      promiseFreightCost.push(
        mySQL.sequelize.query(`call sdisplaypricelistfreight${dangerousGoods ? 'dg' : ''} (:originCity,:destinationCity,:serviceType,:shipmentQty,:weight,:volume);`, {
        type : mySQL.sequelize.QueryTypes.SELECT,
        replacements : { originCity,
                         destinationCity,
                         serviceType,
                         shipmentQty : totalShipment.qty,
                         weight : serviceType === SERVICE_TYPES.SEALCL ? parseFloat(totalShipment.weight,2) / 1000 : totalShipment.weight,
                         volume : serviceType === SERVICE_TYPES.SEALCL ? parseFloat(totalShipment.volume,2) : (totalShipment.volume * 1000000 / 6000)}
      }));
    });
  }else{
    _.map(totalShipment, (shipment, shipmentSize) => {
      _.map(shipment, (shipmentQty, shipmentType) => {
        switch(shipmentSize) {
          case CONTAINER_TYPES.FT20 :
            serviceType = `${SERVICE_TYPES.FT20}${getFreightServiceExtension(shipmentType)}`;
            break;
          case CONTAINER_TYPES.FT40 :
            serviceType = `${SERVICE_TYPES.FT40}${getFreightServiceExtension(shipmentType)}`;
            break;
          case CONTAINER_TYPES.HC40 :
            serviceType = `${SERVICE_TYPES.HC40}${getFreightServiceExtension(shipmentType)}`;
            break;
          default :
            serviceType = SERVICE_TYPES.UNKNOWN
        }
        promiseFreightCost.push(
          mySQL.sequelize.query('call sdisplaypricelistfreight (:originCity,:destinationCity,:serviceType,:shipmentQty,:weight,:volume);', {
          type : mySQL.sequelize.QueryTypes.SELECT,
          replacements : { originCity,
                           destinationCity,
                           serviceType,
                           shipmentQty,
                           weight : 0,
                           volume : 0 }
        }));
      });
    });
  }
  return promiseFreightCost;
}

const calculatePriceFreightSingleBasedOnPriceUOM = (priceUOM, priceMin, priceSum, qty, volume, weight) => {
  let tempPrice = 0;
  if(priceUOM === 'CONT'){
    tempPrice = priceSum / qty;
  }else if(priceUOM === 'W'){
    tempPrice = priceSum / weight;
  }else if(priceUOM === 'V'){
    tempPrice = priceSum / volume;
  }else if(priceUOM === 'VW'){
    tempPrice = priceSum
  }else{
    tempPrice = priceSum
  }
  if(tempPrice > priceMin){
    return tempPrice
  }else{
    return priceMin
  }
}

const calculateCustomOrTruckingCostBasedOnPriceUOM = (priceUOM, priceMin, price, qty, volume, weight) => {
  let tempPrice = 0;
  if(priceUOM === 'CONT'){
      tempPrice = price * qty;
  }else if(priceUOM === 'W'){
      tempPrice = price * weight;
  }else if(priceUOM === 'V'){
      tempPrice = price * volume;
  }else if(priceUOM === 'VW'){
      const tempPriceVol = price * volume;
      const tempPriceWeight = price * weight;
      if(tempPriceWeight > tempPriceVol){
        tempPrice = tempPriceWeight;
      }else{
        tempPrice = tempPriceVol;
      }
  }else{
    //priceUOM === 'ALL'
    tempPrice = price
  }
  if(tempPrice > priceMin){
    return tempPrice
  }else{
    return priceMin
  }
}

const groupFreightPriceResultsByVendorAndShippingLine = (shipmentLoad, _rawServiceTypesResults) => {
  return new Promise( (resolve,reject) => {
    const results = _rawServiceTypesResults.reduce( (combinedSearchResults, serviceTypeResult) => {
      if(Object.keys(serviceTypeResult[0]).length > 0 || shipmentLoad === SHIPMENTLOAD_TYPES.LCL){
        const result = _.reduce(serviceTypeResult[0], (combinedFreightCostComponent,freightCostComponent) => {
          const { quoteId,
                  orderDescGroupPriceFreight, orderCodeGroupPriceFreight,
                  orderCodePriceFreight, orderDescPriceFreight,
                  orderPriceFreight, orderMinPriceFreight,
                  vendorId, vendorName, vendorImage, vendorRating,
                  orderFreight, orderServiceType, orderShippingLine,
                  orderOriginCountry, orderOriginCity, orderOriginCountryCode, orderOriginCityCode,
                  orderDestinationCountry, orderDestinationCity, orderDestinationCountryCode, orderDestinationCityCode,
                  orderTransittimeMin, orderTransittimeMax,
                  orderPriceCurrencyRate, orderPriceCurrencyCode,
                  orderValidUntil, orderNote, orderNoteDetailFreight,
                  orderQty, orderWeight, orderVolume, orderPriceUOMBase
                } = freightCostComponent;
          if(typeof combinedFreightCostComponent[`${vendorId}_${orderFreight}_${orderShippingLine}`] === 'undefined'){
            combinedFreightCostComponent[`${vendorId}_${orderFreight}_${orderShippingLine}`] = {
                quoteId,
                vendorId,
                vendorName,
                vendorImage,
                vendorRating,
                orderFreight,
                orderShippingLine,
                orderOriginCountryCode,
                orderOriginCountry,
                orderOriginCityCode,
                orderOriginCity,
                orderPol : '',
                orderDestinationCountryCode,
                orderDestinationCountry,
                orderDestinationCityCode,
                orderDestinationCity,
                orderPod : '',
                orderDirect : false,
                orderValidUntil,
                orderTransittimeMin,
                orderTransittimeMax,
                orderPriceCurrencyRate,
                orderPriceCurrencyCode,
                orderPriceFreight : {
                  [orderServiceType] : {
                    [orderCodeGroupPriceFreight] : {
                      [orderCodePriceFreight] : {
                        quoteFreightId : quoteId,
                        serviceType : orderServiceType,
                        codeGroupPriceFreight : orderCodeGroupPriceFreight,
                        descGroupPriceFreight : orderDescGroupPriceFreight,
                        codePriceFreight : orderCodePriceFreight,
                        descPriceFreight : orderDescPriceFreight,
                        qty : parseFloat(orderQty),
                        volume : parseFloat(orderVolume),
                        weight : parseFloat(orderWeight),
                        priceUOM : orderPriceUOMBase,
                        priceCurrencyCode : orderPriceCurrencyCode,
                        price : calculatePriceFreightSingleBasedOnPriceUOM(orderPriceUOMBase, parseFloat(orderMinPriceFreight), parseFloat(orderPriceFreight), parseFloat(orderQty), parseFloat(orderVolume), parseFloat(orderWeight)),
                        priceMin : parseFloat(orderMinPriceFreight),
                        priceSum : parseFloat(orderPriceFreight) > parseFloat(orderMinPriceFreight) ? parseFloat(orderPriceFreight) : parseFloat(orderMinPriceFreight)
                      }
                    }
                  }
                },
                orderNote : {
                    [orderServiceType] : orderNote === null || orderNote === "" ? [] : [orderNote]
                },
                orderNoteDetailFreight : {
                    [orderServiceType] : orderNoteDetailFreight === null || orderNoteDetailFreight === "" ? [] : [orderNoteDetailFreight]
                },
            }
          }else{
            combinedFreightCostComponent[`${vendorId}_${orderFreight}_${orderShippingLine}`] = Object.assign({},combinedFreightCostComponent[`${vendorId}_${orderFreight}_${orderShippingLine}`],{
              orderPriceFreight : Object.assign({}, combinedFreightCostComponent[`${vendorId}_${orderFreight}_${orderShippingLine}`].orderPriceFreight, {
                [orderServiceType] :  Object.assign({},combinedFreightCostComponent[`${vendorId}_${orderFreight}_${orderShippingLine}`].orderPriceFreight[orderServiceType],{
                    [orderCodeGroupPriceFreight] : Object.assign({},combinedFreightCostComponent[`${vendorId}_${orderFreight}_${orderShippingLine}`].orderPriceFreight[orderServiceType][orderCodeGroupPriceFreight],{
                      [orderCodePriceFreight] : {
                        quoteFreightId : quoteId,
                        serviceType : orderServiceType,
                        codeGroupPriceFreight : orderCodeGroupPriceFreight,
                        descGroupPriceFreight : orderDescGroupPriceFreight,
                        codePriceFreight : orderCodePriceFreight,
                        descPriceFreight : orderDescPriceFreight,
                        qty : parseFloat(orderQty),
                        volume : parseFloat(orderVolume),
                        weight : parseFloat(orderWeight),
                        priceUOM : orderPriceUOMBase,
                        priceCurrencyCode : orderPriceCurrencyCode,
                        price : calculatePriceFreightSingleBasedOnPriceUOM(orderPriceUOMBase, parseFloat(orderMinPriceFreight), parseFloat(orderPriceFreight), parseFloat(orderQty), parseFloat(orderVolume), parseFloat(orderWeight)),
                        priceMin : parseFloat(orderMinPriceFreight),
                        priceSum : parseFloat(orderPriceFreight) > parseFloat(orderMinPriceFreight) ? parseFloat(orderPriceFreight) : parseFloat(orderMinPriceFreight)
                      }
                    })
                  })
              }),
              orderNote : Object.assign({}, combinedFreightCostComponent[`${vendorId}_${orderFreight}_${orderShippingLine}`].orderNote, {
                  [orderServiceType] : orderNote === null || orderNote === "" ?
                                          combinedFreightCostComponent[`${vendorId}_${orderFreight}_${orderShippingLine}`].orderNote[orderServiceType]
                                        : combinedFreightCostComponent[`${vendorId}_${orderFreight}_${orderShippingLine}`].orderNote[orderServiceType].concat(orderNote)
              }),
              orderNoteDetailFreight : Object.assign({}, combinedFreightCostComponent[`${vendorId}_${orderFreight}_${orderShippingLine}`].orderNoteDetailFreight, {
                  [orderServiceType] : orderNoteDetailFreight === null || orderNoteDetailFreight === "" ?
                                          combinedFreightCostComponent[`${vendorId}_${orderFreight}_${orderShippingLine}`].orderNoteDetailFreight[orderServiceType]
                                        : combinedFreightCostComponent[`${vendorId}_${orderFreight}_${orderShippingLine}`].orderNoteDetailFreight[orderServiceType].concat(orderNoteDetailFreight)
              })
            });
          }
          return combinedFreightCostComponent;
        }, {});
        return combinedSearchResults.concat(result);
      }else{
        reject("RETURNING NO RESULT WHILE GROUPING FREIGHT PRICE");
      }
    }, []);
    console.log("NUMBER OF SERVICE TYPE BINS :", results.length);
    resolve(results);
  })
}
const priceFreightComponentSum = (orderPriceFreight) => {
  return _.reduce(orderPriceFreight, (orderPriceTotal, priceServiceType) => {
    const priceServiceTypeTotal =  _.reduce(priceServiceType, (orderPriceServiceTypeTotal, priceCodeGroup) => {
      const priceCodeGroupTotal = _.reduce(priceCodeGroup, (orderPriceFreightTotal, priceFreight) => {
        return orderPriceFreightTotal + priceFreight.priceSum
      },0);
      return orderPriceServiceTypeTotal + priceCodeGroupTotal
    }, 0);
    return orderPriceTotal + priceServiceTypeTotal
  }, 0);

}
const mergePriceResultsByVendorAndShippingLine = (shipmentLoad, groupedPriceResults) => {
  return new Promise( (resolve,reject) => {
    let validResults;
    if(shipmentLoad !== SHIPMENTLOAD_TYPES.LCL){
      const groupedPriceResultsLength = groupedPriceResults.reduce( (groupedPriceResultsLength, groupedPriceResult) => {
        return groupedPriceResultsLength.concat(Object.keys(groupedPriceResult).length);
      }, []);
      const indexOfMinGroupedPriceLength = groupedPriceResultsLength.indexOf(Math.min(...groupedPriceResultsLength));
      console.log('  LENGTH OF EACH SERVICE TYPE BIN :', groupedPriceResultsLength);
      console.log('  START REFERENCING ON INDEX      : ', indexOfMinGroupedPriceLength);

      validResults = _.reduce(groupedPriceResults[indexOfMinGroupedPriceLength], (validResults, orderReference, orderReferenceIndex) => {
        let isValid = true;

        let validResult = orderReference;
        groupedPriceResults.map( (serviceTypeBin, serviceTypeBinIndex) => {
          if(serviceTypeBinIndex !== indexOfMinGroupedPriceLength){
            if(serviceTypeBin.hasOwnProperty(orderReferenceIndex) && isValid){
              validResult = Object.assign({}, validResult, {
                quoteId : `${validResult.quoteId}-${serviceTypeBin[orderReferenceIndex].quoteId}`,
                orderPriceFreight : Object.assign({}, validResult.orderPriceFreight,serviceTypeBin[orderReferenceIndex].orderPriceFreight),
                orderNote : Object.assign({}, validResult.orderNote,serviceTypeBin[orderReferenceIndex].orderNote),
                orderNoteDetailFreight :  Object.assign({}, validResult.orderNoteDetailFreight,serviceTypeBin[orderReferenceIndex].orderNoteDetailFreight),
              });
            }else{
              isValid = false
            }
          }
        });
        if(isValid){
          return validResults.concat(validResult);
        }else{
          return validResults;
        }
      }, []);
    }else{
      validResults = groupedPriceResults.reduce( (validResults,serviceTypeBin) => {
        const result = _.reduce(serviceTypeBin, (allResultsinBin, result) => {
          return allResultsinBin.concat(result);
        },[]);
        return validResults.concat(result);
      }, []);
    }
    console.log('VALID RESULTS AFTER MERGING FREIGHT :', validResults.length);
    if(validResults.length > 0) {
      resolve(validResults);
    }else{
      reject("RETURNING NO VALID RESULT AFTER MERGING FREIGHT PRICE BY KEY : VENDOR_ORDERFREIGHT_SHIPPINGLINE");
    }
  });
}
const getDocumentCostPromise = (originCity,destinationCity) => {
  let promisesDocumentCost = [];
  promisesDocumentCost.push(mySQL.sequelize.query('call sdisplaypricelistdoc (:originCity,:destinationCity);', {
    type : mySQL.sequelize.QueryTypes.SELECT,
    replacements : { originCity,
                     destinationCity}
  }));
  return promisesDocumentCost;
}

const mergeDocumentResultByVendorIdAndFreight = (mergedPriceResults, rawResults) => {
  return new Promise ( (resolve,reject) => {
    const resultsByVendor = _.reduce(rawResults[0][0], (combinedDocResult, docResult) => {
      const { vendorId, orderFreight, orderServiceType, orderCodePriceDoc, orderDescPriceDoc, orderCodeGroupPriceDoc, orderDescGroupPriceDoc,
              orderPriceDoc, orderMinPriceDoc, orderPriceCurrencyCode, quoteDocId } = docResult;
      if(!combinedDocResult.hasOwnProperty(orderFreight)){
        combinedDocResult[orderFreight] = {}
      }
      if(!combinedDocResult[orderFreight].hasOwnProperty(vendorId)){
        combinedDocResult[orderFreight][vendorId] = {}
      }
      if(!combinedDocResult[orderFreight][vendorId].hasOwnProperty(orderServiceType === null || orderServiceType === "" ? 'NO_ORDER_SERVICE_TYPE' : orderServiceType)){
        combinedDocResult[orderFreight][vendorId][orderServiceType === null || orderServiceType === "" ? 'NO_ORDER_SERVICE_TYPE' : orderServiceType] = {};
      }
      return Object.assign({}, combinedDocResult, {
        [orderFreight] : Object.assign({}, combinedDocResult[orderFreight], {
          [vendorId] : Object.assign({}, !combinedDocResult.hasOwnProperty(orderFreight) ? {} : combinedDocResult[orderFreight][vendorId], {
            [orderServiceType === null || orderServiceType === "" ? 'NO_ORDER_SERVICE_TYPE' : orderServiceType] : Object.assign({}, combinedDocResult[orderFreight][vendorId][orderServiceType === null || orderServiceType === "" ? 'NO_ORDER_SERVICE_TYPE' : orderServiceType], {
              [orderCodePriceDoc] : {
                quoteDocId : quoteDocId,
                serviceType : orderServiceType,
                codeGroupPriceDoc : orderCodeGroupPriceDoc,
                descGroupPriceDoc :  orderDescGroupPriceDoc,
                codePriceDoc : orderCodePriceDoc,
                descPriceDoc : orderDescPriceDoc,
                qty : 1,
                volume : 0,
                weight : 0,
                priceUOM : 'ALL',
                priceCurrencyCode : orderPriceCurrencyCode,
                price : parseFloat(orderPriceDoc),
                priceMin : parseFloat(orderMinPriceDoc),
                priceSum : parseFloat(orderPriceDoc) > parseFloat(orderMinPriceDoc) ? parseFloat(orderPriceDoc) : parseFloat(orderMinPriceDoc)
              }
            })
          })
        })
      })
    }, {});
    console.log('  DOCUMENT BASED ON FREIGHT::VENDOR ID :', resultsByVendor);
    const validResults = _.reduce(mergedPriceResults, (combinedResults, result) => {
      const { vendorId, orderFreight, orderPriceFreight, quoteId } = result;
      let isValid = true;
      let priceDocuments;
      if(orderFreight === 'EXPRESS'){
        //Express has no price documents
        priceDocuments = {}
      }else{
        if((orderFreight === 'OCEAN' || orderFreight === 'AIR') && !resultsByVendor.hasOwnProperty(orderFreight)){
          isValid = false;
        }else{
          if(!resultsByVendor[orderFreight].hasOwnProperty(vendorId)){
            isValid = false;
          }else{
            const orderServiceTypes = Object.keys(orderPriceFreight).concat('NO_ORDER_SERVICE_TYPE');
            priceDocuments = orderServiceTypes.reduce( (priceDocuments, orderServiceType) => {
              priceDocuments = Object.assign({}, priceDocuments, {
                  [orderServiceType] : resultsByVendor[orderFreight][vendorId][orderServiceType]
              });
              return priceDocuments;
            }, {});
          }
        }
      }
      if(isValid){
        const generatedQuoteDocIds = _.reduce( priceDocuments, (generatedQuoteDocIds, quoteDocPerServiceType) => {
          const savedQuoteIdPerServiceType = _.reduce(quoteDocPerServiceType, (quoteDocIdsPerCodePriceDoc, _codePriceDoc) => {
            return quoteDocIdsPerCodePriceDoc.concat(_codePriceDoc.quoteDocId)
          },[]);
          return generatedQuoteDocIds.concat(savedQuoteIdPerServiceType);
        },[]);
        combinedResults = combinedResults.concat(Object.assign({}, result, {
          quoteId : `${result.quoteId}_${generatedQuoteDocIds.length > 0 ? generatedQuoteDocIds.join('-') : 'N'}`,
          orderPriceDocument : priceDocuments
        }));
      }
      return combinedResults;
    }, []);
    console.log('VALID RESULTS AFTER MERGING DOCUMENT :', validResults.length);
    if(validResults.length > 0){
      resolve(validResults);
    }else{
      reject('RETURNING NO RESULT AFTER MERGING PRICE DOCUMENT');
    }
  })
}

const getCustomCostPromise = (cities, orderDestinationCityCode, mergedDocResults) => {
  let vendorIds = [];
  let promisesCustomCost = [];
  if(cities.length > 0) {
      mergedDocResults.map( (mergedDocResult) => {
          if(!vendorIds.includes(mergedDocResult.vendorId)){
            vendorIds.push(mergedDocResult.vendorId);
            cities.map( (city) => {
              promisesCustomCost.push(mySQL.sequelize.query('call sdisplaypricelistcustom (:city,:vendorId,:exim);', {
              type : mySQL.sequelize.QueryTypes.SELECT,
              replacements : { city,
                               vendorId : mergedDocResult.vendorId,
                               exim : city === orderDestinationCityCode ? 'IMPORT' : 'EXPORT'
                             }
              }))
            })
          }
      });
    return promisesCustomCost;
  }else{
    return promisesCustomCost;
  }
}

const mergeCustomResult = (customOriginCity, customDestinationCity, mergedDocResults, rawResults) => {
  return new Promise( (resolve,reject) => {
    if((rawResults.length < 1) && customOriginCity === null && customDestinationCity === null){
      console.log('NO CUSTOM WILL BE MERGED')
      const combinedResults = mergedDocResults.reduce( (combinedResults, result) => {
        return combinedResults.concat( Object.assign({}, result, {
          quoteId : `${result.quoteId}_N`,
          orderPriceCustom : {}
        }));
      },[]);
      resolve(combinedResults);
    }else{
      const orderServiceTypes = Object.keys(mergedDocResults[0].orderPriceFreight).concat('NO_ORDER_SERVICE_TYPE');
      const freightDetail = _.reduce(mergedDocResults[0].orderPriceFreight, (freightDetail, orderServiceType, key) => {
        return Object.assign({}, freightDetail, {
          [key] : {
            qty : orderServiceType['FRE'][Object.keys(orderServiceType['FRE'])[0]].qty,
            volume : orderServiceType['FRE'][Object.keys(orderServiceType['FRE'])[0]].volume,
            weight : orderServiceType['FRE'][Object.keys(orderServiceType['FRE'])[0]].weight
          }
        })
      },{});
      //inject manually
      freightDetail['NO_ORDER_SERVICE_TYPE'] = {
        qty : 1,
        volume : 0,
        weight : 0
      }
      const combinedResultsByVendorInCities = rawResults.reduce( (combinedResultsByVendorInCities, customCityWithVendorBin) => {
        const resultsByVendorInCity = _.reduce(customCityWithVendorBin[0], (resultsByVendorInCity,customCityWithVendorResult) => {
          const { quoteCustomId, vendorId, orderCity, orderFreight, orderServiceType, orderCodePriceCustom,
                  orderDescPriceCustom, orderCodeGroupPriceCustom, orderDescGroupPriceCustom, orderPriceCustom, orderMinPriceCustom,
                  orderPriceCurrencyCode, orderPriceUOM } = customCityWithVendorResult;
          if(!resultsByVendorInCity.hasOwnProperty(orderCity)) {
            resultsByVendorInCity[orderCity] = {}
          }
          if(!resultsByVendorInCity[orderCity].hasOwnProperty(vendorId)) {
            resultsByVendorInCity[orderCity][vendorId] = {}
          }
          if(!resultsByVendorInCity[orderCity][vendorId].hasOwnProperty(orderFreight)){
            resultsByVendorInCity[orderCity][vendorId][orderFreight] = {}
          }
          if(!resultsByVendorInCity[orderCity][vendorId][orderFreight].hasOwnProperty(orderServiceType === null || orderServiceType === "" ? 'NO_ORDER_SERVICE_TYPE' : orderServiceType)){
            resultsByVendorInCity[orderCity][vendorId][orderFreight][orderServiceType === null || orderServiceType === "" ? 'NO_ORDER_SERVICE_TYPE' : orderServiceType] = {}
          }
          return Object.assign({}, resultsByVendorInCity, {
            [orderCity] : Object.assign({}, resultsByVendorInCity[orderCity], {
              [vendorId] : Object.assign({}, resultsByVendorInCity[orderCity][vendorId], {
                [orderFreight] : Object.assign({}, resultsByVendorInCity[orderCity][vendorId][orderFreight], {
                  [orderServiceType === null || orderServiceType === "" ? 'NO_ORDER_SERVICE_TYPE' : orderServiceType] : Object.assign({}, resultsByVendorInCity[orderCity][vendorId][orderFreight][orderServiceType === null || orderServiceType === "" ? 'NO_ORDER_SERVICE_TYPE' : orderServiceType], {
                    [orderCodePriceCustom] : {
                      quoteCustomId,
                      serviceType : orderServiceType,
                      codeGroupPriceCustom : orderCodeGroupPriceCustom,
                      descGroupPriceCustom : orderDescGroupPriceCustom,
                      codePriceCustom : orderCodePriceCustom,
                      descPriceCustom : orderDescPriceCustom,
                      priceCurrencyCode : orderPriceCurrencyCode,
                      priceUOM : orderPriceUOM,
                      price : parseFloat(orderPriceCustom),
                      priceMin : parseFloat(orderMinPriceCustom)
                    }
                  })
                })
              })
            })
          });
        },{});
        if(Object.keys(resultsByVendorInCity).length > 0){
          if(typeof combinedResultsByVendorInCities[Object.keys(resultsByVendorInCity)[0]] === 'undefined'){
            combinedResultsByVendorInCities[Object.keys(resultsByVendorInCity)[0]] = {}
          }
          return Object.assign({}, combinedResultsByVendorInCities, {
            [Object.keys(resultsByVendorInCity)[0]] : Object.assign({}, combinedResultsByVendorInCities[Object.keys(resultsByVendorInCity)[0]], resultsByVendorInCity[Object.keys(resultsByVendorInCity)[0]])
          })
        }
        return combinedResultsByVendorInCities
      },{});
      //check if there is no result in destinationCity / originCity
      if( ( customOriginCity !== null && !Object.keys(combinedResultsByVendorInCities).includes(customOriginCity) ) || ( customDestinationCity !== null && !Object.keys(combinedResultsByVendorInCities).includes(customDestinationCity) ) ){
        const missingCustomPriceList = [];
        if(customOriginCity !== null && !Object.keys(combinedResultsByVendorInCities).includes(customOriginCity) ){
          missingCustomPriceList.push(customOriginCity);
        }
        if(customDestinationCity !== null && !Object.keys(combinedResultsByVendorInCities).includes(customDestinationCity)){
          missingCustomPriceList.push(customDestinationCity);
        }
        if(missingCustomPriceList.length > 0) {
          reject(`ALL VENDORS HAS NO CUSTOM RESULT IN ${missingCustomPriceList.join(' & ')}`);
        }
      }else{
        console.log("  CUSTOM BASED ON CITY :: VENDORID : ", combinedResultsByVendorInCities);
        const validResults = mergedDocResults.reduce( (combinedResults, mergedDocResult) => {
          const { vendorId, orderFreight, orderOriginCityCode, orderDestinationCityCode } = mergedDocResult;
          let isValid = true;
          if(orderFreight === 'EXPRESS'){
            return combinedResults.concat(Object.assign({}, mergedDocResult, {
              quoteId : `${mergedDocResult.quoteId}_N`,
              orderPriceCustom : {}
            }));
          } else {
            if(customOriginCity !== null){
              if(!combinedResultsByVendorInCities.hasOwnProperty(orderOriginCityCode)){
                isValid = false;
              }else{
                if(!combinedResultsByVendorInCities[orderOriginCityCode].hasOwnProperty(vendorId)){
                  isValid = false;
                } else {
                  if(!combinedResultsByVendorInCities[orderOriginCityCode][vendorId].hasOwnProperty(orderFreight)){
                    isValid = false;
                  } else {
                    //console.log(Object.keys(mergedDocResult.orderPriceFreight));
                    //console.log(Object.keys(combinedResultsByVendorInCities[orderOriginCityCode][vendorId][orderFreight]))
                    if(!Object.keys(mergedDocResult.orderPriceFreight).every( serviceType => combinedResultsByVendorInCities[orderOriginCityCode][vendorId][orderFreight].hasOwnProperty(serviceType) )){
                      isValid = false;
                    }
                  }
                }
              }
            }
            if(customDestinationCity !== null){
              if(!combinedResultsByVendorInCities.hasOwnProperty(orderDestinationCityCode)){
                isValid = false;
              } else {
                if(!combinedResultsByVendorInCities[orderDestinationCityCode].hasOwnProperty(vendorId)){
                  isValid = false;
                } else {
                  if(!combinedResultsByVendorInCities[orderDestinationCityCode][vendorId].hasOwnProperty(orderFreight)){
                    isValid = false;
                  } else {
                    //console.log(Object.keys(mergedDocResult.orderPriceFreight));
                    //console.log(Object.keys(combinedResultsByVendorInCities[orderDestinationCityCode][vendorId][orderFreight]))
                    if(!Object.keys(mergedDocResult.orderPriceFreight).every( serviceType => combinedResultsByVendorInCities[orderDestinationCityCode][vendorId][orderFreight].hasOwnProperty(serviceType) )){
                      isValid = false;
                    }
                  }
                }
              }
            }
            if(isValid){
              let quoteCustomIds = [];
              let modifiedMergedDocResult = Object.assign({}, mergedDocResult, {
                quoteId : `${mergedDocResult.quoteId}_`,
                orderPriceCustom : {}
              });
              if(customOriginCity !== null){
                modifiedMergedDocResult = Object.assign({}, modifiedMergedDocResult, {
                  orderPriceCustom : Object.assign({}, modifiedMergedDocResult.orderPriceCustom, {
                    [orderOriginCityCode] : _.reduce(combinedResultsByVendorInCities[orderOriginCityCode][vendorId][orderFreight], (combinedResultsInOriginCity, customPriceServiceType, serviceTypeKey) => {
                      if(orderServiceTypes.includes(serviceTypeKey)){
                        return Object.assign({}, combinedResultsInOriginCity, {
                          [serviceTypeKey] : _.reduce(customPriceServiceType, (modifiedCustomCodePrice, customCodePriceHead, customCodeKey) => {
                              quoteCustomIds.push(customCodePriceHead.quoteCustomId)
                              return Object.assign({}, modifiedCustomCodePrice, {
                                [customCodeKey] : Object.assign({}, customCodePriceHead, {
                                  qty : freightDetail[serviceTypeKey].qty,
                                  volume : freightDetail[serviceTypeKey].volume,
                                  weight : freightDetail[serviceTypeKey].weight,
                                  priceSum : calculateCustomOrTruckingCostBasedOnPriceUOM(customCodePriceHead.priceUOM, customCodePriceHead.priceMin, customCodePriceHead.price, freightDetail[serviceTypeKey].qty, freightDetail[serviceTypeKey].volume, freightDetail[serviceTypeKey].weight)
                                })
                              })
                          },{})
                        })
                      }else{
                        return combinedResultsInOriginCity;
                      }
                    },{})
                  })
                })
              }
              if(customDestinationCity !== null){
                modifiedMergedDocResult = Object.assign({}, modifiedMergedDocResult, {
                  orderPriceCustom : Object.assign({}, modifiedMergedDocResult.orderPriceCustom, {
                    [orderDestinationCityCode] : _.reduce(combinedResultsByVendorInCities[orderDestinationCityCode][vendorId][orderFreight], (combinedResultsinDestinationCity, customPriceServiceType, serviceTypeKey) => {
                      if(orderServiceTypes.includes(serviceTypeKey)){
                        return Object.assign({}, combinedResultsinDestinationCity, {
                          [serviceTypeKey] : _.reduce(customPriceServiceType, (modifiedCustomCodePrice, customCodePriceHead, customCodeKey) => {
                              quoteCustomIds.push(customCodePriceHead.quoteCustomId)
                              return Object.assign({}, modifiedCustomCodePrice, {
                                [customCodeKey] : Object.assign({}, customCodePriceHead, {
                                  qty : freightDetail[serviceTypeKey].qty,
                                  volume : freightDetail[serviceTypeKey].volume,
                                  weight : freightDetail[serviceTypeKey].weight,
                                  priceSum : calculateCustomOrTruckingCostBasedOnPriceUOM(customCodePriceHead.priceUOM, customCodePriceHead.priceMin, customCodePriceHead.price, freightDetail[serviceTypeKey].qty, freightDetail[serviceTypeKey].volume, freightDetail[serviceTypeKey].weight)
                                })
                              })
                          },{})
                        })
                      }else{
                        return combinedResultsinDestinationCity;
                      }
                    },{})
                  })
                })
              }
              return combinedResults.concat(Object.assign({}, modifiedMergedDocResult, {
                quoteId : `${modifiedMergedDocResult.quoteId}${quoteCustomIds.join('-')}`
              }));
            }
          }
          return combinedResults;
        },[]);
        console.log('VALID RESULTS AFTER MERGING CUSTOM :', validResults.length);
        if(validResults.length > 0){
          resolve(validResults);
        }else{
          reject('RETURNING NO RESULT AFTER MERGING PRICE CUSTOM');
        }
      }
    }
  })
}

const getTruckingCostPromise = (cities, mergedCustomResults) => {
  let vendorIds = [];
  let promisesTruckingCost = [];
  if(cities.length > 0) {
      mergedCustomResults.map( (mergedCustomResult) => {
          if(!vendorIds.includes(mergedCustomResult.vendorId)){
            vendorIds.push(mergedCustomResult.vendorId);
            cities.map( (city) => {
              promisesTruckingCost.push(mySQL.sequelize.query('call sdisplaypricelisttrucking (:city,:vendorId);', {
              type : mySQL.sequelize.QueryTypes.SELECT,
              replacements : { city,
                               vendorId : mergedCustomResult.vendorId}
              }))
            })
          }
      });
    return promisesTruckingCost;
  }else{
    return promisesTruckingCost;
  }
}

const mergeTruckingResult = (truckingOriginCity, truckingDestinationCity, mergedCustomResults, rawResults) => {
  return new Promise( (resolve,reject) => {
    if((rawResults.length < 1) && truckingOriginCity === null && truckingDestinationCity === null){
      console.log('NO TRUCKING WILL BE MERGED')
      const combinedResults = mergedCustomResults.reduce( (combinedResults, result) => {
        return combinedResults.concat( Object.assign({}, result, {
          quoteId : `${result.quoteId}_N`,
          orderPriceTrucking : {}
        }));
      },[]);
      resolve(combinedResults);
    }else{
      const orderServiceTypes = Object.keys(mergedCustomResults[0].orderPriceFreight).concat('NO_ORDER_SERVICE_TYPE');
      const freightDetail = _.reduce(mergedCustomResults[0].orderPriceFreight, (freightDetail, orderServiceType, key) => {
        return Object.assign({}, freightDetail, {
          [key] : {
            qty : orderServiceType['FRE'][Object.keys(orderServiceType['FRE'])[0]].qty,
            volume : orderServiceType['FRE'][Object.keys(orderServiceType['FRE'])[0]].volume,
            weight : orderServiceType['FRE'][Object.keys(orderServiceType['FRE'])[0]].weight
          }
        })
      },{});
      //inject manually
      freightDetail['NO_ORDER_SERVICE_TYPE'] = {
        qty : 1,
        volume : 0,
        weight : 0
      }
      const combinedResultsByVendorInCities = rawResults.reduce( (combinedResultsByVendorInCities, truckingCityWithVendorBin) => {
        const resultsByVendorInCity = _.reduce(truckingCityWithVendorBin[0], (resultsByVendorInCity,truckingCityWithVendorResult) => {
          const { quoteTruckingId, vendorId, orderCity, orderFreight, orderServiceType, orderCodePriceTrucking, orderDescPriceTrucking,
                  orderCodeGroupPriceTrucking, orderDescGroupPriceTrucking, orderPriceTrucking, orderMinPriceTrucking,
                  orderPriceCurrencyCode, orderPriceUOM } = truckingCityWithVendorResult;
          if(!resultsByVendorInCity.hasOwnProperty(orderCity)) {
            resultsByVendorInCity[orderCity] = {}
          }
          if(!resultsByVendorInCity[orderCity].hasOwnProperty(vendorId)) {
            resultsByVendorInCity[orderCity][vendorId] = {}
          }
          if(!resultsByVendorInCity[orderCity][vendorId].hasOwnProperty(orderFreight)){
            resultsByVendorInCity[orderCity][vendorId][orderFreight] = {}
          }
          if(!resultsByVendorInCity[orderCity][vendorId][orderFreight].hasOwnProperty(orderServiceType === null || orderServiceType === "" ? 'NO_ORDER_SERVICE_TYPE' : orderServiceType)){
            resultsByVendorInCity[orderCity][vendorId][orderFreight][orderServiceType === null || orderServiceType === "" ? 'NO_ORDER_SERVICE_TYPE' : orderServiceType] = {}
          }
          return Object.assign({}, resultsByVendorInCity, {
            [orderCity] : Object.assign({}, resultsByVendorInCity[orderCity], {
              [vendorId] : Object.assign({}, resultsByVendorInCity[orderCity][vendorId], {
                [orderFreight] : Object.assign({}, resultsByVendorInCity[orderCity][vendorId][orderFreight], {
                  [orderServiceType === null || orderServiceType === "" ? 'NO_ORDER_SERVICE_TYPE' : orderServiceType] : Object.assign({}, resultsByVendorInCity[orderCity][vendorId][orderFreight][orderServiceType === null || orderServiceType === "" ? 'NO_ORDER_SERVICE_TYPE' : orderServiceType], {
                    [orderCodePriceTrucking] : {
                      quoteTruckingId,
                      serviceType : orderServiceType,
                      codeGroupPriceTrucking : orderCodeGroupPriceTrucking,
                      descGroupPriceTrucking : orderDescGroupPriceTrucking,
                      codePriceTrucking : orderCodePriceTrucking,
                      descPriceTrucking : orderDescPriceTrucking,
                      priceCurrencyCode : orderPriceCurrencyCode,
                      priceUOM : orderPriceUOM,
                      price : parseFloat(orderPriceTrucking),
                      priceMin : parseFloat(orderMinPriceTrucking)
                    }
                  })
                })
              })
            })
          });
        },{});
        if(Object.keys(resultsByVendorInCity).length > 0){
          if(typeof combinedResultsByVendorInCities[Object.keys(resultsByVendorInCity)[0]] === 'undefined'){
            combinedResultsByVendorInCities[Object.keys(resultsByVendorInCity)[0]] = {}
          }
          return Object.assign({}, combinedResultsByVendorInCities, {
            [Object.keys(resultsByVendorInCity)[0]] : Object.assign({}, combinedResultsByVendorInCities[Object.keys(resultsByVendorInCity)[0]], resultsByVendorInCity[Object.keys(resultsByVendorInCity)[0]])
          })
        }
        return combinedResultsByVendorInCities
      },{});
      console.log("  TRUCKING BASED ON CITY :: VENDORID : ",combinedResultsByVendorInCities);
      //check if there is no result in destinationCity / orderCity
      if( (truckingOriginCity !== null && !Object.keys(combinedResultsByVendorInCities).includes(truckingOriginCity)) || (truckingDestinationCity !== null && !Object.keys(combinedResultsByVendorInCities).includes(truckingDestinationCity)) ){
        let missingTruckingPriceList = [];
        if(truckingOriginCity !== null && !Object.keys(combinedResultsByVendorInCities).includes(truckingOriginCity) ){
          missingTruckingPriceList.push(truckingOriginCity);
        }
        if(truckingDestinationCity !== null && !Object.keys(combinedResultsByVendorInCities).includes(truckingDestinationCity)){
          missingTruckingPriceList.push(truckingDestinationCity);
        }
        reject(`ALL VENDORS HAS NO TRUCKING RESULT IN ${missingTruckingPriceList.join(' & ')}`);
      }else{
        const validResults = mergedCustomResults.reduce( (combinedResults, mergedCustomResult) => {
          const { vendorId, orderFreight, orderOriginCityCode, orderDestinationCityCode } = mergedCustomResult;
          let isValid = true;
          if(orderFreight === 'EXPRESS'){
            return combinedResults.concat(Object.assign({}, mergedCustomResult, {
              quoteId : `${mergedCustomResult.quoteId}_N`,
              orderPriceTrucking : {}
            }));
          }else{
            if(truckingOriginCity !== null){
              if(!combinedResultsByVendorInCities.hasOwnProperty(orderOriginCityCode)){
                isValid = false;
              } else {
                if(!combinedResultsByVendorInCities[orderOriginCityCode].hasOwnProperty(vendorId)){
                  isValid = false;
                } else
                  if(!combinedResultsByVendorInCities[orderOriginCityCode][vendorId].hasOwnProperty(orderFreight)){
                    isValid = false
                  } else {
                    if(!Object.keys(mergedCustomResult.orderPriceFreight).every( serviceType => combinedResultsByVendorInCities[orderOriginCityCode][vendorId][orderFreight].hasOwnProperty(serviceType) )){
                      isValid = false;
                    }
                  }
              }
            }
            if(truckingDestinationCity !== null){
              if(!combinedResultsByVendorInCities.hasOwnProperty(orderDestinationCityCode)){
                isValid = false;
              } else {
                if(!combinedResultsByVendorInCities[orderDestinationCityCode].hasOwnProperty(vendorId)){
                  isValid = false;
                } else {
                  if(!combinedResultsByVendorInCities[orderDestinationCityCode][vendorId].hasOwnProperty(orderFreight)){
                    isValid = false;
                  } else {
                    if(!Object.keys(mergedCustomResult.orderPriceFreight).every( serviceType => combinedResultsByVendorInCities[orderDestinationCityCode][vendorId][orderFreight].hasOwnProperty(serviceType) )){
                      isValid = false;
                    }
                  }
                }
              }
            }
            if(isValid){
              let quoteTruckingIds = [];
              let modifiedMergedCustomResult = Object.assign({}, mergedCustomResult, {
                quoteId : `${mergedCustomResult.quoteId}_`,
                orderPriceTrucking : {}
              });
              if(truckingOriginCity !== null){
                modifiedMergedCustomResult = Object.assign({}, modifiedMergedCustomResult, {
                  orderPriceTrucking : Object.assign({}, modifiedMergedCustomResult.orderPriceTrucking, {
                    [orderOriginCityCode] : _.reduce(combinedResultsByVendorInCities[orderOriginCityCode][vendorId][orderFreight], (combinedResultsInOriginCity, truckingPriceServiceType, serviceTypeKey) => {
                      if(orderServiceTypes.includes(serviceTypeKey)){
                        return Object.assign({}, combinedResultsInOriginCity, {
                          [serviceTypeKey] : _.reduce(truckingPriceServiceType, (modifiedTruckingCodePrice, truckingCodePriceHead, truckingCodeKey) => {
                              quoteTruckingIds.push(truckingCodePriceHead.quoteTruckingId)
                              return Object.assign({}, modifiedTruckingCodePrice, {
                                [truckingCodeKey] : Object.assign({}, truckingCodePriceHead, {
                                  qty : freightDetail[serviceTypeKey].qty,
                                  volume : freightDetail[serviceTypeKey].volume,
                                  weight : freightDetail[serviceTypeKey].weight,
                                  priceSum : calculateCustomOrTruckingCostBasedOnPriceUOM(truckingCodePriceHead.priceUOM, truckingCodePriceHead.priceMin, truckingCodePriceHead.price, freightDetail[serviceTypeKey].qty, freightDetail[serviceTypeKey].volume, freightDetail[serviceTypeKey].weight)
                                })
                              })
                          },{})
                        })
                      }else{
                        return combinedResultsInOriginCity;
                      }
                    },{})
                  })
                })
              }
              if(truckingDestinationCity !== null){
                modifiedMergedCustomResult = Object.assign({}, modifiedMergedCustomResult, {
                  orderPriceTrucking : Object.assign({}, modifiedMergedCustomResult.orderPriceTrucking, {
                    [orderDestinationCityCode] : _.reduce(combinedResultsByVendorInCities[orderDestinationCityCode][vendorId][orderFreight], (combinedResultsinDestinationCity, truckingPriceServiceType, serviceTypeKey) => {
                      if(orderServiceTypes.includes(serviceTypeKey)){
                        return Object.assign({}, combinedResultsinDestinationCity, {
                          [serviceTypeKey] : _.reduce(truckingPriceServiceType, (modifiedTruckingCodePrice, truckingCodePriceHead, truckingCodeKey) => {
                              quoteTruckingIds.push(truckingCodePriceHead.quoteTruckingId)
                              return Object.assign({}, modifiedTruckingCodePrice, {
                                [truckingCodeKey] : Object.assign({}, truckingCodePriceHead, {
                                  qty : freightDetail[serviceTypeKey].qty,
                                  volume : freightDetail[serviceTypeKey].volume,
                                  weight : freightDetail[serviceTypeKey].weight,
                                  priceSum : calculateCustomOrTruckingCostBasedOnPriceUOM(truckingCodePriceHead.priceUOM, truckingCodePriceHead.priceMin, truckingCodePriceHead.price, freightDetail[serviceTypeKey].qty, freightDetail[serviceTypeKey].volume, freightDetail[serviceTypeKey].weight)
                                })
                              })
                          },{})
                        })
                      }else{
                        return combinedResultsinDestinationCity;
                      }
                    },{})
                  })
                })
              }
              return combinedResults.concat(Object.assign({}, modifiedMergedCustomResult, {
                quoteId : `${modifiedMergedCustomResult.quoteId}${quoteTruckingIds.join('-')}`
              }));
            }
          }
          return combinedResults;
        },[]);
        console.log('VALID RESULTS AFTER MERGING TRUCKING :', validResults.length);
        if(validResults.length > 0){
          resolve(validResults);
        }else{
          reject('RETURNING NO RESULT AFTER MERGING PRICE TRUCKING');
        }
      }
    }
  })
}

const getInsuranceCostPromise = (insurance, mergedTruckingResults) => {
  let vendorIds = [];
  let promisesInsuranceCost = [];
  if(insurance){
    mergedTruckingResults.map((mergedTruckingResult) => {
      if(!vendorIds.includes(mergedTruckingResult.vendorId)){
            vendorIds.push(mergedTruckingResult.vendorId);
              promisesInsuranceCost.push(mySQL.sequelize.query('call sdisplaypricelistinsurance (:vendorId);', {
              type : mySQL.sequelize.QueryTypes.SELECT,
              replacements : { vendorId : mergedTruckingResult.vendorId}
            }))
        }
    });
    return promisesInsuranceCost;
  }else{
    return promisesInsuranceCost;
  }
}

const mergeInsuranceResult = (insurance, goodsValue, goodsValueCurrency, mergedTruckingResults, rawResults) => {
  return new Promise( (resolve,reject) => {
    if(insurance){
      if(rawResults.length > 0){
        const combinedInsuranceResult = _.reduce(rawResults, (insuranceByVendorBins, insuranceByVendorBin) => {
          const insuranceByVendor =  _.reduce(insuranceByVendorBin[0], (insuranceByVendor, result) => {
            const { orderInsuranceCode, vendorId } = result;
            return Object.assign({}, insuranceByVendor, {
              [vendorId] : Object.assign({}, insuranceByVendor[vendorId],{
                [orderInsuranceCode] : result
              })
            });
          },{});
          return Object.assign({}, insuranceByVendorBins, insuranceByVendor);
        }, {})
        const validResults = mergedTruckingResults.reduce( (validResults, mergedTruckingResult) => {
          const { vendorId } = mergedTruckingResult;
          let isValid = true;
          if(!combinedInsuranceResult.hasOwnProperty(vendorId)){
            isValid = false;
          }
          if(isValid){
            let quoteInsuranceIds = [];
            modifiedInsuranceResult =  Object.assign({}, mergedTruckingResult, {
              quoteId : `${mergedTruckingResult.quoteId}_`,
              orderPriceInsurance : _.reduce(combinedInsuranceResult[vendorId], (orderInsuranceCodes, insurance, insuranceCodeKey) => {
                quoteInsuranceIds.push(insurance.quoteInsuranceId);
                return Object.assign({}, orderInsuranceCodes, {
                  [insuranceCodeKey] : Object.assign({}, orderInsuranceCodes[insuranceCodeKey], {
                    quoteInsuranceId : insurance.quoteInsuranceId,
                    goodsValue : parseFloat(goodsValue),
                    goodsValueCurrency,
                    codePriceInsurance : insurance.orderInsuranceCode,
                    insurancePcg : parseFloat(insurance.orderInsurancePcg),
                    priceSum : parseFloat(goodsValue) * parseFloat(insurance.orderInsurancePcg)
                  })
                })
              }, {})
            })
            modifiedInsuranceResult = Object.assign({}, modifiedInsuranceResult, {
              quoteId : `${modifiedInsuranceResult.quoteId}${quoteInsuranceIds.join('-')}`
            });
            return validResults.concat(modifiedInsuranceResult);
          }
          return validResults;
        }, []);
        if(validResults.length > 0){
          resolve(validResults);
        }else{
          reject('RETURNING NO RESULT AFTER MERGING PRICE INSURANCE');
        }
      }else{
        reject(`ALL VENDORS HAS NO INSURANCE RESULT`)
      }
    }else{
      const mergedInsuranceResults = mergedTruckingResults.reduce((combinedMergedInsuranceResults, result) => {
        return combinedMergedInsuranceResults.concat(Object.assign({}, result, {
          orderPriceInsurance : {}
        }))
      }, []);
      resolve(mergedInsuranceResults);
    }
  })
}
const getVendorRatingsPromise = (mergedInsuranceResults) => {
  let vendorIds = [];
  let promisesVendor = [];
  mergedInsuranceResults.map((mergedInsuranceResult) => {
    if(!vendorIds.includes(mergedInsuranceResult.vendorId)){
          vendorIds.push(mergedInsuranceResult.vendorId);
    }
  });
  promisesVendor.push(mySQL.c_comp.findAll({
    where : {
      id : {
      '$in' : vendorIds
      }
    },
    attributes : ['comprate', 'id']
  }))

  return promisesVendor;
}

const mergeVendorResults = (mergedInsuranceResults,rawResults) => {
  return new Promise( (resolve,reject) => {
    const combinedVendorResults = _.reduce(rawResults, (combinedResults, vendorResultPromise) => {
      return _.reduce(vendorResultPromise, ( _combinedResults, vendorResult) => {
        return Object.assign({}, _combinedResults, {
          [vendorResult.id] : Math.ceil(parseFloat(vendorResult.comprate))
        })
      }, {})
    }, {});
    const combinedResults = _.reduce(mergedInsuranceResults, (combinedResults, insuranceResult) => {
      return combinedResults.concat( Object.assign({}, insuranceResult, {
        vendorRating : combinedVendorResults[insuranceResult.vendorId]
      }))
    },[]);
    if(combinedResults.length > 0) {
      resolve(combinedResults);
    }else{
      reject('ERROR IN MERGING RESULT WITH VENDOR RATING')
    }
  })
}
const mergeFinalResults = (mergedVendorResults, orderPriceCurrency, goodsValueCurrency) => {
  return new Promise((resolve,reject) => {
    mySQL.t_currency.findOne({
      attributes : ['base', 'quote', 'rate'],
      where : {
        base : goodsValueCurrency,
        quote : orderPriceCurrency
      },
      order : [['created_at', 'desc']]
    }).then( (currency) => {
      //console.log(currency);
      const results = _.reduce(mergedVendorResults, (finalResults, result) => {
        const { orderPriceFreight, orderPriceDocument, orderPriceCustom, orderPriceTrucking, orderPriceInsurance } = result;
        const totalPriceFreight = _.reduce( orderPriceFreight, (totalPriceFreight, orderServiceType) => {
            return totalPriceFreight + _.reduce( orderServiceType, (totalPriceFreightPerServiceType, orderCodeGroupPriceFreight) => {
              return totalPriceFreightPerServiceType + _.reduce( orderCodeGroupPriceFreight, (totalPriceFreightPerCodeGroup, orderCodePriceFreight) => {
                return  totalPriceFreightPerCodeGroup + orderCodePriceFreight.priceSum
              }, 0)
            }, 0)
        }, 0);
        //console.log('total price freight :', totalPriceFreight);

        const totalPriceDoc = _.reduce( orderPriceDocument, (totalPriceDoc, orderServiceType) => {
          return totalPriceDoc + _.reduce( orderServiceType, (totalPriceDocPerServiceType, orderCodePriceDoc) => {
            return totalPriceDocPerServiceType + orderCodePriceDoc.priceSum
          },0)
        },0);
        //console.log('total price document :', totalPriceDoc);

        const totalPriceCustom = _.reduce( orderPriceCustom, (totalPriceCustom, orderCustomCity) => {
          return totalPriceCustom + _.reduce( orderCustomCity, (totalPriceCustomPerCity, orderServiceTypeCustom) => {
            const totalPriceCustomPerServiceType = _.reduce( orderServiceTypeCustom, (totalPriceCustomPerServiceType, orderCodePriceCustom) => {
              //console.log(orderCodePriceCustom.quoteCustomId,'-');
              return totalPriceCustomPerServiceType + orderCodePriceCustom.priceSum
            },0)
            return totalPriceCustomPerCity  + totalPriceCustomPerServiceType;
          },0)
        },0);
      //  console.log('total price custom :', totalPriceCustom);

        const totalPriceTrucking = _.reduce( orderPriceTrucking, (totalPriceTrucking, orderTruckingCity) => {
          return totalPriceTrucking + _.reduce( orderTruckingCity, (totalPriceTruckingPerCity, orderServiceTypeTrucking) => {
            const totalPriceTruckingPerServiceType = _.reduce( orderServiceTypeTrucking, (totalPriceTruckingPerServiceType, orderCodePriceTrucking) => {
              //console.log(orderCodePriceTrucking.quoteTruckingId)
              return totalPriceTruckingPerServiceType + orderCodePriceTrucking.priceSum
            },0)
            return totalPriceTruckingPerCity + totalPriceTruckingPerServiceType;
          },0)
        },0);
        //console.log('total price trucking :', totalPriceTrucking);

        const totalPriceInsurance = _.reduce( orderPriceInsurance, (totalPriceInsurance, orderInsuranceCode) => {
          return totalPriceInsurance + orderInsuranceCode.priceSum * currency.rate
        },0)
        //console.log('total Price Insurance :', totalPriceInsurance);

        const totalPrice = totalPriceFreight + totalPriceDoc + totalPriceCustom + totalPriceTrucking + totalPriceInsurance;
        //console.log('total Price :', totalPrice, '\n')
        return finalResults.concat(Object.assign({}, result, {
          orderPriceTotal : totalPrice
        }))
      },[]);
      if(results.length > 0){
        resolve(results);
      }else{
        reject('ERROR WHILE MERGING ALL PRICE')
      }
    });
  })
 }
exports.getQueries = (req,res) => {
  debug_post = req.body;
  const { shipmentLoad, form } = req.body;
  const { originCity, destinationCity, loadsLCL, loadsFCL, custom, originArea, destinationArea, insurance, goodsValueCurrency} = form;
  let goodsValue ;
  if(insurance){
  goodsValue = form.goodsValue.replace(/[^0-9\.-]+/g,"");
  }
  const orderType = getOrderType(form);
  let totalShipment, customCities, customOriginCity, customDestinationCity, truckingCities, truckingOriginCity, truckingDestinationCity;
  let response = [];
  switch(orderType){
    case ORDER_TYPES.PTP :
      customCities = custom ? [originCity,destinationCity] : [];
      customOriginCity = custom ? originCity : null;
      customDestinationCity = custom ? destinationCity : null;
      truckingCities = []
      truckingOriginCity = null;
      truckingDestinationCity = null;
      break;
    case ORDER_TYPES.PTD :
      customCities = custom ? [originCity, destinationCity] : [destinationCity];
      customOriginCity = custom ? originCity : null;
      customDestinationCity = destinationCity;
      truckingCities = [destinationCity];
      truckingOriginCity = null;
      truckingDestinationCity = destinationCity;
      break;
    case ORDER_TYPES.DTP :
      customCities = custom ? [originCity, destinationCity] : [originCity];
      customOriginCity = originCity;
      customDestinationCity = custom ? destinationCity : null;
      truckingCities = [originCity];
      truckingOriginCity = originCity;
      truckingDestinationCity = null;
      break;
    case ORDER_TYPES.DTD :
      customCities = [originCity,destinationCity];
      customOriginCity = originCity;
      customDestinationCity = destinationCity;
      truckingCities = [originCity, destinationCity];
      truckingOriginCity = originCity;
      truckingDestinationCity = destinationCity;
      break;
  }

  if(shipmentLoad === 'lcl' && Object.keys(form).length > 0){
  totalShipment = {
          qty : loadsLCL.reduce( (totalQty,shipment) => {
                  return totalQty += shipment.qty ? parseInt(shipment.qty,10) : 0;
                }, 0),
          volume : loadsLCL.reduce( (totalVolume, shipment) => {
                  let volume = parseFloat(shipment.size.length_,2) * parseFloat(shipment.size.width,2) * parseFloat(shipment.size.height,2) / 1000000 * parseFloat(shipment.qty,2)
                  volume = shipment.size.unit === 'in' ? volume / 35.3147 : volume
                  return totalVolume += !isNaN(volume) ? volume : 0;
                }, 0),
          weight : loadsLCL.reduce( (totalWeight, shipment) => {
                  let weight = parseFloat(shipment.weight,2) * parseFloat(shipment.qty,2)
                  weight = shipment.weightUnit === 'lbs' ? weight / 2.20462 : weight
                  return totalWeight += !isNaN(weight) ? weight : 0;
                }, 0)
        }
    //get query total LCL shipmentLoad

  }else if(shipmentLoad === 'fcl' && Object.keys(form).length > 0){
    totalShipment = loadsFCL.reduce( (totalVolume, shipment) => {
        const { size, qty, type, weight } = shipment;
        totalVolume[size] = !totalVolume.hasOwnProperty(size) ? {} : totalVolume[size];
        totalVolume[size][type] = !totalVolume[size].hasOwnProperty(type) ? qty : totalVolume[size][type] + qty;
        return totalVolume;
      }, {});
  }
  const valueRouteTracker = {
    orderDateBucket : moment().format('YYYY-MM-DD'),
    originCityCode : originCity,
    destinationCityCode : destinationCity,
    userId : req.user.companyUserId,
    companyId : req.user.companyId,
    loadType : shipmentLoad,
    shipmentLoad : JSON.stringify(totalShipment),
    createdAt : moment().format('YYYY-MM-DD hh:mm:ss')
  }
  new noSQL.instance.routeTracker(valueRouteTracker).save( (err) => {
    (err) ? console.log(err) : '';
  });
  Promise.all(getFreightCostPromise(form,shipmentLoad,totalShipment)).then( (rawSearchResult) => {
    groupFreightPriceResultsByVendorAndShippingLine(shipmentLoad,rawSearchResult).then( (groupedPriceResults) => {
      mergePriceResultsByVendorAndShippingLine(shipmentLoad,groupedPriceResults).then( (mergedPriceResult) => {
        Promise.all(getDocumentCostPromise(originCity,destinationCity)).then( (rawDocResults) => {
          mergeDocumentResultByVendorIdAndFreight(mergedPriceResult, rawDocResults).then( (mergedDocResults) => {
            Promise.all(getCustomCostPromise(customCities, destinationCity,  mergedDocResults)).then( (rawCustomResults) => {
              mergeCustomResult(customOriginCity, customDestinationCity, mergedDocResults, rawCustomResults).then( (mergedCustomResults) => {
                Promise.all(getTruckingCostPromise(truckingCities, mergedCustomResults)).then((rawTruckingResults) => {
                  mergeTruckingResult(truckingOriginCity, truckingDestinationCity, mergedCustomResults, rawTruckingResults).then( (mergedTruckingResults) => {
                    Promise.all(getInsuranceCostPromise(insurance, mergedTruckingResults)).then( (rawInsuranceResults) => {
                      mergeInsuranceResult(insurance, goodsValue, goodsValueCurrency, mergedTruckingResults, rawInsuranceResults).then( (mergedInsuranceResults) => {
                        Promise.all(getVendorRatingsPromise(mergedInsuranceResults)).then((rawVendorResults) => {
                          mergeVendorResults(mergedInsuranceResults, rawVendorResults).then( (mergedVendorResults) => {
                            mergeFinalResults(mergedVendorResults, 'USD', goodsValueCurrency ).then( (finalResults) => {
                              response = finalResults;
                              debug_response = finalResults;
                              res.status(HTTP_STATUS.OK).json(response);
                            }).catch((err) => {
                              console.log(err);
                              res.status(HTTP_STATUS.SERVER_ERROR).json({ message : HTTP_MESSAGE.SERVER_ERROR});
                            })
                          }).catch((err) => {
                            console.log(err);
                            res.status(HTTP_STATUS.SERVER_ERROR).json({ message : HTTP_MESSAGE.SERVER_ERROR})
                          })
                        }).catch((err) => {
                          console.log(err);
                          res.status(HTTP_STATUS.SERVER_ERROR).json({ message : HTTP_MESSAGE.SERVER_ERROR})
                        })
                      }).catch((emptyResultError) => {
                        console.log(emptyResultError);
                        res.status(HTTP_STATUS.OK).json(response);
                      })
                    }).catch((err) => {
                      console.log(err);
                      res.status(HTTP_STATUS.SERVER_ERROR).json({ message : HTTP_MESSAGE.SERVER_ERROR})
                    })
                  }).catch((emptyResultError) => {
                    console.log(emptyResultError);
                    res.status(HTTP_STATUS.OK).json(response);
                  })
                }).catch((err) => {
                  console.log(err);
                  res.status(HTTP_STATUS.SERVER_ERROR).json({ message : HTTP_MESSAGE.SERVER_ERROR})
                })
              }).catch((emptyResultError) => {
                console.log(emptyResultError);
                  res.status(HTTP_STATUS.OK).json(response);
              })
            }).catch((err) => {
              console.log(err);
              res.status(HTTP_STATUS.SERVER_ERROR).json({ message : HTTP_MESSAGE.SERVER_ERROR})
            })
          }).catch(err => {
            console.log(err);
            res.status(HTTP_STATUS.SERVER_ERROR).json({ message : HTTP_MESSAGE.SERVER_ERROR})
          });
        }).catch(err => {
          console.log(err);
          res.status(HTTP_STATUS.SERVER_ERROR).json({ message : HTTP_MESSAGE.SERVER_ERROR})
        })
      }).catch( (noValidResultError) => {
        console.log(noValidResultError);
        res.status(HTTP_STATUS.OK).json(response);
      })
    }).catch( emptyResultError => {
      console.log(emptyResultError);
      res.status(HTTP_STATUS.OK).json(response);
    })
  }).catch(err => {
    console.log(err);
    res.status(HTTP_STATUS.SERVER_ERROR).json({ message : HTTP_MESSAGE.SERVER_ERROR})
  });
}

exports.debug = (req,res) => {
  res.json(debug_post);
}
