var nodemailer = require('nodemailer');
var hogan = require('hogan.js');
var fs = require('fs');
var path = require("path");
var env = process.env.NODE_ENV_REST_API || 'development';
var config = require(path.join(__dirname, '..', '..', 'config', 'config.json'))[env];
var sendgridTransporter = require('nodemailer-sendgrid-transport');

const templateEmailVerification = fs.readFileSync('views/email_templates/verificationEmail.html', 'utf-8');
const compiledEmailVerification = hogan.compile(templateEmailVerification);
const templateEmailResetPassword = fs.readFileSync('views/email_templates/resetPasswordEmail.html', 'utf-8');
const compiledEmailResetPassword = hogan.compile(templateEmailResetPassword);
const templateEmailInvitation = fs.readFileSync('views/email_templates/invitationEmail.html', 'utf-8');
const compiledEmailInvitation = hogan.compile(templateEmailInvitation);
const templateNewShipmentEmail = fs.readFileSync('views/email_templates/newShipmentEmail.html', 'utf-8');
const compiledNewShipmentEmail = hogan.compile(templateNewShipmentEmail);

const { method, service, auth : { smtp, api }, sender, template } = config.mailer;

const transporter = method === 'smtp' ? (nodemailer.createTransport({
          service,
          auth: {
            user : smtp.user,
            pass : smtp.pass
          }})
) : (
  nodemailer.createTransport(sendgridTransporter({
    auth : {
      api_user : api.user,
      api_key :  api.pass
    }
  }))
)
/*

*/
const verifyTransporter = () => {
  return new Promise( (resolve,reject) => {
    transporter.verify( (err, success) => {
      (err) ? reject(err) : resolve(success)
    })
  });
}

exports.sendVerificationEmail = ({recipient, body}) => {
  verifyTransporter()
    .then( () => {
      const templateValue = {
        recipient : {
          email : recipient.email,
          fullName : recipient.fullName,
          companyName : recipient.companyName,
        },
        sender,
        message : {
          body : {
            title : 'Terima kasih bergabung bersama dengan kami.',
            verification : {
              verifcode : `${template.url}${template.path.verify}?code=${body.encryptedVerifcode}`,
              button : 'Klik disini untuk verifikasi Email anda',
            }
          },
          footer : {
            title : 'selamat bergabung di andalin'
          }
        }}
      const mailOptions = {
        from : sender.from,
        to : recipient.email,
        subject : 'Verifikasi Akun Anda Sekarang',
        html : compiledEmailVerification.render(templateValue)
      }
      transporter.sendMail(mailOptions, (err,info) => {
        (err) ? console.error(err) : console.log(info);
      })
    })
    .catch( (err) => {
      console.error(err);
    })
}

exports.sendResetPasswordEmail = ({recipient, body}) => {
  verifyTransporter()
    .then( () => {
      const templateValue = {
        recipient : {
          email : recipient.email,
          fullName : recipient.fullName
        },
        sender,
        message : {
          body : {
            title : 'Anda baru saja merubah password anda.',
            reset : {
              resetcode : `${template.url}${template.path.password}?code=${body.encryptedResetCode}`,
              button : 'klik disini untuk merubah password anda'
            }
          },
          footer : {
            title : ''
          }
        }}
      const mailOptions = {
        from : sender.from,
        to : recipient.email,
        subject : 'Reset Password',
        html : compiledEmailResetPassword.render(templateValue)
      }
      transporter.sendMail(mailOptions, (err,info) => {
        (err) ? console.error(err) : console.log(info);
      })
    })
    .catch( (err) => {
      console.error(err);
    })
}

exports.sendInvitationEmail = ({recipient, body}) => {
  verifyTransporter()
    .then( () => {
      const templateValue = {
        recipient : {
          email : recipient.email
        },
        sender,
        message : {
          body : {
            title : 'Anda telah diundang untuk bergabung dengan ' + recipient.companyName,
            invitation : {
              invitationCode : `${template.url}${template.path.invite}?code=${body.encryptedInvitationCode}`,
              button : 'klik disini untuk membuat akun andalin anda'
            }
          },
          footer : {
            title : ''
          }
        }}
      const mailOptions = {
        from : sender.from,
        to : recipient.email,
        subject : `Invitation from ${recipient.companyName}`,
        html : compiledEmailInvitation.render(templateValue)
      }
      transporter.sendMail(mailOptions, (err,info) => {
        (err) ? console.error(err) : console.log(info);
      })
    })
    .catch( (err) => {
      console.error(err);
    })
}

exports.sendNewShipmentEmail = ({recipient, body}) => {
  verifyTransporter()
    .then( () => {
      const templateValue = {
        recipient : {
          email : recipient.email
        },
        sender,
        message : {
          body : {
            title : `Order Baru dari ${body.shipment.shipperCompany}`,
            shipment : body.shipment
          },
          footer : {
            title : ''
          }
        }}
      const mailOptions = {
        from : sender.from,
        to : recipient.email,
        subject : `New Order from ${body.shipment.shipperCompany}`,
        html : compiledNewShipmentEmail.render(templateValue)
      }
      transporter.sendMail(mailOptions, (err,info) => {
        (err) ? console.error(err) : console.log(info);
      })
    })
    .catch( (err) => {
      console.error(err);
    })
}
