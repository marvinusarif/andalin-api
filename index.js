const { HTTP_HOST, HTTP_STATUS } = require('./config/express');
const { WEBSOCKET } = require('./config/const');
var env = require('dotenv').config();
var path = require("path");
var env = process.env.NODE_ENV_REST_API || 'development';
var config = require(path.join(__dirname,'config','config.json'))[env];
var dbClient = require('./database/dbClient');
var express = require('express')
var app = express();
var bodyParser = require('body-parser');
var passport = require('passport');
var auth = require('./controllers/auth');
var cors = require('cors');

var noSQL = require('express-cassandra');
var httpServer = require('http').Server(app);
var io = require('socket.io')(httpServer);
var Redis = require('socket.io-redis');
var jwt = require('jsonwebtoken');

const { redis, server, jwt : {secretOrKey}, client } = config;
io.adapter(Redis({ host : redis.host, port : redis.port }))

const routeContact = require('./routes/contact')
const routeUser = require('./routes/user')
const routeCompany = require('./routes/company')
const routeArea= require('./routes/route')
const routeSearch = require('./routes/search')
const routeQuote = require('./routes/quote')
const routeBook = require('./routes/book')
const routeFile = require('./routes/file')
const routeCurrency = require('./routes/currency')
const routeShipment = require('./routes/shipment')
const routeMessages = require('./routes/messages')

/**
  set mailer
*/
app.engine('html', require('hogan-express'));
app.set('view engine', 'html');
app.use(cors());
app.use(function(req, res, next) {
  var allowedOrigins = client.allowedOrigins;
  var origin = req.headers.origin;
  if(allowedOrigins.indexOf(origin) > -1){
    res.setHeader('Access-Control-Allow-Origin', origin);
  }
  res.setHeader("Access-Control-Allow-Methods", "GET, POST, OPTIONS, PUT, PATCH, DELETE");
  res.setHeader("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, Authorization");
  res.setHeader('Access-Control-Allow-Credentials', true);
  next();
});

auth.initJwtStrategy();
app.use(passport.initialize());
app.use(express.static(path.join(__dirname, 'public')));
app.disable('x-powered-by');
app.use('/contact', routeContact);
app.use('/user', routeUser);
app.use('/company', routeCompany);
app.use('/route', routeArea);
app.use('/message',routeMessages);
app.use('/search',routeSearch);
app.use('/book',routeBook);
app.use('/quote',routeQuote);
app.use('/file', routeFile);
app.use('/currency', routeCurrency);
app.use('/shipment', routeShipment);
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));

dbClient.then((res) => {
  console.log(res);
  let portInstance;
  if(process.env.NODE_ENV_REST_API === 'development'){
    portInstance = server.port
  } else {
    portInstance = parseInt(server.port,10) + parseInt(process.env.NODE_APP_INSTANCE,10)
    console.log('#instance: ', process.env.NODE_APP_INSTANCE)
  }
  httpServer.listen(portInstance, (err) => {
    (err) ? console.log(err) : console.log(`start listening on ${portInstance}`)
  })
  app.get('/', (req,res) => {
    res.status(HTTP_STATUS.OK).send('Andalin Producer REST API')
  })

  io.on('connection', (socket) => {
    socket.on('action', (action) => {
      jwt.verify(action.payload.token, secretOrKey, (err,params) => {
        const { companyId, companyUserId } = params;
        if(err){
          console.log(err)
        }else{
          switch(action.type){
            case WEBSOCKET.WEBSOCKET_JOIN_ROOM :
              if(action.payload.room === null){
                return noSQL.instance.channelSubscribersByUser.find({
                    companyId,
                    userId : companyUserId
                  }, { select : ['orderNumber'] },(err, _subscribedChannels) => {
                    if(err) {
                      console.log(err)
                    } else {
                      socket.join(`${companyId}`)
                      const subscribedChannels = _subscribedChannels.reduce( (subscribedChannels,_subscribedChannel) => {
                        socket.join(_subscribedChannel.orderNumber)
                        return subscribedChannels.concat(_subscribedChannel.orderNumber)
                      }, [])
                      console.log(`join : [${companyId},${subscribedChannels}]`)
                      return socket.emit('action', {
                        type : WEBSOCKET.RECEIVE_WEBSOCKET_JOINED_ROOM,
                        payload : {
                          join : [`${companyId}`,...subscribedChannels]
                        }
                      })
                    }
                  })
              } else {
                console.log(`join :${action.payload.room}`)
                socket.join(`${action.payload.room}`)
                return socket.emit('action', {
                  type : WEBSOCKET.RECEIVE_WEBSOCKET_JOINED_ROOM,
                  payload : {
                    join : [action.payload.room]
                  }
                })
              }
          }
        }
      })
    })
  })
});
