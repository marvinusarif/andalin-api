var hogan = require('hogan.js');
var moment = require('moment');
var pdf = require('html-pdf');
var fs = require('fs');
var path = require("path");
var numeral = require('numeral');
var env = process.env.NODE_ENV_REST_API || 'development';
var config = require(path.join(__dirname, '..', '..', 'config', 'config.json'))[env];
const { COMPANY_STATUS } = require( path.join(__dirname, '..', '..','config','const.js'));

const templateInvoice = fs.readFileSync('views/invoice_templates/invoice.html', 'utf-8');
const compiledTemplateInvoice = hogan.compile(templateInvoice);

const options = { format: 'A4', border : '15mm', base : path.join('file:', __dirname,'..','..','public','assets') };
const { invoice : { bank, company, img_url }} = config;

exports.generateInvoice = ({invoiceStatus, currency, shipper, shipment, freights, prices, insurance, payments, invoice, banks}) => {
  const templateInvoice = fs.readFileSync('views/invoice_templates/invoice.html', 'utf-8');
  const compiledTemplateInvoice = hogan.compile(templateInvoice);
  let grossTotal = 0;
  const grossTotalSubAmount =  prices.reduce((grossTotalSubAmount, price) => {
                                  const { componentType, bookPriceSum } = price;
                                  grossTotal += parseFloat(bookPriceSum,2);
                                  grossTotalSubAmount[componentType] = grossTotalSubAmount.hasOwnProperty(componentType) ? grossTotalSubAmount[componentType] + parseFloat(bookPriceSum,2) : parseFloat(bookPriceSum,2);
                                  return grossTotalSubAmount;
                              },{});
  const templateValue = {
    andalinLogo : `${img_url}/assets/andalin_logo.png`,
    statusInvoice : invoiceStatus,
    invoicePaidStatus : invoice ?
                          invoice.isPaid ? true : false : false,
    invoiceNumber : invoice ? invoice.invoiceNumber : '-',
    invoiceDate : invoice ? moment(invoice.invoiceDate).format('DD/MM/YYYY') : '-',
    invoiceDueDate : invoice ? moment(invoice.invoiceDueDate).format('DD/MM/YYYY') : '-',
    invoiceNote : invoice ? invoice.note : '',
    companyName : company.name,
    companyNPWP : company.npwp.number,
    companyPlace : company.npwp.address.place,
    companyStreet : company.npwp.address.street,
    companyDisctrict : company.npwp.address.district,
    companyCity : company.npwp.address.city,

    shipperName : shipper.comptp === COMPANY_STATUS.shipper ? shipper.companyName : '',
    shipperAddress : shipper.comptp === COMPANY_STATUS.shipper ? shipper.companyAddress : '',
    shipperAddressBilling : shipper.comptp === COMPANY_STATUS.shipper ? shipper.companyAddressBilling : '',
    shipperPhone : shipper.comptp === COMPANY_STATUS.shipper ? shipper.companyPhone : '',
    shipperEmail : shipper.comptp === COMPANY_STATUS.shipper ? shipper.companyEmail : '',

    banksAndalin : banks,

    shipmentOrderNumber : shipment.orderNumber,
    shipmentShipperCompany : shipment.shipperCompany,
    shipmentShipperName : shipment.shipperName,
    shipmentShipperEmail : shipment.shipperEmail,
    shipmentShipperFax : shipment.shipperFax,
    shipmentShipperPhone : shipment.shipperPhone,
    shipmentShipperAddress : shipment.shipperAddress,

    shipmentConsigneeCompany : shipment.consigneeCompany,
    shipmentConsigneeName : shipment.consigneeName,
    shipmentConsigneeAddress : shipment.consigneeAddress,
    shipmentConsigneePhone : shipment.consigneePhone,
    shipmentConsigneeEmail : shipment.consigneeEmail,
    shipmentConsigneeFax : shipment.consigneeFax,

    shipmentCommoditiesClass : shipment.commoditiesClass,
    shipmentCommoditiesType : shipment.commoditiesType,
    shipmentCommoditiesProduct : shipment.commoditiesProduct,
    shipmentCommoditiesHSCode : shipment.commoditiesHSCode,

    shipmentDate : moment(shipment.shipmentDate).format('DD/MM/YYYY'),
    shipmentFreightType : shipment.freightType.toUpperCase(),
    shipmentFreightMode : shipment.freightMode === 'OCEAN' ? 'LAUT' : 'UDARA',

    shipmentCarrierName : shipment.carrierName,
    shipmentBlAwb : shipment.carrierBlAwb,

    shipmentOriginCountry : shipment.originCountry,
    shipmentOriginCity : shipment.originCity,
    shipmentDestinationCountry : shipment.destinationCountry,
    shipmentDestinationCity : shipment.destinationCity,

    shipmentBookPriceTotal : shipment.bookPriceTotal,
    shipmentTrucking : shipment.truckingStat ? 'ya' : 'tidak',
    shipmentCustom : shipment.customStat ? 'ya' : 'tidak',
    shipmentInsurance : shipment.insuranceStat ? 'ya' : 'tidak',
    shipmentBookPriceCurrencyCode : shipment.bookPriceCurrencyCode,
    shipmentBookPriceCurrencyRate : shipment.bookPriceCurrencyRate,
    freights,

    prices : prices.reduce( (_prices,price) => {
              return _prices.concat(Object.assign({}, price, {
                bookPriceSum : numeral(price.bookPriceSum).format('(0,0.00)')
              }))
            },[]),
    payments : payments,
    insurance : insurance ? Object.assign({},insurance,{
              pcg : insurance.pcg + '%',
              goodsValue : numeral(insurance.goodsValue).format('(0,0.00)'),
              priceSum : numeral(insurance.priceSum).format('(0,0.00)')
            }) : insurance,

    showInsurance : insurance ? true : false,

    totalFreights : grossTotalSubAmount.FREIGHT,
    showTotalCustom : grossTotalSubAmount.hasOwnProperty('CUSTOM') ? true : false,
    totalCustom   : grossTotalSubAmount.hasOwnProperty('CUSTOM') ? grossTotalSubAmount.CUSTOM : 0,
    showTotalDocument : grossTotalSubAmount.hasOwnProperty('DOCUMENT') ? true : false,
    totalDocument : grossTotalSubAmount.hasOwnProperty('DOCUMENT') ? grossTotalSubAmount.DOCUMENT : 0,
    showTotalTrucking : grossTotalSubAmount.hasOwnProperty('TRUCKING') ? true : false,
    totalTrucking : grossTotalSubAmount.hasOwnProperty('TRUCKING') ? grossTotalSubAmount.TRUCKING : 0,
    showTotalInsurance : grossTotalSubAmount.hasOwnProperty('INSURANCE') ? true : false,
    totalAdditionalInsurance : grossTotalSubAmount.hasOwnProperty('INSURANCE') ? grossTotalSubAmount.INSURANCE : 0,

    isProforma : invoice ? false : true,
    showRate  : shipment.bookPriceCurrencyCode === 'USD' ? true : false,
    rate : invoice
                ? (invoice.currencyFromCode !== 'IDR' ? numeral(parseFloat(invoice.currencyRate,2)).format('(0,0.00)') : numeral(1).format('(0,0.00)') )
                : (shipment.bookPriceCurrencyCode === 'USD' ? numeral(currency.rate).format('(0,0.00)') : numeral(1).format('(0,0.00)') )
  }

  const totalInsuranceValue = {
    totalInsurance : templateValue.totalAdditionalInsurance + (insurance ? insurance.priceSum : 0),
  }
  const grossTotalValue = {
    //in bookPriceCurrencyCode
    grossTotal : grossTotal + totalInsuranceValue.totalInsurance
  }
  const grossTotalInvoiceValue = {
    //invoice already in IDR : for proforma -> if book price currency code is not in IDR convert it to IDR
    grossTotalInvoice : invoice ? invoice.totalGross :
                          shipment.bookPriceCurrencyCode !== 'IDR' ? grossTotalValue.grossTotal * currency.rate : grossTotalValue.grossTotal
  }
  const taxDetail = {
    taxRate : invoice ? invoice.ppn : 0,
    taxTotal : invoice ? invoice.totalPPN : 0
  }
  const grandTotalPaymentValue = {
    grandTotalPayment : invoice ? invoice.totalGrand :
                          grossTotalInvoiceValue.grossTotalInvoice
  }
  const combinedTemplateValue = Object.assign({}, templateValue, totalInsuranceValue, grossTotalValue, grossTotalInvoiceValue, taxDetail, grandTotalPaymentValue);
  const modifiedTemplateValue = Object.assign({}, combinedTemplateValue, {
    totalFreights : numeral(combinedTemplateValue.totalFreights).format('0,0.00'),
    totalCustom : numeral(combinedTemplateValue.totalCustom).format('(0,0.00)'),
    totalDocument : numeral(combinedTemplateValue.totalDocument).format('(0,0.00)'),
    totalTrucking : numeral(combinedTemplateValue.totalTrucking).format('(0,0.00)'),
    totalInsurance : numeral(combinedTemplateValue.totalInsurance).format('(0,0.00)'),
    grossTotal : numeral(combinedTemplateValue.grossTotal).format('(0,0.00)'),
    grossTotalInvoice : numeral(combinedTemplateValue.grossTotalInvoice).format('(0,0.00)'),
    taxTotal : numeral(combinedTemplateValue.taxTotal).format('(0,0.00)'),
    grandTotalPayment : numeral(combinedTemplateValue.grandTotalPayment).format('(0,0.00)')
  })
  return new Promise( (resolve, reject) => {
    pdf.create(compiledTemplateInvoice.render(modifiedTemplateValue), options).toStream((err, stream) => {
      if (err) reject(err);
      resolve(stream);
    });
  })

}
