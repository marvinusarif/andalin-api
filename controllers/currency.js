const {HTTP_STATUS, HTTP_MESSAGE } = require('../config/express');

var mySQL = require('../database/mysql/mysql-client');

exports.getRate = (req,res) => {
  mySQL.t_currency.findAll({
    attributes : [ 'base', 'quote','rate',
                  ['created_at', 'createdAt']
                ],
    where : {
      created_at : {
        $in : mySQL.sequelize.literal('( select max(created_at) from t_currency group by quote, base ) ')
      }
    },
    order : [['id','DESC'], ['created_at','DESC']]
  }).then((currency) => {
    res.json(currency)
  })
}
