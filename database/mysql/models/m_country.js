module.exports = function(sequelize, Sequelize) {
  var m_country = sequelize.define('m_country', {
    id: {
      autoIncrement: true,
      primaryKey: true,
      type: Sequelize.BIGINT(11)
    },
    code : {
      type: Sequelize.STRING(32),
      allowNull : false
    },
    name : {
      type : Sequelize.STRING(64),
      allowNull : false
    },
    codecurrency : {
      type : Sequelize.STRING(32),
      allowNull : false
    },
    note : {
      type : Sequelize.STRING(256),
      allowNull : true
    },
    status : {
      type : Sequelize.BIGINT(11),
      allowNUll : false
    },
    updated_at : {
      type : Sequelize.DATE,
      allowNull : true
    },
    created_at : {
      type : Sequelize.DATE,
      allowNull : true
    },
    created_user : {
      type : Sequelize.BIGINT(11),
      allowNull : false
    },
    modified_user : {
      type : Sequelize.BIGINT(11),
      allowNull : false
    }
  }, {
    timestamps : false,
    freezeTableName : true, //do not pluralizing tables name
    tableName : 'm_country'
  });
  return m_country;
}
