const { HTTP_STATUS } = require('../config/express');
const { USER_STATUS } = require('../config/const');

var passport = require('passport');
var passportJWT = require('passport-jwt');
var jwt = require('jsonwebtoken');
var mySQL = require('../database/mysql/mysql-client');
var path = require('path');

var env = process.env.NODE_ENV_REST_API || 'development';
var config = require(path.join(__dirname, '..','config','config.json'))[env];

const { secretOrKey, session, algorithm, issuer, audience, expiresIn} = config.jwt;

exports.initJwtStrategy = () => {
  const jwtOptions = {
    jwtFromRequest : passportJWT.ExtractJwt.fromAuthHeaderAsBearerToken(),
    secretOrKey,
    algorithms : [algorithm],
    issuer,
    audience
  }
  const Strategy = passportJWT.Strategy;
  const strategy = new Strategy(jwtOptions, (jwtPayload, next) => {
    const { companyId, companyUserId, companyUserUsername, codeusertype } = jwtPayload;
    mySQL.c_user.findOne({
      attributes : ['id', 'id_company', 'username', 'codeusertype'],
      where : { id : companyUserId,
                id_company : companyId,
                username : companyUserUsername,
                codeusertype : codeusertype }
    }).then( (user) => {
        if(user) {
          return next(null, { companyId,
                              companyUserId,
                              companyUserUsername,
                              codeusertype })
        } else {
          return next(new Error('user not found'), false);
        }
    }).catch( (err) => {
        return next(err, false);
    });
  });
  passport.use(strategy);
}

exports.authenticate = () => {
  const jwtSession = {
    session
  };
  return passport.authenticate("jwt", jwtSession)
}
