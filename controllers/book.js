const {HTTP_STATUS, HTTP_MESSAGE } = require('../config/express');
const {WEBSOCKET, USER_STATUS, COMPANY_STATUS, SHIPMENTLOAD_TYPES, CONTAINER_TYPES, SERVICE_TYPES } = require('../config/const');

var _ = require('lodash');
var mySQL = require('../database/mysql/mysql-client');

var moment = require('moment');
var randomstring = require('randomstring');
var { emitToRooms } = require('../utils/socket');
var mailer = require('../utils/mailer');

exports.postBookQuoteValidation = (req,res) => {
    const { quote, query } = req.body;
    const { orderValidUntil } = quote;
    if(moment(orderValidUntil).isAfter(moment())){
      //when valid
      res.status(HTTP_STATUS.OK).json({
        quote,
        query,
        validity : true})
    }else{
      res.status(HTTP_STATUS.OK).json({
        quote,
        query,
        validity : false})
    }
}
const getCompTp =(codeusertype) => {
  if(codeusertype === USER_STATUS.confirmed_user || codeusertype === USER_STATUS.confirmed_user_officer){
    return COMPANY_STATUS.shipper;
  }else if(codeusertype === USER_STATUS.confirmed_vendor || codeusertype === USER_STATUS.confirmed_vendor_officer){
    return COMPANY_STATUS.vendor;
  }else{
    return COMPANY_STATUS.sysadmin
  }
}
const generateUniqueOrderNumber = ({orderFreight, created_at, shipmentLoad, orderOriginCityCode, orderDestinationCityCode}) => {

  return `ORD/${orderFreight.substr(0,3)}/${shipmentLoad.toUpperCase()}/${orderOriginCityCode}/${orderDestinationCityCode}/${moment(created_at).format('YYYYMMDD/hhmmss')}/${randomstring.generate({
    length: 3,
    charset: 'ADLINEXMKUTDOG'
  })}`;
}
const getFreightServiceExtension = (shipmentType) => {
  switch(shipmentType) {
    case CONTAINER_TYPES.REEFER :
      return SERVICE_TYPES.REEFER;
    case CONTAINER_TYPES.VENTILATE :
      return SERVICE_TYPES.VENTILATE;
    default :
      return '';
  }
}
getFreightFCLServiceType = (shipmentSize) => {
  switch(shipmentSize) {
    case CONTAINER_TYPES.FT20 :
      return SERVICE_TYPES.FT20;
    case CONTAINER_TYPES.FT40 :
      return SERVICE_TYPES.FT40
    case CONTAINER_TYPES.HC40 :
      return SERVICE_TYPES.HC40;
  }
}
const getPriceConversion = (rates, currencyBase, currencyQuote, price) => {
  if(currencyBase !== currencyQuote ){
    return price * rates[currencyBase][currencyQuote].rate;
  }else{
    return price
  }
}
const generateFreightRecord = (query, orderFreight, {id_shipment, order_number, created_at, codeusertype, id_user, id_company }) => {
  const { shipmentLoad, form : { loadsFCL, loadsLCL } } = query;
  let loads;
  if(shipmentLoad === SHIPMENTLOAD_TYPES.LCL){
    loads = _.reduce(loadsLCL, (loads, lcl) => {
      const loadValue = {
        id_shipment,
        order_number,
        service_type : orderFreight === 'OCEAN' ? SERVICE_TYPES.SEALCL :
                        orderFreight === 'AIR' ?  SERVICE_TYPES.AIRCARGO : SERVICE_TYPES.AIRCOURIER,
        qty : lcl.qty,
        weight : lcl.weight,
        volume : lcl.volume,
        additional_info : lcl.type,
        created_at,
        updated_at : null,
        created_by_comptp : getCompTp(codeusertype),
        created_by_id_comp : id_company,
        created_by_id_user : id_user
      }
      return loads.concat(loadValue);
    },[]);
  } else {
    loads = _.reduce(loadsFCL, (loads, fcl) => {
        const loadValue = {
          id_shipment,
          order_number,
          service_type : `${getFreightFCLServiceType(fcl.size)}${getFreightServiceExtension(fcl.type)}` ,
          qty : fcl.qty,
          weight : 0,
          volume : 0,
          additional_info : fcl.weight,
          tracking_number : null,
          created_at,
          updated_at : null,
          created_by_comptp : getCompTp(codeusertype),
          created_by_id_comp : id_company,
          created_by_id_user : id_user
        }
        return loads.concat(loadValue)
    }, []);
  }
  return loads;
}

const generatePriceRecord = (rates, orderCurrency, orderPriceFreight, orderPriceDocument, orderPriceCustom, orderPriceTrucking, {id_shipment,order_number, created_at, codeusertype, id_user, id_company}) => {
  let allPricelist = [];
  const pricelistFreight = _.reduce(orderPriceFreight, (combinedPricelistPerServiceType, pricelistPerServiceType) =>{
    const combinedPricelistPerPriceGroup = _.reduce(pricelistPerServiceType, (combinedPricelistPerPriceGroup, pricelistPerPriceGroup) => {
      const combinedPricelistPerPriceCode = _.reduce(pricelistPerPriceGroup, (combinedPricelistPerPriceCode, pricelistPerPriceCode) => {
        const { quoteFreightId, serviceType, codeGroupPriceFreight, descGroupPriceFreight, codePriceFreight, descPriceFreight, qty, volume, weight,
                priceUOM, priceCurrencyCode, price, priceMin, priceSum } = pricelistPerPriceCode;
        return combinedPricelistPerPriceCode.concat({
          id_shipment,
          order_number,
          id_price : quoteFreightId,
          component_type : 'FREIGHT',
          component_city_code : null,
          service_type : serviceType,
          price_group_code : codeGroupPriceFreight,
          price_group_desc : descGroupPriceFreight,
          price_code : codePriceFreight,
          price_desc : descPriceFreight,
          qty,
          volume,
          weight,
          price_uom : priceUOM,
          price : price,
          price_min : priceMin,
          price_sum : priceSum,
          price_currency_code : priceCurrencyCode,
          book_price : getPriceConversion(rates, priceCurrencyCode, orderCurrency, price),
          book_price_min : getPriceConversion(rates, priceCurrencyCode, orderCurrency, priceMin),
          book_price_sum : getPriceConversion(rates, priceCurrencyCode, orderCurrency, priceSum),
          book_price_currency_code : orderCurrency,
          book_price_currency_rate : rates[priceCurrencyCode][orderCurrency].rate,
          created_at,
          updated_at : null,
          created_by_comptp : getCompTp(codeusertype),
          created_by_id_comp : id_company,
          created_by_id_user : id_user
        });
      },[]);
      return combinedPricelistPerPriceGroup.concat(combinedPricelistPerPriceCode);
    }, []);
    return combinedPricelistPerServiceType.concat(combinedPricelistPerPriceGroup);
  },[]);

  const pricelistDoc = _.reduce(orderPriceDocument, (combinedPricelistPerServiceType, pricelistPerServiceType) => {
    const combinedPricelistPerPriceCode = _.reduce(pricelistPerServiceType, (combinedPricelistPerPriceCode, pricelistPerPriceCode) => {
      const { quoteDocId, serviceType, codeGroupPriceDoc, descGroupPriceDoc, codePriceDoc, descPriceDoc, qty, volume, weight,
              priceUOM, priceCurrencyCode, price, priceMin, priceSum } = pricelistPerPriceCode;
      return combinedPricelistPerPriceCode.concat({
          id_shipment,
          order_number,
          id_price : quoteDocId,
          component_type : 'DOCUMENT',
          component_city_code : null,
          service_type : serviceType,
          price_group_code : codeGroupPriceDoc,
          price_group_desc : descGroupPriceDoc,
          price_code : codePriceDoc,
          price_desc : descPriceDoc,
          qty,
          volume,
          weight,
          price_uom : priceUOM,
          price : price,
          price_min : priceMin,
          price_sum : priceSum,
          price_currency_code : priceCurrencyCode,
          book_price : getPriceConversion(rates,priceCurrencyCode, orderCurrency, price),
          book_price_min : getPriceConversion(rates,priceCurrencyCode, orderCurrency, priceMin),
          book_price_sum : getPriceConversion(rates,priceCurrencyCode, orderCurrency, priceSum),
          book_price_currency_code : orderCurrency,
          book_price_currency_rate : rates[priceCurrencyCode][orderCurrency].rate,
          created_at,
          updated_at : null,
          created_by_comptp : getCompTp(codeusertype),
          created_by_id_comp : id_company,
          created_by_id_user : id_user
        });
    },[])
    return combinedPricelistPerServiceType.concat(combinedPricelistPerPriceCode);
  }, []);

  const pricelistCustom = _.reduce(orderPriceCustom, (pricelistCustom, pricelistPerCityCode, cityCode) => {
    const pricelistCustomPerCityCode = _.reduce(pricelistPerCityCode, (combinedPricelistPerServiceType, pricelistPerServiceType) => {
        const combinedPricelistPerPriceCode = _.reduce(pricelistPerServiceType, (combinedPricelistPerPriceCode, pricelistPerPriceCode) => {
          const { quoteCustomId, serviceType, codeGroupPriceCustom, descGroupPriceCustom, codePriceCustom, descPriceCustom, qty, volume, weight,
                  priceUOM, priceCurrencyCode, price, priceMin, priceSum } = pricelistPerPriceCode;
          return combinedPricelistPerPriceCode.concat({
              id_shipment,
              order_number,
              id_price : quoteCustomId,
              component_type : 'CUSTOM',
              component_city_code : cityCode,
              service_type : serviceType,
              price_group_code : codeGroupPriceCustom,
              price_group_desc : descGroupPriceCustom,
              price_code : codePriceCustom,
              price_desc : descPriceCustom,
              qty,
              volume,
              weight,
              price_uom : priceUOM,
              price : price,
              price_min : priceMin,
              price_sum : priceSum,
              price_currency_code : priceCurrencyCode,
              book_price : getPriceConversion(rates,priceCurrencyCode, orderCurrency, price),
              book_price_min : getPriceConversion(rates,priceCurrencyCode, orderCurrency, priceMin),
              book_price_sum : getPriceConversion(rates,priceCurrencyCode, orderCurrency, priceSum),
              book_price_currency_code : orderCurrency,
              book_price_currency_rate : rates[priceCurrencyCode][orderCurrency].rate,
              created_at,
              updated_at : null,
              created_by_comptp : getCompTp(codeusertype),
              created_by_id_comp : id_company,
              created_by_id_user : id_user
            });
        },[])
        return combinedPricelistPerServiceType.concat(combinedPricelistPerPriceCode);
      }, []);
    return pricelistCustom.concat(pricelistCustomPerCityCode);
  },[]);

  const pricelistTrucking = _.reduce(orderPriceTrucking, (pricelistTrucking, pricelistPerCityCode, cityCode) => {
    const pricelistTruckingPerCityCode = _.reduce(pricelistPerCityCode, (combinedPricelistPerServiceType, pricelistPerServiceType) => {
        const combinedPricelistPerPriceCode = _.reduce(pricelistPerServiceType, (combinedPricelistPerPriceCode, pricelistPerPriceCode) => {
          const { quoteTruckingId, serviceType, codeGroupPriceTrucking, descGroupPriceTrucking, codePriceTrucking, descPriceTrucking, qty, volume, weight,
                  priceUOM, priceCurrencyCode, price, priceMin, priceSum } = pricelistPerPriceCode;
          return combinedPricelistPerPriceCode.concat({
              id_shipment,
              order_number,
              id_price : quoteTruckingId,
              component_type : 'TRUCKING',
              component_city_code : cityCode,
              service_type : serviceType,
              price_group_code : codeGroupPriceTrucking,
              price_group_desc : descGroupPriceTrucking,
              price_code : codePriceTrucking,
              price_desc : descPriceTrucking,
              qty,
              volume,
              weight,
              price_uom : priceUOM,
              price : price,
              price_min : priceMin,
              price_sum : priceSum,
              price_currency_code : priceCurrencyCode,
              book_price : getPriceConversion(rates,priceCurrencyCode, orderCurrency, price),
              book_price_min : getPriceConversion(rates,priceCurrencyCode, orderCurrency, priceMin),
              book_price_sum : getPriceConversion(rates,priceCurrencyCode, orderCurrency, priceSum),
              book_price_currency_code : orderCurrency,
              book_price_currency_rate : rates[priceCurrencyCode][orderCurrency].rate,
              created_at,
              updated_at : null,
              created_by_comptp : getCompTp(codeusertype),
              created_by_id_comp : id_company,
              created_by_id_user : id_user
            });
        },[])
        return combinedPricelistPerServiceType.concat(combinedPricelistPerPriceCode);
      }, []);
    return pricelistTrucking.concat(pricelistTruckingPerCityCode);
  },[]);

  return allPricelist.concat(pricelistFreight, pricelistDoc, pricelistCustom, pricelistTrucking);
}

const generateInsuranceRecord = (rates, orderCurrency, orderPriceInsurance, {id_shipment,order_number, created_at, codeusertype, id_user, id_company}) => {
  return _.reduce(orderPriceInsurance, (price, insuranceCode) => {
    const { quoteInsuranceId, goodsValue, goodsValueCurrency, insurancePcg, priceSum } = insuranceCode;
    const insurancePrice = {
      id_shipment,
      id_price : quoteInsuranceId,
      order_number,
      goods_value : goodsValue,
      goods_value_currency : goodsValueCurrency,
      pcg : insurancePcg,
      price_sum : getPriceConversion(rates, goodsValueCurrency, orderCurrency, priceSum),
      price_currency_rate : rates[goodsValueCurrency][orderCurrency].rate,
      price_currency_code : orderCurrency,
      created_at,
      update_at : null,
      created_by_comptp : getCompTp(codeusertype),
      created_by_id_comp : id_company,
      created_by_id_user : id_user
    }
    return price.concat(insurancePrice);
  },[]);
}
const getRequestUnit = (type) => {
  if(type === 'min_temperature' || type === 'max_temperature'){
    return 'Celcius'
  }else if(type === 'free_time') {
    return 'Days'
  }
}

const generateRequestRecord = (requests, {id_shipment,order_number, created_at, codeusertype, id_user, id_company}) => {
  if(typeof requests !== 'undefined'){
    return requests.reduce((combinedRequests, request) => {
      const { type, value, note } = request;
      const _request = {
        id_shipment,
        shipper_id_comp : id_company,
        shipper_id_user : id_user,
        order_number,
        type,
        value,
        unit : getRequestUnit(type),
        stat : 'U',
        note : note,
        created_at,
        updated_at : null,
        created_by_comptp : getCompTp(codeusertype),
        created_by_id_comp : id_company,
        created_by_id_user : id_user
      }
      return combinedRequests.concat(_request);
    }, [])
  }else{
    return []
  }

}
const generateFreightSum = (query) => {
  let totalShipment = null;
  if(query.shipmentLoad === 'lcl' && Object.keys(query.form).length > 0){
    const { loadsLCL } = query.form;
    totalShipment = {
            qty : loadsLCL.reduce( (totalQty,shipment) => {
                    return totalQty += shipment.qty ? parseInt(shipment.qty,10) : 0;
                  }, 0),
            volume : loadsLCL.reduce( (totalVolume, shipment) => {
                    let volume = parseInt(shipment.size.length_,10) * parseInt(shipment.size.width,10) * parseInt(shipment.size.height,10) / 1000000 * parseInt(shipment.qty,10)
                    volume = shipment.size.unit === 'in' ? volume / 35.3147 : volume
                    return totalVolume += !isNaN(volume) ? parseInt(volume,10) : 0;
                  }, 0),
            weight : loadsLCL.reduce( (totalWeight, shipment) => {
                    let weight = parseInt(shipment.weight,10) * parseInt(shipment.qty,10)
                    weight = shipment.weightUnit === 'lbs' ? weight / 2.20462 : weight
                    return totalWeight += !isNaN(weight) ? parseInt(weight,10) : 0;
                  }, 0)
          }
  }else if(query.shipmentLoad === 'fcl' && Object.keys(query.form).length > 0){
    const { loadsFCL } = query.form;
    totalShipment = {
            qty : loadsFCL.reduce( (totalQty, shipment) => {
                  return totalQty += shipment.qty ? parseInt(shipment.qty,10) : 0;
                  }, 0),
            summary : loadsFCL.reduce( (totalVolume, shipment) => {
                    const { qty, size, type, weight } = shipment;
                    totalVolume[size] = !totalVolume.hasOwnProperty(size) ? {} : totalVolume[size];
                    totalVolume[size][type] = !totalVolume[size].hasOwnProperty(type) ? {} : totalVolume[size][type];
                    totalVolume[size][type][weight]= totalVolume[size][type][weight] ? (totalVolume[size][type][weight] + qty ) : qty;
                    return totalVolume
                  },{})
    }
  }
  return totalShipment;
}
exports.postBookPlaceOrderQuote = (req,res) => {
  const { companyId, companyUserUsername, companyUserId, codeusertype } = req.user;
  const { quote, query, form } = req.body;
  const { vendorId, vendorName, orderShippingLine,
          orderOriginCountryCode, orderOriginCountry, orderOriginCityCode, orderOriginCity,
          orderDestinationCountryCode, orderDestinationCountry, orderDestinationCityCode, orderDestinationCity,
          orderPol, orderPod, orderValidUntil, orderTransittimeMax, orderTransittimeMin,
          orderPriceTotal, orderPriceCurrencyCode, orderPriceCurrencyRate,
          orderFreight, orderDirect,
          orderPriceFreight, orderPriceDocument, orderPriceCustom, orderPriceTrucking, orderPriceInsurance} = quote;
  const { shipperCompany, shipperPICName, shipperAddress, shipperPhone, shipperEmail, shipperNote,
          commoditiesClass, commoditiesType, commoditiesHSCode, commoditiesProduct,
          consigneeCompany, consigneePICName, consigneeAddress, consigneePhone, consigneeEmail,
          requests, shipperFax, consigneeFax, orderCurrency } = form;
  const { shipmentLoad, form : { datePickup } } = query;
  const combinedFreight = generateFreightSum(query);
  let response = {};
  mySQL.t_currency.findAll({
    attributes : [ 'base', 'quote','rate',
                  ['created_at', 'createdAt']
                ],
    where : {
      created_at : {
        $in : mySQL.sequelize.literal('( select max(created_at) from t_currency group by quote, base ) ')
      }
    },
    order : [['id','DESC'], ['created_at', 'DESC']]
  }).then((currencies) => {
    const rates = currencies.reduce( (rates, _rate) => {
        if( !rates.hasOwnProperty(_rate.base) ) {
          rates[_rate.base] = {}
        }
        return Object.assign({}, rates, {
          [_rate.base] : Object.assign({}, rates[_rate.base], {
            [_rate.quote] : {
              rate : _rate.rate
            }
          })
        });
      }, {});
    const created_at = moment().format('YYYY-MM-DD hh:mm:ss');
    const shipmentValue = {
      order_number : generateUniqueOrderNumber({created_at, orderFreight, shipmentLoad, orderOriginCityCode, orderDestinationCityCode}),
      order_stat : 'BOOK',
      shipper_id_comp : companyId,
      shipper_id_user : companyUserId,
      shipper_comp : shipperCompany,
      shipper_name : shipperPICName,
      shipper_address : shipperAddress,
      shipper_phone : shipperPhone,
      shipper_email : shipperEmail,
      shipper_fax : shipperFax,
      shipper_notes : shipperNote,
      commodities_class : commoditiesClass,
      commodities_type : commoditiesType,
      commodities_hscode : commoditiesHSCode,
      commodities_product : commoditiesProduct,
      consignee_comp : consigneeCompany,
      consignee_name : consigneePICName,
      consignee_address : consigneeAddress,
      consignee_phone : consigneePhone,
      consignee_email : consigneeEmail,
      consignee_fax : consigneeFax,
      additional_request_stat : typeof requests !== 'undefined' ? 1 : 0,
      freight_mode : orderFreight,
      freight_direct : orderDirect,
      freight_type : shipmentLoad,
      freight_sum : JSON.stringify(combinedFreight),
      trucking_stat : Object.keys(orderPriceTrucking).length > 0 ? 1 : 0,
      document_stat : Object.keys(orderPriceDocument).length > 0 ? 1 : 0,
      custom_stat : Object.keys(orderPriceCustom).length > 0 ? 1 : 0,
      insurance_stat : Object.keys(orderPriceInsurance).length > 0 ? 1 : 0,
      price_total : orderPriceTotal,
      price_currency_code : orderPriceCurrencyCode,
      book_price_total : getPriceConversion(rates, orderPriceCurrencyCode, orderCurrency, orderPriceTotal),
      book_price_currency_code : orderCurrency,
      book_price_currency_rate : rates[orderPriceCurrencyCode][orderCurrency].rate,
      is_invoice_paid : false,
      is_invoice_issued : false,
      vendor_id : vendorId,
      vendor_name : vendorName,
      shipment_date : moment(datePickup,'DD/MM/YY').format('YYYY-MM-DD'),
      carrier_name : orderShippingLine,
      carrier_bl_awb : null,
      carrier_contract : null,
      origin_country_code : orderOriginCountryCode,
      origin_country : orderOriginCountry,
      origin_city_code : orderOriginCityCode,
      origin_city : orderOriginCity,
      pol : orderPol,
      destination_country_code : orderDestinationCountryCode,
      destination_country : orderDestinationCountry,
      destination_city_code : orderDestinationCityCode,
      destination_city : orderDestinationCity,
      pod : orderPod,
      valid_until : orderValidUntil,
      transittime_min : orderTransittimeMin,
      transittime_max : orderTransittimeMax,
      created_at,
      updated_at : null
    }
    mySQL.sequelize.transaction((ts) => {
      return mySQL.t_shipment.create(shipmentValue,{transaction : ts}).then((shipment) => {
        const joinValue = {
          id_shipment : shipment.id ,
          order_number : shipment.order_number,
          created_at,
          codeusertype,
          id_user : companyUserId,
          id_company : companyId }
        const freightValue = generateFreightRecord(query, orderFreight, joinValue);
        const priceValue = generatePriceRecord(rates, orderCurrency, orderPriceFreight, orderPriceDocument, orderPriceCustom, orderPriceTrucking, joinValue);
        const insuranceValue = generateInsuranceRecord(rates, orderCurrency, orderPriceInsurance, joinValue);
        const requestValue = generateRequestRecord(requests, joinValue);
        return mySQL.t_shipmentfreight.bulkCreate(freightValue, {transaction : ts}).then((shipmentfreight) => {
            return mySQL.t_shipmentprice.bulkCreate(priceValue, {transaction : ts}).then((shipmentprice) => {
              if(insuranceValue.length > 0){
                return mySQL.t_shipmentinsurance.bulkCreate(insuranceValue, {transaction : ts}).then((shipmentinsurance) => {
                  if(requestValue.length > 0){
                    return mySQL.t_shipmentrequest.bulkCreate(requestValue, {transaction :ts}).then((shipmentrequests) => {
                      return Object.assign({}, shipmentValue, { id : shipment.id });
                      })
                    } else {
                      return Object.assign({}, shipmentValue, { id : shipment.id });
                    }
                })
              }else{
                if(requestValue.length > 0){
                  return mySQL.t_shipmentrequest.bulkCreate(requestValue, {transaction :ts}).then((shipmentrequests) => {
                    return Object.assign({}, shipmentValue, { id : shipment.id });
                    })
                  } else {
                    return Object.assign({}, shipmentValue, { id : shipment.id });
                  }
              }
            })
          })
      })
    }).then((transactionResult) => {
      const { id, order_number, order_stat, shipper_comp, commodities_type, consignee_comp, vendor_id, freight_mode, freight_type, freight_sum,
              book_price_total, book_price_currency_rate, book_price_currency_code,
              vendor_name, carrier_bl_awb, carrier_name, origin_country_code, origin_city_code,
              destination_country_code, destination_city_code, pol, pod, transittime_max, transittime_min,
              created_at } = transactionResult;
      const response = Object.assign({},{
        validity : true,
          result : Object.assign({},{
              shipmentId : id,
              orderNumber : order_number,
              orderStat : order_stat,
              shipperId : companyId,
              shipperCompany : shipper_comp,
              commoditiesType : commodities_type,
              consigneeCompany : consignee_comp,
              freightMode : freight_mode,
              freightType : freight_type,
              freightSummary : JSON.parse(freight_sum),
              bookPriceTotal : book_price_total,
              bookPriceCurrencyCode : book_price_currency_code,
              bookPriceCurrencyRate : book_price_currency_rate,
              vendorName : vendor_name,
              shipmentDate : datePickup,
              carrierBlAwb : carrier_bl_awb,
              carrierName : carrier_name,
              originCountryCode : origin_country_code,
              originCityCode : origin_city_code,
              destinationCountryCode : destination_country_code,
              destinationCityCode : destination_city_code,
              pol,
              pod,
              transittimeMax : transittime_max,
              transittimeMin : transittime_min,
              createdAt : created_at
        })
      });
      const rooms = [`${companyId}`, `${vendorId}`, `${1}`]
      emitToRooms(rooms, WEBSOCKET.RECEIVE_WEBSOCKET_BOOK_SHIPMENT, { response : Object.assign({}, response, {
        result : Object.assign({}, response.result, {
          isNew : true
        })
      })})
      res.status(HTTP_STATUS.OK).json(response);

      mySQL.c_user.findAll({
        attributes : ['email', 'fullname'],
        where : {
          id_company : {
            $in : [companyId, vendorId]
          }
        }
      }).then((users) => {
        const sendEmailsTo = users.reduce( (sendEmailsTo,user) => {
          sendEmailsTo[user.dataValues.email] = user.dataValues.email
          return sendEmailsTo
        }, { "sales@andalin.com" : "Sales Andalin"})
        _.map( sendEmailsTo, (fullname,email) => {
          const templateEmail = {
            recipient: {
              email: email,
              fullName: fullname,
            },
            body: {
              shipment : response.result
            }
          }
          mailer.sendNewShipmentEmail(templateEmail);
        })
      })
    });
  }).catch( (err) => {
    console.log(err)
    res.status(HTTP_STATUS.SERVER_ERROR).json({message: HTTP_MESSAGE.SERVER_ERROR});
  })


}
