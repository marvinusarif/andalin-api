const { HTTP_STATUS, HTTP_MESSAGE } = require('../config/express');
var moment = require('moment');
var noSQL = require('express-cassandra');

exports.postQuote = (req,res) => {
    const { companyId, companyUserId, companyUserUsername } = req.user;
    const { query, quote } = req.body
    const { query : { shipmentLoad, form },
            quote : { vendorId, vendorName,
                      quoteId,
                      orderFreight,
                      orderShippingLine,
                      orderOriginCountryCode,
                      orderOriginCountry,
                      orderOriginCityCode,
                      orderOriginCity,
                      orderPol,
                      orderDestinationCountryCode,
                      orderDestinationCountry,
                      orderDestinationCityCode,
                      orderDestinationCity,
                      orderPod,
                      orderDirect,
                      orderValidUntil,
                      orderTransittimeMax,
                      orderTransittimeMin,
                      orderPriceTotal,
                      orderPriceCurrencyCode,
                      orderPriceCurrencyRate,
                      orderNote,
                      orderNoteDetailFreight,
                      orderPriceFreight,
                      orderPriceDocument,
                      orderPriceCustom,
                      orderPriceTrucking,
                      orderPriceInsurance} } = req.body;

    const valueQuotesByShipper = {
      shipperId : parseInt(companyId),
      shipperUserId : parseInt(companyUserId),
      vendorId : parseInt(vendorId),
      vendorName,
      queryShipmentLoad : shipmentLoad,
      queryForm : JSON.stringify(form),
      quoteId,
      orderFreight,
      orderShippingLine,
      orderDirect : (orderDirect == 'true'),
      orderOriginCountryCode,
      orderOriginCountry,
      orderOriginCityCode,
      orderOriginCity,
      orderPol,
      orderDestinationCountryCode,
      orderDestinationCountry,
      orderDestinationCityCode,
      orderDestinationCity,
      orderPod,
      orderTransittimeMax : parseInt(orderTransittimeMax),
      orderTransittimeMin : parseInt(orderTransittimeMin),
      orderValidUntil : moment(orderValidUntil).format('YYYY-MM-DD HH:mm:ss Z'),
      orderPriceTotal : parseFloat(orderPriceTotal),
      orderPriceCurrencyCode,
      orderPriceCurrencyRate : parseFloat(orderPriceCurrencyRate),
      orderNote : JSON.stringify(orderNote),
      orderNoteDetailFreight : JSON.stringify(orderNoteDetailFreight),
      orderPriceFreight : JSON.stringify(orderPriceFreight),
      orderPriceDocument : JSON.stringify(orderPriceDocument),
      orderPriceCustom : JSON.stringify(orderPriceCustom),
      orderPriceTrucking : JSON.stringify(orderPriceTrucking),
      orderPriceInsurance : JSON.stringify(orderPriceInsurance),

      createdBy : companyUserUsername,
      createdAt : moment().format('YYYY-MM-DD HH:mm:ss')
    }
    new noSQL.instance.quotesByShipper(valueQuotesByShipper)
      .save( (err) => {
        (err) ? console.log(err) : res.status(HTTP_STATUS.OK).json({ quote, query, createdAt : valueQuotesByShipper.createdAt })
      });
}

exports.getQuote = (req,res) => {
    const { companyId, companyUserId } = req.user;
    const { pageState } = req.params;

    const queryQuoteByShipper = {
      shipperId : companyId,
      shipperUserId : companyUserId
    }
    const optionQuoteByShipper = {
      fetchSize : 10,
      select : [
              'queryShipmentLoad',
              'queryForm',
              'vendorName',
              'vendorId',
              'quoteId',
              'orderFreight',
              'orderShippingLine',
              'orderDirect',
              'orderOriginCountryCode',
              'orderOriginCountry',
              'orderOriginCityCode',
              'orderOriginCity',
              'orderPol',
              'orderDestinationCountryCode',
              'orderDestinationCountry',
              'orderDestinationCityCode',
              'orderDestinationCity',
              'orderPod',
              'orderTransittimeMax',
              'orderTransittimeMin',
              'orderValidUntil',
              'orderPriceTotal',
              'orderPriceCurrencyCode',
              'orderPriceCurrencyRate',
              'orderNote',
              'orderNoteDetailFreight',
              'orderPriceFreight',
              'orderPriceDocument',
              'orderPriceCustom',
              'orderPriceTrucking',
              'orderPriceInsurance',
              'createdBy',
              'createdAt'],
      $orderBy : {'$desc' : 'createdAt'},
      pageState : typeof pageState !== "undefined" ? pageState : null
    }
    let resultsQuotesByShipper = {
      results : [],
      pageState : null
    };
    const quotesByShipper = noSQL.instance.quotesByShipper
          quotesByShipper.eachRow(queryQuoteByShipper, optionQuoteByShipper, (n,row) => {
            const { queryShipmentLoad,
                    queryForm,
                    vendorName,
                    vendorId,
                    quoteId,
                    orderFreight,
                    orderShippingLine,
                    orderDirect,
                    orderOriginCountryCode,
                    orderOriginCountry,
                    orderOriginCityCode,
                    orderOriginCity,
                    orderPol,
                    orderDestinationCountryCode,
                    orderDestinationCountry,
                    orderDestinationCityCode,
                    orderDestinationCity,
                    orderPod,
                    orderTransittimeMax,
                    orderTransittimeMin,
                    orderValidUntil,
                    orderPriceTotal,
                    orderPriceCurrencyCode,
                    orderPriceCurrencyRate,
                    orderNote,
                    orderNoteDetailFreight,
                    orderPriceFreight,
                    orderPriceDocument,
                    orderPriceCustom,
                    orderPriceTrucking,
                    orderPriceInsurance,
                    createdBy,
                    createdAt
                    } = row;
            resultsQuotesByShipper.results.push({
              query: {
                shipmentLoad : queryShipmentLoad,
                form : JSON.parse(queryForm)
              },
              quote:{
                vendorId,
                vendorName,
                quoteId,
                orderFreight,
                orderShippingLine,
                orderDirect,
                orderOriginCountryCode,
                orderOriginCountry,
                orderOriginCityCode,
                orderOriginCity,
                orderPol,
                orderDestinationCountryCode,
                orderDestinationCountry,
                orderDestinationCityCode,
                orderDestinationCity,
                orderPod,
                orderTransittimeMax,
                orderTransittimeMin,
                orderValidUntil,
                orderPriceTotal,
                orderPriceCurrencyCode,
                orderPriceCurrencyRate,
                orderNote : JSON.parse(orderNote),
                orderNoteDetailFreight : JSON.parse(orderNoteDetailFreight),
                orderPriceFreight : JSON.parse(orderPriceFreight),
                orderPriceDocument : JSON.parse(orderPriceDocument),
                orderPriceCustom : JSON.parse(orderPriceCustom),
                orderPriceTrucking : JSON.parse(orderPriceTrucking),
                orderPriceInsurance : JSON.parse(orderPriceInsurance)
              },
              createdBy,
              createdAt
            });
    }, (err,quotes) => {
      (err) && console.error(err);
        resultsQuotesByShipper.pageState = quotes.pageState;
        res.status(HTTP_STATUS.OK).json(resultsQuotesByShipper);
    });
}

exports.deleteQuote = (req,res) => {
  const { quoteId, createdAt } = req.params;
  const { companyUserId, companyId } = req.user;
  var queryQuotesByShipper = {
                                shipperId : companyId,
                                shipperUserId : companyUserId,
                                quoteId,
                                createdAt : createdAt
                              };
  noSQL.instance.quotesByShipper.delete(queryQuotesByShipper, (err) => {
    if(err) {
      console.log(err);
      res.status(HTTP_STATUS.BAD_REQUEST).json({
        message : HTTP_MESSAGE.BAD_REQUEST
      })
    }
    res.status(HTTP_STATUS.OK).json({quoteId, createdAt})
  });
}
