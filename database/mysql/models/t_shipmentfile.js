module.exports = function(sequelize, Sequelize) {
  var t_shipmentfile = sequelize.define('t_shipmentfile', {
    id: {
      autoIncrement: true,
      primaryKey: true,
      type: Sequelize.BIGINT(11)
    },
    id_shipment : {
      type : Sequelize.BIGINT(11),
      allowNull : false
    },
    order_number : {
      type : Sequelize.STRING(64),
      allowNull : false
    },
    file_category : {
      type : Sequelize.STRING(32),
      allowNull : false
    },
    file_description : {
      type : Sequelize.STRING(256),
      allowNull : false
    },
    file_container : {
      type : Sequelize.STRING(128),
      allowNull : false
    },
    file_name : {
      type : Sequelize.STRING(256),
      allowNull : false
    },
    file_ext : {
      type : Sequelize.STRING(8),
      allowNull : false
    },
    file_origin : {
      type : Sequelize.STRING(8),
      allowNull : false
    },
    created_at : {
      type : Sequelize.DATE,
      allowNull : false
    },
    updated_at : {
      type : Sequelize.DATE,
      allowNull : true
    },
    created_by_comptp : {
      type : Sequelize.STRING(8),
      allowNull : false
    },
    created_by_id_comp : {
      type : Sequelize.BIGINT(11),
      allowNull : false
    },
    created_by_id_user : {
      type : Sequelize.BIGINT(11),
      allowNull : false
    },
    created_by_username : {
      type : Sequelize.STRING(64),
      allowNull : false
    }
  }, {
    timestamps : false,
    freezeTableName : true, //do not pluralizing tables name
    tableName : 't_shipmentfile'
  });
  return t_shipmentfile;
}
