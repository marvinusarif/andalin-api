var express = require('express');
var bodyParser = require('body-parser');

var auth = require('../controllers/auth');
var book = require('../controllers/book');

var router = express.Router();
router.use(bodyParser.json());
router.use(bodyParser.urlencoded({extended: true}));

router.route('/validate')
.post( auth.authenticate(), book.postBookQuoteValidation);

router.route('/order')
.post( auth.authenticate(), book.postBookPlaceOrderQuote);

module.exports = router
