const { ALLOWED_CONTENT_TYPES } = require('../config/file');
var storage = require('azure-storage');
var storageFilter = new storage.ExponentialRetryPolicyFilter();
var path = require('path');
var streamifier = require('streamifier');
var crypto = require('crypto');
var moment = require('moment');
var uuidv4 = require('uuid/v4');
var fs = require('fs');

exports.multerSettings = (multerStorage, contentType='images') => {
  return {
    storage : multerStorage,
    limits : { fileSize : ALLOWED_CONTENT_TYPES[contentType].size },
    fileFilter : (req, file, next) => {
      const ext = path.extname(file.originalname);
      !ALLOWED_CONTENT_TYPES[contentType].ext.includes(ext.toLowerCase()) && next(new Error (`Only ${contentType} are allowed`))
      next(null, true);
    }
  }
}
exports.downloadBlobFile = (azureConfig, container, fileName) => {
  const { acc, key } = azureConfig;
  const azureStorage = storage.createBlobService(acc,key).withFilter(storageFilter);
  const hashedContainerName = crypto.createHmac('sha1', container.secret)
                              .update(container.name)
                              .digest('hex');
  const fileNameBase = path.basename(fileName, path.extname(fileName));
  const pathFileName = path.join(__dirname,'..','tmp','files',fileName);
  return new Promise( (resolve, reject) => {
    azureStorage.getBlobToLocalFile(
      hashedContainerName,
      fileNameBase,
      pathFileName,
      (err,blob) => {
        if(err){
          console.log(err)
          reject(err)
        }else{
          resolve(pathFileName);
        }
      }
    )
  })
}
exports.downloadBlobStream = (azureConfig, container, fileName) => {
  const { acc, key } = azureConfig;
  const azureStorage = storage.createBlobService(acc,key).withFilter(storageFilter);
  const hashedContainerName = crypto.createHmac('sha1', container.secret)
                              .update(container.name)
                              .digest('hex');
  const fileNameOnly = path.basename(fileName, path.extname(fileName));
  return new Promise( (resolve,reject) => {
    azureStorage.getBlobProperties(
      hashedContainerName,
      fileNameOnly,
      (err,props,status) => {
        if(err){
          reject(err);
        }else if(!status.isSuccessful){
          reject('FILE_NOT_FOUND');
        }else{
          const result = {
            contentType : props.contentSettings.contentType,
            stream : azureStorage.createReadStream(hashedContainerName, fileNameOnly)
          }
          resolve(result);
        }
      })
  });
}

exports.createBlobContainer = (azureConfig, container, req) => {
  const { acc, key } = azureConfig;

  const azureStorage = storage.createBlobService(acc,key).withFilter(storageFilter);
  const hashedContainerName = crypto.createHmac('sha1', container.secret)
                               .update(container.name)
                               .digest('hex');
  return new Promise( (resolve,reject) => {
    azureStorage.createContainerIfNotExists(hashedContainerName, container.publicAccessLevel !== 'private' ? { publicAccessLevel : container.publicAccessLevel } : null,
    (err,res,resp) => {
      err ? reject(err) : resolve({ azureStorage, container : { name : hashedContainerName, secret : container.secret }, req });
    });
  });
}

exports.uploadFileStream = ({azureStorage, container, req}) => {
  return new Promise( (resolve, reject) => {
    const fileName = crypto.createHmac('sha256', container.secret)
                      .update(`${uuidv4()}`)
                      .digest('hex');
    const stream = streamifier.createReadStream(req.file.buffer);
    const newFileName = !req.saveWithFileExtension ? fileName : `${fileName}${path.extname(req.file.originalname)}`;
    azureStorage.createBlockBlobFromStream(
      container.name,
      newFileName,
      stream,
      req.file.size,
      (err, res, resp) => {
        err ? reject(err) : resolve({ container : container.name,
                                      file : fileName,
                                      ext : path.extname(req.file.originalname)});
      });
  });
}

exports.deleteFileFromContainer = (azureConfig,container,filesName, withExt=false) => {
  const { acc, key } = azureConfig;

  const azureStorage = storage.createBlobService(acc,key).withFilter(storageFilter);
  const hashedContainerName = crypto.createHmac('sha1', container.secret)
                               .update(container.name)
                               .digest('hex');
  const _filesName = !withExt ? path.basename(filesName, path.extname(filesName)) : filesName;
  azureStorage.deleteBlob(hashedContainerName, _filesName, (err,res) => {
    if(err){
      console.log(err)
    }
  })
}
