var fs = require('fs');
var path = require("path");
var env = process.env.NODE_ENV_REST_API || 'development';
var config = require(path.join(__dirname, '..', '..','config','config.json'))[env];
var Sequelize = require('sequelize');
var db = {}

const { sql : { database, username, password, host, port, dialect, pool : { max, min, idle } }} = config;

const sequelize = new Sequelize(database, username, password, {
  host,
  dialect /** dialect mysql || postgres || mssql */,
  pool : { max, min, idle }
})

fs.readdirSync(__dirname + '/models')
  .filter( (file) => {
    return (file.indexOf(".") !== 0)
  })
  .forEach( (file) => {
    var model = sequelize.import(path.join(__dirname, '/models/', file));
    db[model.name] = model;
  });

Object.keys(db).forEach( (modelName) => {
  if("associate" in db[modelName]) {
    db[modelName].associate(db);
  }
});

db.sequelize = sequelize;
db.Sequelize = Sequelize;

module.exports = db;
