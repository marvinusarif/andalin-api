var express = require('express');
var bodyParser = require('body-parser');

var auth = require('../controllers/auth');
var currency = require('../controllers/currency')

var router = express.Router();
router.use(bodyParser.json());
router.use(bodyParser.urlencoded({extended: true}));

router.route('/')
.get( auth.authenticate(), currency.getRate);

module.exports = router
