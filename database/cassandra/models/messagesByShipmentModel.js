module.exports = {
  fields: {
    "shipmentYearMonthBucket" : "int",
    "messageDateBucket" : "int",
    "shipmentId" : "int",
    "orderNumber" : "text",
    "body" : "text",
    "attachment" : {
      type : "frozen",
      typeDef : "<attachment>"
    },
    "asReplyTo" : {
      type : "frozen",
      typeDef : "<messagereplied>"
    },
    "sender" : {
      type : "frozen",
      typeDef : "<sender>"
    },
    "postedFrom" : "text",
    "createdAt" : "timestamp",
    "messageId" : "uuid"
  },
  key: [["shipmentYearMonthBucket", "messageDateBucket"], "shipmentId", "orderNumber", "createdAt", "messageId"],
  clustering_order: {
    "shipmentId" : "ASC",
    "orderNumber" : "ASC",
    "createdAt" : "ASC",
    "messageId" : "ASC"
  },
  table_name: "messagesByShipment"
}
