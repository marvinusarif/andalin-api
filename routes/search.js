var express = require('express');
var bodyParser = require('body-parser');

var search = require('../controllers/search')
var auth = require('../controllers/auth')

var router = express.Router();
router.use(bodyParser.json());
router.use(bodyParser.urlencoded({extended: true}));

router.route('/')
.post( auth.authenticate(), search.getQueries);

router.route('/debug')
.get( search.debug);

module.exports = router
