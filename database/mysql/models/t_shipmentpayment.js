module.exports = function(sequelize, Sequelize) {
  var t_shipmentpayment = sequelize.define('t_shipmentpayment', {
    id: {
      autoIncrement: true,
      primaryKey: true,
      type: Sequelize.BIGINT(11)
    },
    id_invoice : {
      type : Sequelize.BIGINT(11),
      allowNull : false
    },
    id_shipment : {
      type : Sequelize.BIGINT(11),
      allowNull : false
    },
    order_number : {
      type : Sequelize.STRING(64),
      allowNull : false
    },
    method : {
      type : Sequelize.STRING(16),
      allowNull : false
    },
    andalin_bank_name : {
      type : Sequelize.STRING(64),
      allowNull : false
    },
    andalin_bank_account_name : {
      type : Sequelize.STRING(64),
      allowNull : false
    },
    andalin_bank_account_number : {
      type : Sequelize.STRING(64),
      allowNull : false
    },
    shipper_bank_name : {
      type : Sequelize.STRING(64),
      allowNull : false
    },
    shipper_bank_account_name : {
      type : Sequelize.STRING(64),
      allowNull : false
    },
    shipper_bank_account_number : {
      type : Sequelize.STRING(64),
      allowNull : false
    },
    payment_reference_number : {
      type : Sequelize.STRING(64),
      allowNull : false
    },
    payment_date : {
      type : Sequelize.DATE,
      allowNull : false
    },
    payment_total : {
      type : Sequelize.DOUBLE,
      allowNull : false
    },
    payment_currency_code : {
      type : Sequelize.STRING(8),
      allowNull : false
    },
    note : {
      type : Sequelize.STRING(256),
      allowNull : true
    },
    created_at : {
      type : Sequelize.DATE,
      allowNull : false
    },
    updated_at : {
      type : Sequelize.DATE,
      allowNull : true
    },
    created_by_comptp : {
      type : Sequelize.STRING(8),
      allowNull : false
    },
    created_by_id_comp : {
      type : Sequelize.BIGINT(11),
      allowNull : false
    },
    created_by_id_user : {
      type : Sequelize.BIGINT(11),
      allowNull : false
    }
  }, {
    timestamps : false,
    freezeTableName : true, //do not pluralizing tables name
    tableName : 't_shipmentpayment'
  });
  return t_shipmentpayment;
}
