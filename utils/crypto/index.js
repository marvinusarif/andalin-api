var encrypter = require('object-encrypter');
var engine = encrypter('J4Mjdn212kaxeSsHnikL', { outputEncoding : 'hex'});

exports.encrypt = (obj) => {
  return engine.encrypt(obj)
}

exports.decrypt = (obj) => {
  return engine.decrypt(obj)
}
