module.exports = function(sequelize, Sequelize) {
  var m_contacts = sequelize.define('m_contacts', {
    id: {
      autoIncrement: true,
      primaryKey: true,
      type: Sequelize.BIGINT(11)
    },
    fullname : {
      type : Sequelize.STRING(64),
      allowNull : false
    },
    email : {
      type : Sequelize.STRING(64),
      allowNull : false
    },
    phone : {
      type : Sequelize.TEXT,
      allowNull : false
    },
    subject : {
      type : Sequelize.STRING(256),
      allowNull : false
    },
    subject_description : {
      type : Sequelize.STRING(256),
      allowNull : false
    },
    message : {
      type : Sequelize.TEXT,
      allowNull : false
    },
    read : {
      type : Sequelize.INTEGER,
      allowNull : false,
      defaultValue : 0
    },
    sender : {
      type : Sequelize.STRING(8),
      allowNull : false
    }
  }, {
    timestamps : false,
    freezeTableName : true, //do not pluralizing tables name
    tableName : 'm_contacts'
  });
  return m_contacts;
}
