var Promise = require('bluebird');
var noSQL = require( __dirname + '/cassandra/cassandra-client');
var mySQL = require( __dirname + '/mysql/mysql-client');

var dbClient = new Promise((resolve, reject) => {
    Promise.all([noSQL,mySQL]).spread( (nosql,mysql) => {
      resolve('db connection is established');
    })
    .catch( (err) => reject(err) )
});

module.exports = dbClient;
