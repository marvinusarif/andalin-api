var express = require('express');
var bodyParser = require('body-parser');

var contact = require('../controllers/contact');

var router = express.Router();
router.use(bodyParser.json());
router.use(bodyParser.urlencoded({extended: true}));

router.route('/')
.post( contact.contacts);

module.exports = router
