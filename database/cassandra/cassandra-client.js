var path = require("path");
var env = process.env.NODE_ENV_REST_API || 'development';
var config = require(path.join(__dirname,'..','..','config','config.json'))[env];
var noSQL = require('express-cassandra');

const { nosql : { username, password, keyspace, contactPoints, port, replication : { strategy, factor }, queryOptions, migration, createKeyspace } } = config;
//Tell express-cassandra to use the models-directory, and
//use bind() to load the models using cassandra configurations.
var clientConnection = new Promise((resolve,reject) => {
  noSQL.setDirectory( __dirname + '/models').bind(
    {
        clientOptions: {
            contactPoints,
            protocolOptions: { port },
            keyspace,
            //authProvider: new noSQL.driver.auth.DsePlainTextAuthProvider( username, password),
            queryOptions: {consistency: queryOptions === 'one' ? noSQL.consistencies.one : noSQL.consistencies.quorum}
        },
        ormOptions: {
            defaultReplicationStrategy : {
                class: strategy,
                replication_factor: factor
            },
            migration : migration,
            createKeyspace,
            udts: {
              attachment : {
                type : 'text',
                fileId : 'int',
                fileDesc : 'text'
              },
              sender : {
                id : 'int',
                username : 'text',
                codeusertype : 'text',
                compid : 'int',
                comptp : 'text'
              },
              messagereplied : {
                messageId : 'uuid',
                body : 'text',
                sender : 'frozen<sender>',
                attachment : 'frozen<attachment>',
                createdAt : "timestamp"
              }
            }
        }
    },
    (err) => { if(err) reject(err.message);
               else resolve('noSQL connection is established');
    });
});

module.exports = clientConnection;
