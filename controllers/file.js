const { HTTP_STATUS, HTTP_MESSAGE } = require('../config/express');
const { WEBSOCKET, USER_STATUS, COMPANY_STATUS, HISTORY_ACTION } = require('../config/const');
var noSQL = require('express-cassandra');
var mySQL = require('../database/mysql/mysql-client');
var fs = require('fs');
var path = require('path');
var env = process.env.NODE_ENV_REST_API || 'development';
var config = require(path.join(__dirname, '..', 'config', 'config.json'))[env];
var moment = require('moment');
var multer = require('multer');
var fileUtil = require('../utils/file');
var { emitToRooms } = require('../utils/socket');

const { file : { companyStorage, shipmentStorage }, redis } = config;

var memoryStorage = multer.memoryStorage();

const getCompTp =(codeusertype) => {
  if(codeusertype === USER_STATUS.confirmed_user || codeusertype === USER_STATUS.confirmed_user_officer){
    return COMPANY_STATUS.shipper;
  }else if(codeusertype === USER_STATUS.confirmed_vendor || codeusertype === USER_STATUS.confirmed_vendor_officer){
    return COMPANY_STATUS.vendor;
  }else{
    return COMPANY_STATUS.sysadmin
  }
}

exports.multerSingleDoc = (req,res,next) => {
  const upload = multer(fileUtil.multerSettings(memoryStorage,'all')).single('fileDocument');
  upload(req,res,(err)=>{
      if(err) {
        console.log(err);
        const { code } = err;
        if(code === 'LIMIT_FILE_SIZE'){
          res.status(HTTP_STATUS.REQUEST_TOO_LARGE).json({ message : HTTP_MESSAGE.REQUEST_TOO_LARGE})
        }else if(code === 'LIMIT_UNEXPECTED_FILE'){
          res.status(HTTP_STATUS.BAD_REQUEST).json({ message : HTTP_MESSAGE.BAD_REQUEST});
        }else{
          res.status(HTTP_STATUS.BAD_REQUEST).json({ message : HTTP_MESSAGE.FILE_TYPE})
        }
      }
      next()
  })
}
exports.multerSingleImage = (req,res,next) => {
  const upload = multer(fileUtil.multerSettings(memoryStorage,'images')).single('fileImage');
  upload(req,res,(err)=>{
      if(err) {
        console.log(err);
        const { code } = err;
        if(code === 'LIMIT_FILE_SIZE'){
          res.status(HTTP_STATUS.REQUEST_TOO_LARGE).json({ message : HTTP_MESSAGE.REQUEST_TOO_LARGE})
        }else if(code === 'LIMIT_UNEXPECTED_FILE'){
          res.status(HTTP_STATUS.BAD_REQUEST).json({ message : HTTP_MESSAGE.BAD_REQUEST});
        }else{
          res.status(HTTP_STATUS.BAD_REQUEST).json({ message : HTTP_MESSAGE.FILE_TYPE})
        }
      }
      next()
  })
}
exports.uploadShipmentDoc = (req,res,next) => {
  const { shipmentId } = req.body;

  const container = {
    name : shipmentId.toString(),
    secret : shipmentStorage.secret,
    publicAccessLevel : 'private'
  }
  if(req.file){
    fileUtil.createBlobContainer(shipmentStorage, container, req)
      .then(fileUtil.uploadFileStream)
      .then((res) => {
        req.upload = res;
        next()
      }).catch((err) => {
        console.log(err);
        res.status(HTTP_STATUS.SERVER_ERROR).json({
          message : HTTP_MESSAGE.SERVER_ERROR
        });
      })
  } else {
    res.status(HTTP_STATUS.BAD_REQUEST).json({
      message : HTTP_MESSAGE.BAD_REQUEST
    })
  }
}

exports.uploadCompanyDoc = (req,res,next) => {
  const { companyId } = req.user
  const container = {
    name : companyId.toString(),
    secret : companyStorage.secret,
    publicAccessLevel : 'private'
  }
  if(req.file){
    fileUtil.createBlobContainer(companyStorage,container,req)
      .then(fileUtil.uploadFileStream)
      .then((res)=>{
        req.upload = res;
        next();
      }).catch((err)=>{
        console.log(err);
        res.status(HTTP_STATUS.SERVER_ERROR).json({
          message : HTTP_MESSAGE.SERVER_ERROR
        })
      });
  }else{
    res.status(HTTP_STATUS.BAD_REQUEST).json({
      message : HTTP_MESSAGE.BAD_REQUEST
    })
  }
}
exports.saveShipmentDoc = (req,res) => {
  const { companyId, companyUserId, companyUserUsername, codeusertype } = req.user;
  const { documentCategory, documentDesc, shipmentId, orderNumber, documentOrigin } = req.body;
  const { file, container, ext } = req.upload;
  const splittedOrderNumber = orderNumber.split('/');
  const shipmentYearMonthBucket = parseInt(splittedOrderNumber[5].substr(2,4),0);
  const comptp = getCompTp(codeusertype);
  const createdAt = moment().format('YYYY-MM-DD HH:mm:ss Z');

  mySQL.sequelize.transaction((t) => {
    const fileValue = {
      id_shipment : shipmentId,
      order_number : orderNumber,
      file_category : documentCategory,
      file_description : documentDesc,
      file_container : container,
      file_name : file,
      file_ext : ext,
      file_origin : documentOrigin,
      created_at : createdAt,
      updated_at : null,
      created_by_comptp : comptp ,
      created_by_id_comp : companyId,
      created_by_id_user : companyUserId,
      created_by_username : companyUserUsername
    }
    return mySQL.t_shipmentfile.create(fileValue,{transaction : t}).then((fileRes) => {
      const historyValue = {
        id_shipment : shipmentId,
        order_number : orderNumber,
        created_by_comptp : comptp,
        created_by_id_comp : companyId,
        created_by_id_user : companyUserId,
        created_by_username : companyUserUsername,
        action : HISTORY_ACTION.DOCUMENT.UPLOAD,
        description : `upload ${documentCategory} with description of ${documentDesc}`,
        created_at : createdAt
      }
      return mySQL.t_shipmenthistory.create(historyValue, {transaction: t})
    })
  }).then((transactionResult) => {
    const queryShipment = {
          id_shipment : shipmentId,
          order_number : orderNumber
        }
    mySQL.t_shipmentfile.findAll({
      attributes : [
        ['id', 'fileId'],
        ['id_shipment', 'shipmentId'],
        ['order_number', 'orderNumber'],
        ['file_category', 'fileCategory'],
        ['file_description', 'fileDesc'],
        ['file_ext', 'fileExt'],
        ['file_origin', 'fileOrigin'],
        ['created_at', 'createdAt'],
        ['created_by_comptp', 'createdByComptp'],
        ['created_by_id_comp', 'createdByIdCompany'],
        ['created_by_id_user', 'createdByIdUser'],
        ['created_by_username', 'createdByUsername'],
      ],
      where : queryShipment,
      order : [['created_at', 'DESC']]
    }).then( (shipmentDocuments) => {
      const splittedOrderNumber = orderNumber.split('/');
      const shipmentYearMonthBucket = parseInt(splittedOrderNumber[5].substr(2,4),0);
      const messagesHeadValue = {
        shipmentId : parseInt(shipmentId,0),
        shipmentYearMonthBucket : parseInt(shipmentYearMonthBucket),
        messageDateBucket : parseInt(moment().format('YYYYMMDD'),0)
      }
      new noSQL.instance.messagesByShipmentHead(messagesHeadValue).save( (err) => {
        if(err){
          console.log(err);
          res.status(HTTP_STATUS.SERVER_ERROR).json({message : HTTP_MESSAGE.SERVER_ERROR});
        } else {
          const { shipmentYearMonthBucket, messageDateBucket, shipmentId } = messagesHeadValue;
          const messageBodyValue = {
            shipmentYearMonthBucket,
            messageDateBucket,
            shipmentId,
            orderNumber,
            body : `upload ${documentCategory}`,
            attachment : {
              type : shipmentDocuments[0].dataValues.fileCategory + '/' + shipmentDocuments[0].dataValues.fileExt,
              fileId : parseInt(shipmentDocuments[0].dataValues.fileId,0),
              fileDesc : shipmentDocuments[0].dataValues.fileDesc
            },
            asReplyTo : null,
            postedFrom : 'WEBAPP',
            sender : {
              id : parseInt(companyUserId,0),
              username : companyUserUsername,
              codeusertype : codeusertype,
              compid : companyId,
              comptp : getCompTp(codeusertype)
            },
            createdAt,
            messageId : noSQL.uuid()
          }
          new noSQL.instance.messagesByShipment(messageBodyValue).saveAsync().then(() => {
            const response = Object.assign({},{
              shipmentId,
              notification : {
                shipmentId,
                orderNumber,
                topic : 'NEW_DOCUMENT',
                isRead : false,
                createdAt
              },
              documents : shipmentDocuments,
              message : messageBodyValue
            })
              const rooms = [orderNumber]
              emitToRooms(rooms, WEBSOCKET.RECEIVE_WEBSOCKET_UPDATE_SHIPMENT_DOCUMENTS, { response })
              res.status(HTTP_STATUS.OK).json(response);
              noSQL.instance.channelSubscribersByShipment.find({
                shipmentYearMonthBucket,
                shipmentId
              }, { select : ['userId', 'companyId']}, (err,subscribers) => {
                const queries = subscribers.reduce( (queries,subscriber) => {
                  const { userId, companyId } = subscriber
                  return userId !== companyUserId
                  ? queries.concat( new noSQL.instance.channelNotificationsByUser({
                      companyId,
                      userId,
                      shipmentId,
                      orderNumber,
                      topic : 'NEW_DOCUMENT',
                      isRead : false,
                      createdAt
                    }).save({ return_query : true, ttl : 86400*90}))
                  : queries
                },[])
                noSQL.doBatch(queries, (err) => {
                  if (err) console.log(err)
                })
              })
          }).catch((err) => {
            console.log(err);
            res.status(HTTP_STATUS.SERVER_ERROR).json({message : HTTP_MESSAGE.SERVER_ERROR})
          })
        }
      })
    }).catch( (err) => {
      console.log(err);
      res.status(HTTP_STATUS.SERVER_ERROR).json({message : HTTP_MESSAGE.SERVER_ERROR})
    })
  }).catch((err) => {
    console.log(err);
    res.status(HTTP_STATUS.SERVER_ERROR).json({
      message : HTTP_MESSAGE.SERVER_ERROR
    })
  });
}

exports.saveCompanyDoc = (req,res) => {
  const { companyId } = req.user;
  const { documentType } = req.body;
  const { file, ext } = req.upload;
  mySQL.c_comp.findOne({
    attributes : [`attach${documentType}`],
    where : {
      id : companyId
    }
  }).then( (company) => {
    //deleting old files in container
    if(company[`attach${documentType}`] !== null){
      const container = {
        name : companyId.toString(),
        secret : companyStorage.secret
      }
      fileUtil.deleteFileFromContainer(companyStorage,container,company[`attach${documentType}`]);
    }
    const compValue = {
      id : companyId,
      [`attach${documentType}`] : `${file}${ext}`
    }
    company.updateAttributes(compValue)

    if(company){
      const response = {
        [documentType] : ext
      }
      res.status(HTTP_STATUS.OK).json(response);
    }else{
      res.status(HTTP_STATUS.SERVER_ERROR).json({message : HTTP_MESSAGE.SERVER_ERROR});
    }
  }).catch( (err) => {
    console.log(err);
    res.status(HTTP_STATUS.BAD_REQUEST).json({message : HTTP_MESSAGE.BAD_REQUEST});
  })
}
exports.downloadShipmentDoc = (req,res) => {
  const { companyId } = req.user;
  const { method, fileId, fileCategory } = req.params;
  const shipmentId  = parseInt(req.params.shipmentId, 0);
  const orderNumber = req.params.orderNumber.split('-').join('/');
  mySQL.t_shipmentfile.findOne({
    attributes : [
      ['id', 'id'],
      ['order_number', 'orderNumber'],
      ['id_shipment', 'shipmentId'],
      ['file_container','fileContainer'],
      ['file_name','fileName'],
      ['file_ext','fileExt']
    ],
    where : {
      id : fileId,
      id_shipment : shipmentId,
      order_number : orderNumber,
      file_category : fileCategory
    }
  }).then((file) => {
    const container = {
      name : shipmentId.toString(),
      secret : shipmentStorage.secret
    }
    if(method === 'file'){
      fileUtil.downloadBlobFile(shipmentStorage, container, file.dataValues.fileName)
        .then((result) => {
          res.download(result, `${orderNumber.split('/').join('-')}_${fileCategory}_${fileId}${file.dataValues.fileExt}`, (err) => {
            if(err){
              console.log(err);
            }else{
              fs.unlink(result, (err) => {
                if(err){
                  console.log(err)
                }
              });
            }
          });
        }).catch((err) => {
          console.log(err);
          res.status(HTTP_STATUS.BAD_REQUEST).json({message : HTTP_MESSAGE.BAD_REQUEST});
        });
    }else if(method === 'stream') {
      fileUtil.downloadBlobStream(shipmentStorage, container, file.dataValues.fileName)
        .then((result) => {
          const { stream, contentType } = result;
          res.header('Content-Type', contentType);
          stream.pipe(res);
        }).catch((err) => {
          console.log(err);
          res.status(HTTP_STATUS.BAD_REQUEST).json({message : HTTP_MESSAGE.BAD_REQUEST});
        });
    }else{
      res.status(HTTP_STATUS.BAD_REQUEST).json({message : HTTP_MESSAGE.BAD_REQUEST});
    }
  })
}
exports.downloadCompanyDoc = (req,res) => {
  const { companyId } = req.user;
  const { documentType, method } = req.params;
  mySQL.c_comp.findOne({
    attributes : [`attach${documentType}`],
    where : {
      id : companyId
    }
  }).then( (company) => {
    const container = {
      name : companyId.toString(),
      secret : companyStorage.secret
    }
    if(method === 'file') {
      fileUtil.downloadBlobFile(companyStorage, container, company[`attach${documentType}`])
        .then((result) => {
          res.download(result, `${documentType}${path.extname(company[`attach${documentType}`])}`, (err) => {
            if(err){
              console.log(err);
            }else{
              fs.unlink(result, (err) => {
                if(err){
                  console.log(err)
                }
              });
            }
          });
        }).catch((err) => {
          console.log(err);
          res.status(HTTP_STATUS.BAD_REQUEST).json({message : HTTP_MESSAGE.BAD_REQUEST});
        });
    }else if(method === 'stream') {
      fileUtil.downloadBlobStream(companyStorage, container, company[`attach${documentType}`])
        .then((result) => {
          const { stream, contentType } = result;
          res.header('Content-Type', contentType);
          stream.pipe(res);
        }).catch((err) => {
          console.log(err);
          res.status(HTTP_STATUS.BAD_REQUEST).json({message : HTTP_MESSAGE.BAD_REQUEST});
        });
    }else{
      res.status(HTTP_STATUS.BAD_REQUEST).json({message : HTTP_MESSAGE.BAD_REQUEST});
    }
  }).catch( (err) => {
    console.log(err);
    res.status(HTTP_STATUS.SERVER_ERROR).json({message : HTTP_MESSAGE.SERVER_ERROR});
  })
}

exports.uploadUserPhoto = (req,res, next) => {
  if(req.file){
    const container = {
      name : 'photo',
      secret : companyStorage.secret,
      publicAccessLevel : 'blob'
    }
    req.saveWithFileExtension = true;
    fileUtil.createBlobContainer(companyStorage,container,req)
      .then(fileUtil.uploadFileStream)
      .then((res)=>{
        req.upload = res;
        next();
      }).catch((err)=>{
        console.log(err);
        res.status(HTTP_STATUS.SERVER_ERROR).json({
          message : HTTP_MESSAGE.SERVER_ERROR
        })
      });
  }else{
    res.status(HTTP_STATUS.BAD_REQUEST).json({
      message : HTTP_MESSAGE.BAD_REQUEST
    })
  }
}

exports.saveUserPhoto = (req,res) => {
  const { companyUserId, companyId, companyUserUsername } = req.user;
  const { container, file, ext } = req.upload;
  mySQL.c_user.find({
    attributes : ['photo'],
    where : {
      id : companyUserId,
      id_company : companyId,
      username : companyUserUsername
    }
  }).then( (user) => {
    //delete old files
    const containerSettings = {
      name : 'photo',
      secret : companyStorage.secret
    }
    if(user.photo){
      fileUtil.deleteFileFromContainer(companyStorage, containerSettings, path.basename(user.photo), true);
    }
    user.updateAttributes({
      id : companyUserId,
      photo : `${container}/${file}${ext}`
    });
    const response = {
      photo : `${container}/${file}${ext}`
    }
    res.status(HTTP_STATUS.OK).json(response);
  }).catch( (err) => {
    console.log(err);
    res.status(HTTP_STATUS.BAD_REQUEST).json({message : HTTP_MESSAGE.BAD_REQUEST});
  })
}
